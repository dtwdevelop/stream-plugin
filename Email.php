<?php
namespace Iptv ;
require ABSPATH . 'wp-content/plugins/iptv/lib/vendor/autoload.php';

/**
 *  Sender email action
 */

class Email {

    public static function type_check($type){
       $email_template = "";
        switch($type){
            case 'register' : {
                $email_template  = file_get_contents(ABSPATH . 'wp-content/plugins/iptv/template/register.html');
            break;
            }
            case 'reset' : {
                $email_template  = file_get_contents(ABSPATH . 'wp-content/plugins/iptv/template/password_reset.html');
                break ;
            }
            case 'payment' : {
                $email_template  = file_get_contents(ABSPATH . 'wp-content/plugins/iptv/template/payment_user.html');
            break;
            }
            case 'payment_admin' : {
                $email_template  = file_get_contents(ABSPATH . 'wp-content/plugins/iptv/template/payment_admin.html');
            break;
            }
            case 'package_confirm' : {
                $email_template  = file_get_contents(ABSPATH . 'wp-content/plugins/iptv/template/package_confirm.html');
            break;
            }
            default :{
                return "" ;
            }
        }
        return $email_template;
    }

    public static function sendEmail($attrs){

        try{
           
            $admin_email  = get_option('admin_email');
            $replacements = [];
           
            //$replacements['test@demo.com'] = [ '{user_id}'=> '1', '{email_admin}' =>$admin_email ];
            if($attrs['type'] == 'register' || $attrs['type'] == 'reset'){
                $replacements[$attrs['email_to']] = [ '{user_id}'=> base64_encode($attrs['user_id']), '{email_admin}' =>$admin_email ];
            }  
            
            if($attrs['type'] == 'package_confirm' ) {
                $reference  =  md5($attrs['user_id'].$attrs['email_to']);
                $replacements[$attrs['email_to']] = [ '{reference_id}'=> $reference, '{expire_date}'=> $attrs['expire_date'], '{name}'=> $attrs['name'], '{package_name}'=> $attrs['package_name'], '{email_admin}' =>$admin_email , '{suma}'=>$attrs['suma'] ];
            } 

            if($attrs['type'] == 'payment' ) {
                $reference  =  md5($attrs['user_id'].$attrs['email_to']);
                $replacements[$attrs['email_to']] = [ '{reference_id}'=> $reference,  '{name}'=> $attrs['name'],  '{email_admin}' =>$admin_email , '{suma}'=>$attrs['suma'] ];
            } 

            if($attrs['type'] == 'payment_admin' ) {
                $reference  =  md5($attrs['user_id'].$attrs['email_to']);
                $replacements[$attrs['email_to']] = [ 
                '{reference_id}'=> $reference,
                 '{id}'=> $attrs['user_id'] ,
                '{user}'=> $attrs['name'], 
                '{email_admin}' => $admin_email,
                '{client_email}' => $attrs['email_to'] ,
                '{suma}'=> $attrs['suma'] ,
                '{type_p}'=> $attrs['pay_type'] 
            ];
            } 
           
            
              $template  = self::type_check($attrs['type']);
             
              $transport = new \Swift_SendmailTransport('/usr/sbin/sendmail -bs');
              $mailer = new \Swift_Mailer($transport);
              $decorator = new \Swift_Plugins_DecoratorPlugin($replacements);
              $mailer->registerPlugin($decorator);
           
              $email_to   = $attrs['email_to'];
             // if($attrs['type'] == 'payment_admin' ) $email_to ="test@gmail.com";
           
               // Create a message
              $message = (new \Swift_Message('Iptv'))
                  ->setFrom([$admin_email => 'Iptv '])
                  ->setBody($template,'text/html')
                  ->setTo([$email_to => 'Dear customer']);
              $mdata =  $message->toString();
            //echo $mdata;
             
            if(!$mailer->send($message)){
               
                  return rest_ensure_response(['data'=> 'Your email not sended' ,'status'=> false ]);
              }
             else{
                  return rest_ensure_response(['data'=> 'Your email  sended' ,'status' =>true ]);
              }
          }
          catch( Exception $err){
              return rest_ensure_response($err);
          }

    }
}