<?php
  $routes = [
    //package
    ['methods' => 'GET', 'url' => '/streams', 'callback' => 'get_chanel_list'],
    ['methods' => 'POST', 'url' => '/packages', 'callback' => 'add_package'],
    ['methods' => 'GET', 'url' => '/packages', 'callback' => 'list_packages'],
    ['methods' => 'GET', 'url' => '/package-delete', 'callback' => 'delete_package'],
    ['methods' => 'GET', 'url' => '/package-edit', 'callback' => 'edit_category'],
    ['methods' => 'POST', 'url' => '/package-update', 'callback' => 'update_category'],

    //mpackage
    ['methods' => 'GET', 'url' => '/mpackages', 'callback' => 'get_mpackages'],
    ['methods' => 'GET', 'url' => '/mpackage-by', 'callback' => 'get_category_by'],
    ['methods' => 'GET', 'url' => '/mpackage-remove-by', 'callback' => 'remove_category_by'],
    ['methods' => 'POST', 'url' => '/mpackage', 'callback' => 'add_main_package'],
    ['methods' => 'GET', 'url' => '/mpackage-delete', 'callback' => 'mpackage_delete'],
    ['methods' => 'POST', 'url' => '/mpackage-add-category', 'callback' => 'add_package_category'],
    ['methods' => 'POST', 'url' => '/update-price', 'callback' => 'package_price_update'],

    //chanel 

    ['methods' => 'GET', 'url' => '/chanels', 'callback' => 'list_chanels_by_package'],
    ['methods' => 'POST', 'url' => '/chanels', 'callback' => 'update_package_list'],
    ['methods' => 'POST', 'url' => '/chanel-update', 'callback' => 'update_chanel'],
    ['methods' => 'POST', 'url' => '/chanel-delete', 'callback' => 'chanel_delete'],

    //add_chanels_to_category
    ['methods' => 'POST', 'url' => '/chanel-add', 'callback' => 'add_chanels_to_category'],

    //user
    ['methods' => 'POST', 'url' => '/user', 'callback' => 'add_user'],
    ['methods' => 'GET', 'url' => '/users', 'callback' => 'add_all_users'],
    ['methods' => 'POST', 'url' => '/user-package-update', 'callback' => 'user_package_update'],
    ['methods' => 'POST', 'url' => '/user-status', 'callback' => 'user_status'],
    ['methods' => 'POST', 'url' => '/account-delete', 'callback' => 'delete_user_account'],
    ['methods' => 'POST', 'url' => '/update-expire', 'callback' => 'update_expire'],
    ['methods' => 'POST', 'url' => '/update-adult', 'callback' => 'update_adult'],
    ['methods' => 'POST', 'url' => '/update-password', 'callback' => 'update_user_password'],
    ['methods' => 'GET', 'url' => '/active-user', 'callback' => 'active_user'],
    ['methods' => 'GET', 'url' => '/check-user-active', 'callback' => 'if_user_active'],
    //stalker
    ['methods' => 'POST', 'url' => '/add-stalker', 'callback' => 'add_stalker_account'],
    ['methods' => 'POST', 'url' => '/change-stalker', 'callback' => 'change_stalker_account'],
    ['methods' => 'GET', 'url' => '/get-stalker', 'callback' => 'get_stalker_account'],
    ['methods' => 'POST', 'url' => '/change-stalker-tariff', 'callback' => 'change_stalker_package'],
    ['methods' => 'GET', 'url' => '/delete-stalker', 'callback' => 'delete_stalker_account'],
    ['methods' => 'GET', 'url' => '/all-tariff', 'callback' => 'get_all_tariff'],
    ['methods' => 'GET', 'url' => '/stalker-expire', 'callback' => 'change_data_expire_stalker'],

    //auth
    ['methods' => 'POST', 'url' => '/login', 'callback' => 'loginIn'],
    ['methods' => 'POST', 'url' => '/logout', 'callback' => 'Logout'],
    ['methods' => 'GET', 'url' => '/restore', 'callback' => 'restorePassword'],
    //setting
    ['methods' => 'POST', 'url' => '/setting', 'callback' => 'save_setting'],
    ['methods' => 'GET', 'url' => '/setting', 'callback' => 'get_setting'],
    //  ['methods'=>'GET','url'=>'/balance','callback'=>'fill_balance'],
    ['methods' => 'GET', 'url' => '/get-balance', 'callback' => 'account_balance_id'],
    ['methods' => 'GET', 'url' => '/account', 'callback' => 'user_account'],
    ['methods' => 'POST', 'url' => '/update-balance', 'callback' => 'update_balance'],
    ['methods' => 'POST', 'url' => '/user-limit', 'callback' => 'user_limit_sesstion'],
    ['methods' => 'POST', 'url' => '/update-token', 'callback' => 'update_token'],
    ['methods' => 'GET', 'url' => '/get-all-subaccount', 'callback' => 'get_all_subaccounts'],
    ['methods' => 'POST', 'url' => '/add-subaccount', 'callback' => 'add_subaccount'],
    ['methods' => 'POST', 'url' => '/delete-subaccount', 'callback' => 'delete_sub_token'],
    //token auth
    ['methods' => 'GET', 'url' => '/auth', 'callback' => 'token_check'],
    //playlist
    //   (?P<market>[a-zA-Z0-9-]+)/lat=(?P<lat>[a-z0-9 .\-]+)/long=(?P<long>[a-z0-9 .\-]+)
    ['methods' => 'GET', 'url' => '/playlist', 'callback' => 'playlist'],
    ['methods' => 'GET', 'file'=>'file', 'url' => '/package-urls/(?P<hash>[a-zA-Z0-9-]+)/(?P<type>[a-zA-Z0-9-]+)/(?P<ftype>.+)', 'callback' => 'package_urls'],
    ['methods' => 'GET', 'url' => '/check-email', 'callback' => 'check_email'],
    ['methods' => 'POST', 'url' => '/package-title', 'callback' => 'package_title_update'],
    ['methods' => 'POST', 'url' => '/user-desc-update', 'callback' => 'user_desc_update'],
    ['methods' => 'POST', 'url' => '/update-archive', 'callback' => 'update_archive'],
    ['methods' => 'POST', 'url' => '/update-logo', 'callback' => 'upload_chanel_logo'],
    ['methods' => 'GET', 'url' => '/server-status', 'callback' => 'server_status'],
    ['methods' => 'GET', 'url' => '/all-chanels', 'callback' => 'getAllChanels'],
    ['methods' => 'POST', 'url' => '/chanel-detail', 'callback' => 'chanel_detail'],
    //payment type
    ['methods' => 'GET', 'url' => '/stripe-pay', 'callback' => 'apiStripe'],
    ['methods' => 'GET', 'url' => '/2checkout-pay', 'callback' => 'apiCheckout2'],
    ['methods' => 'GET', 'url' => '/coinbase-pay', 'callback' => 'apiCoinBase'],
    ['methods' => 'GET', 'url' => '/blockchain-pay', 'callback' => 'apiBlockchain'],
    ['methods' => 'GET', 'url' => '/payssiera-pay', 'callback' => 'apiPaySsiera'],
    ['methods' => 'GET', 'url' => '/coinbase-auth', 'callback' => 'authCoinbase'],
    ['methods' => 'GET', 'url' => '/coinbase-data', 'callback' => 'coinbaseData'],
    
    //statistic
    ['methods' => 'GET', 'url' => '/popular-chanels', 'callback' => 'popular_chanels'],
    ['methods' => 'GET', 'url' => '/version-plugin', 'callback' => 'check_table_version'],
    ['methods' => 'GET', 'url' => '/email', 'callback' => 'emailSend'],
    //iptv portal 
   // ['methods' => 'GET', 'url' => '/auth-portal', 'callback' => 'auth_portal'],
    ['methods' => 'GET', 'url' => '/add-user-portal', 'callback' => 'add_user_portal'],
    ['methods' => 'GET', 'url' => '/disable-user-portal', 'callback' => 'disable_user_portal'],
    ['methods' => 'GET', 'url' => '/change-package-portal', 'callback' => 'disable_user_portal'],

];