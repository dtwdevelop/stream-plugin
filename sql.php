<?php

function iptv_install() {
	global $wpdb;
	$ip_db_version  = '1.0';
	$table_packages = $wpdb->prefix . 'iptv_packages';
	$table_categories = $wpdb->prefix . 'iptv_categories';
	$table_chanels = $wpdb->prefix . 'iptv_chanels';
	$table_accounts = $wpdb->prefix . 'iptv_accounts';
	$table_balance = $wpdb->prefix . 'iptv_balance';
	$table_transaction = $wpdb->prefix . 'iptv_transation';
	$table_tokens = $wpdb->prefix . 'iptv_tokens';
	$table_stalker = $wpdb->prefix . 'iptv_stalker';
	$table_statistics = $wpdb->prefix . 'iptv_statistics';
	$table_statistics_archive = $wpdb->prefix . 'iptv_statistics_archive';
	$table_cross = $wpdb->prefix . 'iptv_packages_categories';
    $charset_collate = $wpdb->get_charset_collate();
	
	
    // create table packages
    $sql1= "CREATE TABLE IF NOT EXISTS $table_packages (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		package_id mediumint(9) NOT NULL ,
		user_ID mediumint(9) NOT NULL ,
		tariff_id mediumint(9) NOT NULL ,
	    title tinytext NOT NULL,
		description TEXT NOT NULL,
		price float(4)  NOT NULL,
		archive tinyint(4) NOT NULL,
	    expire datetime DEFAULT '2000-01-01 01:10:10' NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate; ";

   // create table categories
	$sql2= "CREATE TABLE IF NOT EXISTS $table_categories (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		 title tinytext NOT NULL,
		description TEXT NOT NULL,
		total tinyint(9)  NOT NULL,
		expire datetime DEFAULT '2000-01-01 01:10:10' NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate; ";

        // cross table
		$sql3= "CREATE TABLE IF NOT EXISTS $table_cross(
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			package_id  mediumint(9) NOT NULL ,
			category_id  mediumint(9) NOT NULL ,
			PRIMARY KEY  (id)
		) $charset_collate; ";

	  // create chanels
	  $sql4= "CREATE TABLE IF NOT EXISTS $table_chanels (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		package_id mediumint(9) NOT NULL ,
	    title tinytext NOT NULL,
		dataid varchar(255) NOT NULL,
		chanel tinytext NOT NULL,
		logo  varchar(255) DEFAULT '2000-01-01 01:10:10'  NOT NULL, 
		link   varchar(255) NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate; ";

	 //create table account iptv
	 $sql5= "CREATE TABLE IF NOT EXISTS $table_accounts (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		user_id mediumint(9) DEFAULT '0' NOT NULL,
		category_id mediumint(9) NOT NULL,
	    ip tinytext NOT NULL ,
		keyhash varchar(255) NOT NULL,
		session_limit  mediumint(9) DEFAULT '1' NOT NULL,
		description varchar(255) NOT NULL,
		status varchar(6) NOT NULL ,
		adult   varchar(6) NOT NULL ,
		expire datetime DEFAULT '2000-01-01 01:10:10' NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate; ";


// create table balance
$sql6= "CREATE TABLE IF NOT EXISTS $table_balance (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	user_id mediumint(9) NOT NULL ,
	balance float(4)  NOT NULL,
    created datetime DEFAULT '2000-01-01 01:10:10' NOT NULL,
	updated datetime DEFAULT '2000-01-01 01:10:10' NOT NULL,
	PRIMARY KEY  (id)
) $charset_collate; ";

// create table transaction
$sql7 = "CREATE TABLE IF NOT EXISTS $table_transaction (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	user_id mediumint(9) NOT NULL ,
	balance_id mediumint(9) NOT NULL ,
	payment float(4)  NOT NULL,
	hashpay tinytext NOT NULL,
	typepay tinytext NOT NULL,
    created datetime DEFAULT '2000-01-01 01:10:10' NOT NULL,
	PRIMARY KEY  (id)

) $charset_collate; ";

//table token
$sql8 = "CREATE TABLE IF NOT EXISTS $table_tokens (
	    id mediumint(9) NOT NULL AUTO_INCREMENT,
		token_user varchar(255) NOT NULL,
		token varchar(255) NOT NULL,
		expire datetime DEFAULT '2000-01-01 01:10:00' NOT NULL,
		name   varchar(255)  NOT NULL,
		type enum('play', 'stalker', 'portal') NOT NULL,	
		PRIMARY KEY  (id)
    ) $charset_collate; ";

// table stalker
	$sql9 = "CREATE TABLE IF NOT EXISTS $table_stalker (
	    id mediumint(9) NOT NULL AUTO_INCREMENT,
		user_id varchar(255) NOT NULL,
		stalker_id varchar(255) NOT NULL,
		created datetime DEFAULT '2000-01-01 01:10:00' NOT NULL,
		stalker_p varchar(255) NOT NULL,
		PRIMARY KEY  (id)

	) $charset_collate; ";

// table stat chanel
	$sql10 = "CREATE TABLE IF NOT EXISTS $table_statistics (
	    id mediumint(9) NOT NULL AUTO_INCREMENT,
		chanel  varchar(255) NOT NULL,
		agent varchar(255) NOT NULL,
		ip varchar(255) NOT NULL,
		stream_clients mediumint(9) NOT NULL,
		total mediumint(9) DEFAULT '1' NOT NULL ,
		created datetime DEFAULT '2000-01-01 01:10:00' NOT NULL,
	    PRIMARY KEY  (id)
	   ) $charset_collate; ";
	   
	   // table stat archive
	$sql11 = "CREATE TABLE IF NOT EXISTS $table_statistics_archive (
	    id mediumint(9) NOT NULL AUTO_INCREMENT,
		chanel  varchar(255) NOT NULL,
		agent varchar(255) NOT NULL,
		ip varchar(255) NOT NULL,
		stream_clients mediumint(9) NOT NULL,
		total mediumint(9)  DEFAULT '1' NOT NULL ,
		created datetime DEFAULT '2000-01-01 01:10:00' NOT NULL,
	    PRIMARY KEY  (id)
       ) $charset_collate; ";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$queries =[$sql1 ,$sql2, $sql3, $sql4, $sql5, $sql6 , $sql7, $sql8, $sql9, $sql10, $sql11];
	foreach($queries as $query){
		dbDelta($query);
	}

	add_option( 'iptv_db_version', $ip_db_version );
}


register_deactivation_hook(__FILE__,'iptv_uninstall');
register_uninstall_hook(__FILE__,'iptv_uninstall');

function iptv_uninstall(){
	die;
	 global $wpdb;
     $pre  = $wpdb->prefix;
	 $tables_delete = [ $pre."iptv_packages_categories",  $pre."iptv_packages", $pre."iptv_categories", $pre."iptv_chanels",
	 $pre."iptv_balance" , $pre."iptv_transation" ,$pre."iptv_tokens", $pre."iptv_stalker",
	 $pre."iptv_statistics", $pre."iptv_statistics_archive", $pre."iptv_accounts"];
     foreach ($tables_delete as $tablename) {
		$wpdb->query("DROP TABLE IF EXISTS $tablename");
	 }
	if(get_option('iptv_db_version')){
		delete_option("iptv_db_version");
	}
}


