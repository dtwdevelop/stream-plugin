<?php

namespace Iptv;

use Exception;

class ServiceRemote
{
    const API_AUTH_LINK  = "/jsonrpc/" ;
    const APIT_SQL_LINK  =   "/jsonsql/";

   /**
    * auth  get session id
    */
    public static function auth_portal($data = null)
    {
       
        try {
            $options   = get_option("option_iptv");

            $databody  =   [

                "jsonrpc" => "2.0",
                "id" => 1.0,
                "method" => "authorize_user",
                "params" => [
                    "username" => $options['portal_login'],
                    "password" => $options['portal_password']
                ]
            ];
            $params =   [
                'sslverify' => false,
                'body' => json_encode($databody),
                'headers' => [
                    'Content-Type' => 'application/json'

                ],
                'method'    => 'POST',
                ['timeout' => 60]
            ];

            $url = $options['portal_link_api'].self::API_AUTH_LINK ;
            $body = self::send_request($params, $url);
            $json  = json_decode($body);

            return $json->result->session_id;
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    

    /**
     * get all package from iptv portal
     */
    public static function get_packages()
    {
        try {
            $options   = get_option("option_iptv");
            $sqldata = [
                "jsonrpc" => "2.0",
                "id" => 2,
                "method" => "select",
                "params" => [
                "data" => ["id","name"],
                "from" => "package"
                ]
        ];
      

            $token = self::auth_portal();
           
          
            $params =   [
                'sslverify' => false,
                'body' => json_encode($sqldata),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Iptvportal-Authorization' => "sessionid=$token"
                ],
                'method'    => 'POST',
                ['timeout' => 120]
            ];
           $url  =  $options['portal_link_api'] .self::APIT_SQL_LINK;
            $response =  self::send_request($params , $url);
           return json_decode($response)->result;
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * add user to iptv portal
     */
    public static function add_user($data)
    {
        $options   = get_option("option_iptv");
        try {
            $databody  =   [
                "jsonrpc" => "2.0",
                "id" => 1,
                "method" => "insert",
                "params" => [
                    "into" => "subscriber",
                    "columns" => ["username", "password", "surname", "email"],
                    "values" => [["{$data['login']}", "{$data['password']}", "{$data['full_name']}", "test@duga.tv"]],
                    "returning" => "id"
                ]
            ];
            $token = self::auth_portal();
          
            $params =   [
                'sslverify' => false,
                'body' => json_encode($databody),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Iptvportal-Authorization' => "sessionid=$token"
                ],
                'method'    => 'POST',
                ['timeout' => 120]
            ];
           $url  =  $options['portal_link_api'] .self::APIT_SQL_LINK;
           $response =  self::send_request($params,$url);
            
           $response  = json_decode($response)->result;
          
          $res = self::add_package_subscriber($response[0],$data['tariff_plan'],$token);
        
           
           return $res  ;
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * 5177591
     */
    public static function add_package_subscriber($user_id,$package_id ,$token){
        try{
            $options   = get_option("option_iptv");
            $databody  =   [
                "jsonrpc" => "2.0",
                "id" => 2,
                "method" => "insert",
                "params" => [
                    "into" => "subscriber_package",
                    "columns" => ["subscriber_id", "package_id"],
                    "values" => [[$user_id, $package_id]],
                    "returning" => "id"
                ]
            ];
            $params =   [
                'sslverify' => false,
                'body' => json_encode($databody),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Iptvportal-Authorization' => "sessionid=$token"
                ],
                'method'    => 'POST',
                ['timeout' => 120]
            ];
           $url  =  $options['portal_link_api'] .self::APIT_SQL_LINK;
           $response =  self::send_request($params,$url);
           return $response;

        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

    /**
     * delete user from portal
     */
    public function remove_user($user_name){
        try{
            $options   = get_option("option_iptv");
            $dataparams = [
                "jsonrpc" => "2.0",
                "id" => 2,
                "method" => "delete",
                "params" => [
                "from" => "subscriber",
                 "where" =>  [ "eq" => ["username" , "$user_name" ] ] ,
                "returning" => "id" ,
                ] ];
                
            $token = self::auth_portal();
          
            $params =   [
                'sslverify' => false,
                'body' => json_encode($dataparams),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Iptvportal-Authorization' => "sessionid=$token"
                ],
                'method'    => 'POST',
                ['timeout' => 120]
            ];
           $url  =  $options['portal_link_api'] .self::APIT_SQL_LINK;

           $response =  self::send_request($params , $url);
           return $response;
    }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

    
    /**
     * change user package in portal
     */
    public static function chnage_package($user,$package){
        try{
           
           $options   = get_option("option_iptv");
           $dataparams = [
                "jsonrpc" => "2.0",
                "id" => 3,
                "method" => "update",
                "params" => [
                "table"  => "subscriber_package",
                "set" =>  [ "package_id"  => $package],
                "where" => ["in" => ["subscriber_id", [
                "select" => [
                "data" => "id",
                "from"=> "subscriber",
                "where"=> ["eq" => ["username", $user]]]
                ]]],
                "returning" => "id"
                ]];
                $token = self::auth_portal();
          
                $params =   [
                    'sslverify' => false,
                    'body' => json_encode($dataparams),
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Iptvportal-Authorization' => "sessionid=$token"
                    ],
                    'method'    => 'POST',
                    ['timeout' => 120]
                ];
               $url  =  $options['portal_link_api'] .self::APIT_SQL_LINK;
    
               $response =  self::send_request($params , $url);
               return $response;


        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

    /**
     * disable user from iptv portal
     */
    public static function disable_user($user ,$status)
    {
        try {
            $options   = get_option("option_iptv");
           $status  = ($status == 'false')?'true':'false';
           
           $dataparams = [
                "jsonrpc" => "2.0",
                "id" => 2,
                "method" => "update",
                "params" => [
                "table" => "subscriber",
                "set" => [ "disabled" => $status],
                "where" =>  [ "eq" => ["username" , "$user" ] ] ,
                "returning" => "id" ,
                ]
        ];
      

            $token = self::auth_portal();
          
            $params =   [
                'sslverify' => false,
                'body' => json_encode($dataparams),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Iptvportal-Authorization' => "sessionid=$token"
                ],
                'method'    => 'POST',
                ['timeout' => 120]
            ];
           $url  =  $options['portal_link_api'] .self::APIT_SQL_LINK;

           $response =  self::send_request($params , $url);
           return $response;

        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * send request to iptv portal
     */
    public static function send_request($databody, $url)
    {
        $data  =  wp_remote_request($url, $databody);
        $body = wp_remote_retrieve_body($data);
       
        return $body;
    }
}
