axios.defaults.headers.common['Iptvkey'] =  window.btoa("iptv4567");
const store = new Vuex.Store({
    state: {
        clients: [],
        chanels: [],
        packages: [],
        tariffs : [],
        all_chanels :[],
        logIn: "",
        mpackages: [],
        loaded: true,
        emailValid : false,
        statserver: [],
        chanel_detail :  0,
        popular_chanels : [],
        popular_chanels_archive :[],
        sub_accounts : [],
        pagination: {
            descending: true,
            page: 1,
            itemsPerPage: 20,
            totalItems: 0,
           
          },

        options: {
            login: "",
            password: "",
            url: "",
        },

        account: {
            user: {

            },
            package: {

            }
        },
        status: false,
        balance: 0,
        linkUrl: "",
        linkUrls: [],
        loaded: true,
        categories: [],
        userLogin :{
            error :false,
            message : ""
        } ,
        pricer: 0,
        key: null, 
        user : null,
        login_error: null,
        stalker :{
            status :false,
            account : {}
        },
        categoriesby : [],
        categoriesby_id : null,
        search : '',
        auth : {
            token :  null,
            state : null,
           
        },
        user_active : null
      
        
 },
   getters:{
    pagination(state){
        return state.pagination
    },
    search(state){
        return state.search
    },

    auth(state){
      return state.auth
    }
   },
    mutations: {
        load_packages(state, payload) {
            state.packages = payload.packages
        },
        auth_change(state, payload) {
            state.auth = {
                token :  payload.token,
                state : payload.state,
               
            }
            window.localStorage.setItem("token",payload.token)
            window.localStorage.setItem("statecode",payload.state)
           
          
         },
        search_set(state,payload){
          state.search  =payload.s
        },
        change_category_by(state, payload) {
            state.categoriesby_id= payload.cat
        },
        load_links(state, payload) {
            state.linkUrls = [
                { url: 'http://' + document.domain + '/wp-json/iptv/package-urls', type: 'm3u8', ext: '.m3u8' },
                { url: 'http://' + document.domain + '/wp-json/iptv/package-urls', type: 'm3u', ext: '.m3u' },
                { url: 'http://' + document.domain + '/wp-json/iptv/package-urls', type: 'ottplayer', ext: '.m3u8' },
                { url: 'http://' + document.domain + '/wp-json/iptv/package-urls', type: 'siptv', ext: '.m3u' },
                { url: 'http://' + document.domain + '/wp-json/iptv/package-urls', type: 'ssiptv', ext: '.m3u' },
                { url: 'http://' + document.domain + '/wp-json/iptv/package-urls', type: 'ottplayerm', ext: '.m3u8' },
            ]
            state.loaded = false
        },

        load_chanels(state, payload) {
            state.chanels = payload.list
        },
        load_option(state, payload) {


        },
        set_empty(state, payload) {
            state.linkUrls = []
            state.loaded = false
        },
        change_balance(state, payload) {
            state.balance = payload.price
        },
        change_chanel(state, payload) {
            
            state.chanel_detail = payload.total
        },

        test_price(state,payload){
               state.pricer =payload.price
        }
    },

    actions: {

        getAuth({commit,state}){
         const data =  {
         token :window.localStorage.getItem("token") , 
         statecode : window.localStorage.getItem("statecode")
        }
         return data
        },
        /**
         * 
         * @param {*} param0 
         * Load packages 
         */
        async  loadPackages({ commit, state }) {
            try {
                const packagelist = []

                const res = await axios.get('/wp-json/iptv/packages')
                // console.log(res.data)
                res.data.forEach(function (pack) {
                    let obj = {
                        id: pack.id,
                        name: pack.title,
                        description: pack.description,
                        total: pack.total,
                        price: pack.price

                    }
                    packagelist.push(obj);
                })

                commit('load_packages', { packages: packagelist })


            }
            catch (err) {
                console.log("Error load package from server")
            }

        },
        /**
         * 
         * @param {*} param0 
         * @param {*} payload 
         * delete package by id
         */
        async  DeletePackage({ commit, state, dispatch }, payload) {
            try {

                for (let package in state.packages) {
                    if (state.packages[package].id == payload.id) {
                        state.packages.splice(package, 1)
                    }
                }
                const res = await axios.get('/wp-json/iptv/package-delete', { params: { id: payload.id } })
                //dispatch("loadPackages")

            }
            catch (err) {
                console.log("Error get responce from server" + err)
            }
        },
        /**
         * add chanels 
         * @param {*} param0 
         */
        async  ChanelsFromJson({ commit, state }) {
            try {
                const chanels = []
                const res = await axios.get('/wp-json/iptv/streams/?param=media')
                res.data.forEach((val) => {
                    let package = { chanel: val.chanel, title: val.title, dataid: "" }

                    chanels.push(package)
                    state.loaded = false
                })
                commit('load_chanels', { list: chanels })
            }
            catch (err) {
                console.log("Error get responce from server")
            }
        },


        /**
         * add Categories
         * @param {*} param0 
         * @param {*} payload 
         */
        async addPackage({ commit, state, dispatch }, payload) {


            await axios.post('/wp-json/iptv/packages', { payload }).then(function (res) {

                let pack = {
                    id: res.last_id,
                    name: payload.package.title,
                    description: payload.package.desc,
                    total: payload.package.select.length
                }
                state.packages.push(pack)
                dispatch("loadPackages")
            }).catch(function (erro) {

            })
        },
        /**
         * add package
         * @param {*} param0 
         * @param {*} payload 
         */
        async addMPackage({ commit, state, dispatch }, payload) {
            await axios.post('/wp-json/iptv/mpackage', { payload }).then(function (res) {
                dispatch("getMPackage")
            }).catch(function (erro) {

            })
        },
        /**
         * Get package
         * @param {*} param0 
         * @param {*} payload 
         */
        async getMPackage({ commit, state, dispatch }, payload) {
            await axios.get('/wp-json/iptv/mpackages').then(function (res) {

                state.mpackages = res.data
            }).catch(function (erro) {
            })
        },

        /**
         * delete package
         * @param {*} param0 
         * @param {*} payload 
         */
        async DeleteMPackage({ commit, state, dispatch }, payload) {
            await axios.get('/wp-json/iptv/mpackage-delete?id=' + payload.id).then(function (res) {

                state.mpackages = res.data
            }).catch(function (erro) {
            })
        },

        /**
         * User register
         * @param {*} param0 
         * @param {*} client 
         */
        async UserRegistration({ commit, state, }, client) {
        try{
            const res  =  await axios.post('/wp-json/iptv/user', { client })
            
            if(res.data.error){
                state.userLogin = { error:true, message :res.data.error.existing_user_login }
            }
            else{
                state.userLogin ={ error:false ,message: "" }
            }

         }
           
           catch(err) {
                console.log("Registration failed " + err)
            }
        },

        async userCheck({ commit, state, }, payload) {
            try{
                const res  = await axios.get('/wp-json/iptv/check-user-active?id='+payload.id)
                return res.data.status
            }
           catch(err) {
              console.log(err)
            }
        },

        /**
         * User login
         * @param {*} param0 
         * @param {*} client 
         */
        async UserLogin({ commit, state, dispatch}, client) {
            try{

             //const wp  = wpApiSettings.nonce
            const params = {
                login: client.login,
                password: client.password,
                _wpnonce: "x4678ijhl"
            }

            const res =  await axios.post('/wp-json/iptv/login', params)
             if (res.data != null) {
                   state.user = res.data
                 
                  
                   dispatch("Account", { id: res.data.ID })
                    state.status = false
                  return  dispatch("userCheck",{id: res.data.ID}).then(res=>{
                       return res
                    })
                  
                 }
              
            }
            catch(err) {
              state.status = true
            }
        },

        /**
         * Log out
         * @param {*} param0 
         * @param {*} payload 
         */
        async Logout({ commit, state, }, payload) {
            try{
                const res  = await axios.post('/wp-json/iptv/logout')
                state.user = null
            }
           catch(err) {
              console.log(err)
            }
        },

        /**
         * Save setting
         * @param {*} param0 
         * @param {*} setting 
         */
        async SaveSetting({ commit, state, }, setting) {
            await axios.post('/wp-json/iptv/setting', { setting }).then(function (res) {
            }).catch(function (err) {
                console.log("Setting save failed " + err)
            })
        },

        /**
         * get Setting
         * @param {*} param0 
         */
        async GetSetting({ commit, state, }) {
            await axios.get('/wp-json/iptv/setting').then(function (res) {
                const data = res.data

                if (res.data) {
                    const options = {
                        login: data.login,
                        password: data.password,
                        url: data.url,
                        sub_url: data.sub_url,
                        domain: data.domain,
                        epg: data.epg,
                        login_stalker: data.login_stalker,
                        password_stalker: data.password_stalker,
                        url_stalker: data.url_stalker,
                        paypal_token : data.paypal_token,
                        paypal_status : data.paypal_status,
                        stripe_token : data.stripe_token,
                        stripe_token2 : data.stripe_token2,
                        stripe_status : data.stripe_status,
                        checkout2_key : data.checkout2_key,
                        checkout2_merchant : data.checkout2_merchant,
                        checkout2_status : data.checkout2_status,
                        stat_link : data.stat_link,
                        coinbase_key:data.coinbase_key,
                        coinbase_secret: data.coinbase_secret,
                        coinbase_status : data.coinbase_status,
                        coinbase_client_k :data.coinbase_client_k,
                        coinbase_private_k : data.coinbase_private_k,
                        coinbase_redirect : data.coinbase_redirect,
                        sub_limit : data.sub_limit,
                        portal_login : data.portal_login,
                        portal_password : data.portal_password,
                        portal_link_api   : data.portal_link_api,
                        btn_portal_status : data.btn_portal_status

                    }
                    state.options = options

                }

            }).catch(function (err) {
                console.log("Setting save failed " + err)
            })
        },

        /**
         * Client  in back end
         * @param {*} param0 
         */
        async  Clients({ commit, state},payload) {
            try {
                
                const clients = []
                const datenow  = moment().format("YYYY-MM-DD")
               
                const res = await axios.get('/wp-json/iptv/users?page='+payload.pages+'&limit='+payload.limit)
                res.data.users.forEach(user => {
                   
                    const status = ( user.expire >= datenow ) ? true : false
                    let objuser = {
                        id: user.id,
                        name: user.user_login,
                        email: user.user_email,
                        expire: user.expire,
                        packet: user.category_id,
                        status: user.status,
                        user_id: user.user_id,
                        keyhash: user.keyhash,
                        description : user.description,
                        user_limit : user.session_limit,
                        ip : user.ip,
                        light : status
                       
                    }

                    clients.push(objuser)
                })
                state.pagination.totalItems = parseInt(res.data.total,10)
                // if (payload.q != '') {
                //   state.clients = state.clients.filter(function (item) {
                //          return Object.values(item).join(",").toLowerCase().includes(payload.q);
                //      });
                //     }
                 
                        state.clients = clients
                    

                }
            catch (err) {
                console.log("Server error load all users " + err)
            }
        },

        /**
         * User account
         * @param {*} param0 
         * @param {*} payload 
         */
        async  Account({ commit, state,dispatch }, payload) {
            try {
                const acc = {}
                const res = await axios.get('/wp-json/iptv/account?id=' + payload.id)
                const datenow  = moment().format("YYYY-MM-DD")

                res.data.user.forEach(el => {
                    const status = ( el.expire >= datenow ) ? true : false
                    const user_acc = {
                        id : el.id ,
                        user_login: el.user_login,
                        ip: el.ip,
                        keyhash: el.keyhash,
                        expire: el.expire,
                        package_id: el.category_id,
                        user_id  : el.user_id,
                        adult :el.adult,
                        email : el.email,
                        light : status,
                        status : el.status,
                        user_status :el.user_status
                        
                    }
                    state.account.user = user_acc
                })
                res.data.package.forEach(el2=>{
                    const pack ={
                    package_id : el2.id,
                    title  : el2.title,
                    price : el2.price
                    }
                    state.account.package  = pack
                })

               const balance = res.data.balance.data
                    state.balance = balance.balance
                    dispatch("checkStalkerAccount",{param : state.account.user.id})

         }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * Delete user account
         * @param {*} param0 
         * @param {*} payload 
         */
        async  DeleteAccount({ commit, state, dispatch }, payload) {
            try {
                const acc = {}
               state.clients = state.clients.filter((item)=>{
                    return item.id != payload.id
                })
                const res = await axios.post('/wp-json/iptv/account-delete', { user: payload.user ,id: payload.id })


            }
            catch (err) {
                console.log("error " + err)
            }
        },
        /**
         * Load playlist url
         * @param {*} param0 
         * @param {*} payload 
         */
        async  PlaylistUrl({ commit, state }, payload) {
            try {
                const acc = {}
                const res = await axios.get('/wp-json/iptv/chanels?param=' + payload.id + '&type=' + payload.type)
                state.linkUrl = res.data.link.url
                state.status = true

            }
            catch (err) {
                console.log("error " + err)
            }
        },
        /**
         * Play list urls
         * @param {*} param0 
         * @param {*} payload 
         */
        async  PlaylistUrls({ commit, state }, payload) {
            try {
                const acc = {}
                const res = await axios.get('/wp-json/iptv/package-urls?param=' + payload.param)
                //  state.linkUrls  = res.data,
                state.loaded = true


            }
            catch (err) {
                console.log("error " + err)
                state.linkUrls = []
            }
        },

        /**
         * Status
         * @param {*} param0 
         * @param {*} payload 
         */
        async  Status({ commit, state }, payload) {
            try {
                state.status = payload.status
                state.linkUrl = ''

            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * Update package by user id
         * @param {*} param0 
         * @param {*} payload 
         */
        async  UpdateUserPackage({ commit, state }, payload) {
            try {
                const acc = {}
                const res = await axios.post('/wp-json/iptv/user-package-update', { user_id: payload.user_id, package_id: payload.package_id })

            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * Update user statsus
         * @param {*} param0 
         * @param {*} payload 
         */
        async  UpdateUserStatus({ commit, state }, payload) {
            try {
                const acc = {}

                const res = await axios.post('/wp-json/iptv/user-status', { user_id: payload.user_id, status: payload.status, token:payload.token })
               

            }
            catch (err) {
                console.log("error " + err)
            }
        },
       /**
        * Load categories
        * @param {*} param0 
        * @param {*} payload 
        */
        async  loadEditCategory({ commit, state }, payload) {
            try {
                const acc = {}

                const res = await axios.get('/wp-json/iptv/package-edit?id=' + payload.id)
                state.categories = res.data


            }
            catch (err) {
                console.log("error " + err)
            }
        },

       /**
        * Update chanel
        * @param {*} param0 
        * @param {*} payload 
        */
        async  UpdateChanel({ commit, state }, payload) {
            try {
                const acc = {}
                const res = await axios.post('/wp-json/iptv/chanel-update', { category: payload })
                console.log(res.data)
            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * update category
         * @param {*} param0 
         * @param {*} payload 
         */
        async  UpdateCategory({ commit, state }, payload) {
            try {
                const acc = {}
                const res = await axios.post('/wp-json/iptv/package-update', payload)
                console.log(res.data)
            }
            catch (err) {
                console.log("error " + err)
            }
        },
        /**
         * Delete chanel
         * @param {*} param0 
         * @param {*} payload 
         */
        async  deleteChanel({ commit, state }, payload) {
            try {
                const acc = {}
                const res = await axios.post('/wp-json/iptv/chanel-delete', payload)
                console.log(res.data)
            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * add chanel
         * @param {*} param0 
         * @param {*} payload 
         */
        async  addChanel({ commit, state,dispatch }, payload) {
            try {

                const res = await axios.post('/wp-json/iptv/chanel-add', payload)
               // console.log(res.data)
                dispatch("loadPackages")
            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * check email validate
         * @param {*} param0 
         * @param {*} payload 
         */
        async checkEmail({ commit, state }, payload){
            try {
                const res = await axios.get('/wp-json/iptv/check-email?email='+payload.email)
                state.emailValid = res.data.email
                
            }
            catch (err) {
                console.log("error " + err)
            }
        },

       /**
        * update package
        * @param {*} param0 
        * @param {*} payload 
        */
        async updatePackageName({ commit, state }, payload){
            try {
                const res = await axios.post('/wp-json/iptv/package-title',payload)
                state.emailValid = res.data.email
                
            }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * change description
         * @param {*} param0 
         * @param {*} payload 
         */
        async changeDescription({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/user-desc-update',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },
        /**
         * change limit divice
         * @param {*} param0 
         * @param {*} payload 
         */
        async changeLimitDivice({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/user-limit',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },
        /**
         * change arvchive
         * @param {*} param0 
         * @param {*} payload 
         */
        async changeArchive({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/update-archive',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },
        /**
         * Upadate token
         * @param {*} param0 
         * @param {*} payload 
         */
        async updateToken({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/update-token',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },
        /**
         * add category by id
         * @param {*} param0 
         * @param {*} payload 
         */
        async addCategoryById({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/mpackage-add-category',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },

        /**
         * create stalker account
         * @param {*} param0 
         * @param {*} payload 
         */
        async addStalkerAccount({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/add-stalker',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },

        async changeStalkerAccount({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/change-stalker',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },

        async checkStalkerAccount({ commit, state }, payload){
            try{
                const res = await axios.get('/wp-json/iptv/get-stalker?param='+payload.param)
               
                  if(res.data.results != null){
                    state.stalker.status = true
                   
                    state.stalker.account = res.data.results[0]
                }
                else{
                    state.stalker.status = false
                }
                 
            }
            catch(err){
                console.log("error " + err)
            }
        },
       /**
        * Update expire date
        * @param {*} param0 
        * @param {*} payload 
        */
        async expireUpdate({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/update-expire',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },
       /**
        * send adult api
        * @param {*} param0 
        * @param {*} payload 
        */
        async sendAdult({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/update-adult',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },
        /**
         * update package price 
         * @param {*} param0 
         * @param {*} payload 
         */
        async updatePackagePrice({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/update-price',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },

        async updatePassword({ commit, state }, payload){
            try{
                const res = await axios.post('/wp-json/iptv/update-password',payload)
              
            }
            catch(err){
                console.log("error " + err)
            }
        },

        async updateBalance({ commit, state }, payload){
            try{
                commit('change_balance', { price: payload.balance })
                const res = await axios.post('/wp-json/iptv/update-balance',payload)
                
              
            }
            catch(err){
                console.log("error " + err)
            }

        },

        async UpdateStalkerPackage({ commit, state }, payload){
            try{
              
                const res = await axios.post('/wp-json/iptv/change-stalker-tariff',payload)
                 }
            catch(err){
                console.log("error " + err)
            }

        },

        /**
         * all chanels
         * @param {*} param0 
         * @param {*} payload 
         */
        async getAllChanels({ commit, state }, payload){
            try{
               
                const res = await axios.get('/wp-json/iptv/all-chanels',payload)

                state.all_chanels = res.data
                
         }
            catch(err){
                console.log("error " + err)
            }

        },

        async loadCategoryBy({ commit, state }, payload){
            try{
               
                const res = await axios.get('/wp-json/iptv/mpackage-by?id='+payload.id)

                state.categoriesby= res.data
                
         }
            catch(err){
                console.log("error " + err)
            }

        },

        async removeCategoryBy({ commit, state }, payload){
            try{
               
                const res = await axios.get('/wp-json/iptv/mpackage-remove-by?id='+payload.id+'&package_id='+payload.pac)
          }
            catch(err){
                console.log("error " + err)
            }

        },

        async PaymentStripe({ commit, state }, payload){
            try{
              //  console.log(payload.payment)
                const res = await axios.get('/wp-json/iptv/stripe-pay',payload)
            }
            catch(err){
                console.log("error " + err)
            }
        },

        async getKey({ commit, state }, payload){
            try{
              //  console.log(payload.payment)
                const res = await axios.get('/wp-json/iptv/stripe-pay?data='+payload.data)
                state.key = res.data.key
            }
            catch(err){
                console.log("error " + err)
            }
        },



        /**
         * 
         * @param get chanel detail chanel
         * @param {*} payload 
         */
        async getChanelsDetail({ commit, state }, payload){
            try{
               const res = await axios.post('/wp-json/iptv/chanel-detail',payload)
                res.data.forEach((el)=>{
                    
                   state.chanel_detail = el.value.stats.client_count
                  
                })
                
           }
            catch(err){
                console.log("error " + err)
            }

        },

        /**
         * 
         * @param {*} param0 
         * @param {*} payload 
         */
        async LoadSubAccounts({ commit, state }, payload){
            try{
               const res = await axios.get('/wp-json/iptv/get-all-subaccount?token='+payload.token)

               state.sub_accounts  = res.data
                
           }
            catch(err){
                console.log("error " + err)
            }

        },

        async addSubAccount({ commit, state }, payload){
            try{
               const res = await axios.post('/wp-json/iptv/add-subaccount', payload)
               const sub_acc = {
               id : res.data.id,
               token_user: res.data.sub_acc.token_user,
               token : res.data.sub_acc.token,
               type : res.data.sub_acc.type,
               name : res.data.sub_acc.name

               }
               state.sub_accounts.push(sub_acc)

              }
            catch(err){
                console.log("error " + err)
            }

        },

        async deleteSubAccount({ commit, state }, payload){
            try{
              state.sub_accounts = state.sub_accounts.filter(item =>{
                return  item.token != payload.token
                      
               })
               const res = await axios.post('/wp-json/iptv/delete-subaccount', payload)

              }
            catch(err){
                console.log("error " + err)
            }

        },

        
        /**
         * 
         * @param get popular chanels
         * @param {*} payload 
         */
        async getPopularChanels({ commit, state }, payload){
            try{
               const res = await axios.get('/wp-json/iptv/popular-chanels?limit='+payload.limit)
               state.popular_chanels = res.data
             }
            catch(err){
                console.log("error " + err)
            }

        },
        async getPopularArchive({ commit, state }, payload){
            try{
               const res = await axios.get('/wp-json/iptv/popular-chanels?limit='+payload.limit+'&archive='+payload.archive)
               state.popular_chanels_archive = res.data
             }
            catch(err){
                console.log("error " + err)
            }

        },

        async PaycoinbaseToken({ commit, state }, payload){
            try{
                const res = await axios.get('/wp-json/iptv/coinbase-auth?code='+payload.code+'&state='+payload.statecode)
                return res.data.token

            }
           
            catch(err){
                console.log("error " + err)
            }
              
       },

       async PaycoinbasePayment({ commit, state }, payload){
           try{
            const res = await axios.get('/wp-json/iptv/coinbase-pay?token='+payload.token+'&suma='+payload.suma)
            return res.data
           }
           catch(err){
            console.log("error " + err)
        }
       
          
   },


        /**
         * 
         * @param {*} param0 
         * @param {*} payload 
         */
        async sendToken({ commit, state }, payload){
            try{
               const res = await axios.get('/wp-json/iptv/2checkout-pay?token='+payload.token+'&suma='+payload.suma)
               return res.data
             }
            catch(err){
                console.log("error " + err)
            }

        },
        async  getStateCode({ commit, state },payload) {
            try {
                const res = await axios.get('/wp-json/iptv/coinbase-auth?user='+payload.user)
                return  res.data.url
              
           }
            catch (err) {
                console.log("error " + err)
            }
        },

        async getTariff({ commit, state }, payload){
            try{
               const res = await axios.get('/wp-json/iptv/all-tariff')
               const data_tariff  = []
               data_tariff.push({  id :  "", name : "-----------------"})
               res.data.results.forEach(data=>{
                 const tariff = {
                      id :  data.external_id,
                      name : data .name
                  }
                  data_tariff.push(tariff)
               })
              
               this.state.tariffs  = data_tariff
              
             }
            catch(err){
                console.log("error " + err)
            }

        },

        

       /**
        * get server data
        * @param {*} param0 
        */
        async  statusServer({ commit, state }) {
            try {
                const res = await axios.get('/wp-json/iptv/server-status')
                state.statserver = res.data
           }
            catch (err) {
                console.log("error " + err)
            }
        },

        /**
         * send email to user
         * @param {*} param0 
         */
        async  sendRestoreEmail({ commit, state },payload) {
            try {
                const res = await axios.get('/wp-json/iptv/restore?email='+payload.email)
                console.log(res.data)
           }
            catch (err) {
                console.log("error " + err)
            }
        },
        /**
         * 
         * @param {*} param0 
         * @param {*} payload 
         */
        async  sendEmailPackageComplete({ commit, state },payload) {
            try {
                const res = await axios.get('/wp-json/iptv/email?id='+payload.id+'&suma='+payload.suma+'&name='+payload.name+'&pack_name='+payload.pack+'&email='+payload.email+'type=package-confirm')
                console.log(res.data)
           }
            catch (err) {
                console.log("error " + err)
            }
        },

        async  sendBuyCreditComplete({ commit, state },payload) {
            try {
                const res = await axios.get('/wp-json/iptv/email?id='+payload.id+'&suma='+payload.suma+'&email='+payload.email+'&name='+payload.name+'type=payment'+'tpay='+payload.tpay)
                console.log(res.data)
           }
            catch (err) {
                console.log("error " + err)
            }
        },

        async  activateUser({ commit, state },payload) {
            try {
                const res = await axios.get('/wp-json/iptv/active-user?id='+payload.id)
                console.log(res.data)
           }
            catch (err) {
                console.log("error " + err)
            }
        },
        async  getBalance({ commit, state },payload) {
            try {
                const res = await axios.get('/wp-json/iptv/get-balance?id='+payload.id)
                state.balance =  res.data.balance
           }
            catch (err) {
                console.log("error " + err)
            }
        },
        /**
         * 
         * @param {*} param 
         * @param {*} payload 
         * upload chanel icon
         */
        async uploadLogo({ commit, state }, payload){
            try{
                var bodyFormData = new FormData();
                bodyFormData.set('id', payload.id);
                bodyFormData.append('logo', payload.logo); 
                const res = await axios.post('/wp-json/iptv/update-logo',bodyFormData,{headers :{'Content-Type': 'multipart/form-data' }})
               }
            catch(err){
                console.log("error " + err)
            }
        }
    },
})