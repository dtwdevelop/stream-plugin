export default  {
   name : 'Site',
   template: `
   <div>
<v-app>
<v-content>
 <v-container>
<router-view>
<v-progress-circular indeterminate color='primary'></v-progress-circular>
</router-view>
</v-container>
   </v-content>
 </v-app>
  </div>`,
 
 created(){
  
 }

}
