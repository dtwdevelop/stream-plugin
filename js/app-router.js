 //const Category = () => import('./components/category.js')
 import { Category } from './components/category.js'
 const Packages = () => import('./components/packages.js')
 const User = () => import('./components/users.js')
 const Setting = () => import('./components/settings.js')
 const Status = () => import('./components/status.js')
 const Admin = () => import('./admin.js')
 const Site = () => import('./site.js')
 //import { Admin } from "./admin.js"

 //Account, Payment , Package
import { Develop , UserRegister,  UserLogin , NotFound ,RestorePassword ,UserPasswordChange ,ActivationUser} from './app.js';
import {Account} from './components/site/account.js';
import {Payment} from './components/site/payment.js';
import {Package} from './components/site/package.js';
import {AuthRedirect} from './components/site/auth.js';
import {Transaction} from './components/site/transaction.js';

import {messages} from './translate.js';

 

const routesadmin = [
    { path: '/', component: User } ,
    { path: '/status', component: Status },
    { path: '/packages', component: Packages },
    { path: '/users', component: User },
    { path: '/categories', component: Category },
    { path: '/setting', component: Setting },
    { path: '/register', component: UserRegister },
  ]

  const routeradmin = new VueRouter({
  //mode: 'history',
    routes:routesadmin // short for `routes: routes`
  })

  const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
  })

 
  if(document.querySelector("#apps") !== null){

new Vue({ 
    store,
    i18n,
    router : routeradmin,
   vuetify: new Vuetify(),
   render: h => h(Admin)
}).$mount("#apps")

  }

  
const routes = [
  //{ path: '/', name : 'login', component : UserLogin},
    { path: '/billing', name : 'login', component : UserLogin},
    { path: '/billing/register', component: UserRegister},
    { path: '/billing/restore', component: RestorePassword},
    { path: '/billing/pass-change/:user_id', component: UserPasswordChange},
    { path: '/billing/activation/:user_id', component: ActivationUser },
    { path: '/billing/account', name :'account', component: Account , meta: { requiresAuth: true } },
   // { path: '/package', component: Package , meta: { requiresAuth: true } },
   { path: '/billing/transaction', component: Transaction , meta: { requiresAuth: true } },
    { path: '/billing/payment', component: Payment , meta: { requiresAuth: true } },
    { path: '/billing/payment/:user_id', component: Payment , meta: { requiresAuth: true } },
   // { path: '/payment/:sucefful', component: Payment },
    //{ path: '/payment/:error', component: Payment }
   
    { path: '/billing/auth', component: AuthRedirect },
    { path: '*' , component: NotFound }
  ]
  
  const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

  router.beforeEach((to, from,next)=>{
    if (to.matched.some(record => record.meta.requiresAuth)) {
     
     if (!store.state.user  ) {
        next({
          name: 'login',
         
       })
      } else {
        next()
      }
    } else {
      next() // make sure to always call next()!
    }

  })

  if(document.querySelector("#app") !== null){
   new Vue({
      store ,
      i18n,
      router,
      vuetify: new Vuetify(),
      render: h => h(Site)
      }).$mount("#app")
  }
  