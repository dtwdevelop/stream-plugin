// const validationMixin = window.vuelidate
// const { required, minLength ,sameAs ,email } = window.validators
Vue.use(window.vuelidate.default)
const { required, minLength, sameAs, email } = window.validators

const Menu = Vue.component('IpMenu', {
  template: `
  <div >
  <v-toolbar>
      <v-toolbar-title>{{$t('message.user_balance')}} &pound; {{balance}}  </v-toolbar-title>
       <v-spacer></v-spacer>
        <v-toolbar-items>
        <v-btn text to="/billing/account">{{$t('message.user_account')}}</v-btn>
       <!-- <v-btn text to="/billing/transaction">{{$t('message.user_transaction')}}</v-btn> -->
        <v-btn to="/billing/payment" text>{{$t('message.user_add_balance')}}</v-btn>
        <v-btn  text @click="Logout()">{{$t('message.user_logout')}}</v-btn>
      </v-toolbar-items>
      <template v-if="$vuetify.breakpoint.smAndUp">
      </template>
    </v-toolbar>
   </div>`,
  data: () => ({
    data: []
  }),
  computed: {
    balance: function () {
      return parseFloat(this.$store.state.balance)
    },
    user_id: function () {
      this.$store.state.user.ID
    }
  },
  methods: {
    Logout() {
      this.$store.dispatch('Logout', { status: true })
      this.$router.push("/billing/")
      // this.$router.go()
    }
  },
})

const UserRegister = Vue.component('UserRegister', {
  //mixins: [validationMixin.validationMixin],
  template: `
  <div>
  
    <v-card class="mb-12">
    <v-container>
 <v-row>
      <v-col cols="8" md="4">
      <v-card-title>{{$t('message.register')}}</v-card-title>
      <v-form >
      <v-card-text>
      <v-alert v-if="created" type="success"> Пользователь создан</v-alert>
      <v-alert type="error" v-if="isLoginValid">
    Логин такой уже существует
    </v-alert>
      <v-alert type="error" v-if="isValidEmail">Такой email  уже существует</v-alert>
      <v-text-field prepend-icon="mdi-account" v-model="user.login" required :error-messages="loginErrors" @input="$v.user.login.$touch()"@blur="$v.user.login.$touch()"  :label="$t('message.user_login')" required ></v-text-field>
      <v-text-field prepend-icon="mdi-at" v-model="user.email" @change="checkEmail()" type="email" :error-messages="emailErrors" @input="$v.user.email.$touch()"@blur="$v.user.email.$touch()"   :label="$t('message.user_email')" required ></v-text-field>
      <v-text-field  prepend-icon="mdi-lock-open" v-model="user.password" type="password"  :error-messages="passwordErrors" @input="$v.user.password.$touch()"@blur="$v.user.password.$touch()"  :label="$t('message.user_password')" required ></v-text-field>
     
      <v-text-field  prepend-icon="mdi-lock" v-model="user.password_r" type="password" :error-messages="passwordRErrors" @input="$v.user.password_r.$touch()"@blur="$v.user.password_r.$touch()"    :label="$t('message.user_password_repeat')" required ></v-text-field>
      <v-checkbox v-model="user.agree"   :label="$t('message.user_agree')" required ></v-checkbox>
      </v-card-text>
      <v-card-actions>
      <v-btn color="primary" @click="UserRegister()" class="mr-4">{{$t('message.register')}}</v-btn>
      <v-btn type="link"  color="link" to="/billing/" class="mr-4">{{$t('message.back')}}</v-btn>
      </v-card-actions>
      </v-form>
      </v-col>
      </v-row>
     </v-container>
      </v-card>
 </div>`,
  data: () => ({
    user: {
      login: "",
      password: "",
      password_repeat: "",
      email: "",
      agree: ""
    },
    created: false,

  }),

  validations: {
    user: {
      login: {
        required,
        minLength: minLength(4)
      },
      password: {
        required,
        minLength: minLength(4)
      },
      email: {
        required,
        email

      },
      password_r: {
        required,
        minLength: minLength(4),
        sameAs: sameAs(function () {
          return this.user.password
        })

      }
    }

  },
  computed: {
    loginErrors() {
      const errors = []
      if (!this.$v.user.login.$dirty) return errors
      !this.$v.user.login.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.user.login.required && errors.push('Логин обязательно.')
      return errors
    },
    emailErrors() {
      const errors = []
      if (!this.$v.user.email.$dirty) return errors
      !this.$v.user.email.email && errors.push('должно быть в формате sample@person.com')
      !this.$v.user.email.required && errors.push('Эмайл обязательно.')
      return errors
    },
    passwordErrors() {
      const errors = []
      if (!this.$v.user.password.$dirty) return errors
      !this.$v.user.password.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.user.password.required && errors.push('Пароль обязательный обязательно.')
      return errors
    },
    isValidEmail() {
      return this.$store.state.emailValid
    },
    isLoginValid() {
      return this.$store.state.userLogin.error
    },

    passwordRErrors() {
      const errors = []
      if (!this.$v.user.password_r.$dirty) return errors
      !this.$v.user.password_r.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.user.password_r.sameAs && errors.push('Пароль пароли разные.')
      !this.$v.user.password_r.required && errors.push('Повторить пароль.')
      return errors
    }

  },
  created() {

  },
  methods: {
    checkEmail() {
      this.$store.dispatch("checkEmail", { email: this.user.email })
    },
    UserRegister() {

      this.$v.$touch()
      if (!this.$v.$invalid && !this.isValidEmail) {
        const $this = this
        this.$store.dispatch("UserRegistration", { login: this.user.login, password: this.user.password, email: this.user.email }).then(
          () => {
            if (!$this.isLoginValid) {
              $this.created = true
              $this.user = {
                login: "",
                password: "",
                password_repeat: "",
                email: "",
                agree: ""
              }

            }
          })
      }
    }
  },
})


const UserLogin = Vue.component('UserLogin', {
  // mixins: [validationMixin.validationMixin],
  template: `
  <div>
  <v-card   >
    <v-container  >
   <v-row>
      <v-col  md="4">
      <v-card-title>
      {{$t('message.login_in')}}
      <v-alert  dense outlined type="error" v-if="status">
     {{ $t('message.invalid') }} 
    </v-alert>
    <v-alert  dense outlined type="error" v-if="no_active">
    Please activate account
   </v-alert>
    </v-card-title>
      <v-form >
      <v-card-text>
      <v-text-field  prepend-icon="mdi-account" v-model="login"  :label="$t('message.user_login')" :error-messages="loginErrors" required @input="$v.login.$touch()"@blur="$v.login.$touch()" ></v-text-field>
      <v-text-field  prepend-icon="mdi-lock" v-model="password" type="password" :error-messages="passwordErrors" @input="$v.password.$touch()"@blur="$v.password.$touch()"   :label="$t('message.user_password')" required ></v-text-field>
      <router-link to="/billing/restore">{{$t('message.reset')}}</router-link>
     </v-card-text>
     <v-card-actions>
     
      <v-btn color="primary" @click="UserLogin()" class="mr-4">{{$t('message.login')}}</v-btn>
      <v-btn  type="link" :disabled="true"  color="link" to="/billing/register" class="mr-4">{{$t('message.register')}}</v-btn>
     
      </v-card-actions>
      </v-form>
      </v-col>
      <v-col>
    
      </v-col>
      </v-row>
     
     </v-container>
      </v-card>
 </div>`,
  data: () => ({

    login: "",
    password: "",
    checkbox: "",
    no_active : false,

  }),

  computed: {
    loginErrors() {
      const errors = []
      if (!this.$v.login.$dirty) return errors
      !this.$v.login.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.login.required && errors.push(this.$i18n.t('message.login_required'))
      return errors
    },

    status() {
      return this.$store.state.status
    },
    passwordErrors() {
      const errors = []
      if (!this.$v.password.$dirty) return errors
      !this.$v.password.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.password.required && errors.push(this.$i18n.t('message.password_required'))
      return errors
    }


  },

  validations: {
    login: {
      required,
      minLength: minLength(4)
    },
    password: {
      required,
      minLength: minLength(4)
    }
  },
  created() {

  },

  methods: {
    async UserLogin() {
      if (!this.$v.$invalid) {
        const $this  = this
        this.$store.dispatch("UserLogin", { login: this.login, password: this.password }).then((res) => {
          
          if(res > 0){
            this.$router.push({ name: 'account' })
          }
          else{
            $this.no_active  = true
          }
         
         


        })


        //   this.$router.go()
      }

    }
  },

})

const Develop = Vue.component('Develop', {
  // mixins: [validationMixin],
  template: `
  <div>
  <v-card class="mb-12">
    <v-container>
 <v-row>
      <v-col cols="8" md="4">
      <v-card-title>
      <h3> {{text}} <v-icon  large > fas fa-circle-notch fa-spin </v-icon></h3>
      </v-card-title>
      <v-card-text>
    </v-card-text>
    <v-card-actions>
      </v-card-actions>
     </v-col>
      <v-col>
    </v-col>
      </v-row>
     </v-container>
      </v-card>
 </div>`,
  data: () => ({
    text: "В разработке ... ",
    password: "",
    checkbox: "",
  }),
})

const NotFound = Vue.component('NotFound', {
  // mixins: [validationMixin],
  template: `
   <div>
   <v-card class="mb-12">
     <v-container>
  <v-row>
       <v-col cols="8" md="4">
       <v-card-title>
       <h3> {{text}} <v-icon  large > fas fa-bug </v-icon></h3>
       </v-card-title>
       <v-card-text>
     </v-card-text>
     <v-card-actions>
       </v-card-actions>
      </v-col>
       <v-col>
     </v-col>
       </v-row>
      </v-container>
       </v-card>
  </div>`,
  data: () => ({
    text: "404 ",

  }),
})

const ActivationUser = Vue.component('ActivationUser', {

  template: `
   <div>
   <v-card   >
     <v-container  >
    <v-row>
       <v-col  md="4">
       <v-card-title>
      Activate user
       <v-alert  dense outlined type="success" v-if="status">
      {{ $t('message.user_activated') }} 
     </v-alert>
     </v-card-title>
      
     
       </v-col>
       </v-row>
      
      </v-container>
       </v-card>
  </div>`,
  data: () => ({

    text: "",
    status: "",
  }),

  computed: {

  },
  created() {
    if (this.$route.query) {
     
      const user_id = atob(this.$route.params.user_id)
      this.activationAccount(user_id)
    }
  },

  methods: {
    async activationAccount(id) {
    this.$store.dispatch('activateUser',{id:id})

    }
  },

})

const RestorePassword = Vue.component('RestorePassword', {
  // mixins: [validationMixin.validationMixin],
  template: `
   <div>
   <v-card   >
     <v-container  >
    <v-row>
       <v-col  md="4">
       <v-card-title>
       {{$t('message.reset')}}
       <v-alert  dense outlined type="success" v-if="status_email">
      {{ $t('message.user_email_send') }} 
     </v-alert>
     </v-card-title>
       <v-form >
       <v-card-text>
       <v-text-field  prepend-icon="mdi-email" v-model="login"  :label="$t('message.user_email')" :error-messages="loginErrors" required @input="$v.login.$touch()"@blur="$v.login.$touch()" ></v-text-field>
      </v-card-text>
      <v-card-actions>
      
       <v-btn color="primary" @click="resetPassword()" class="mr-4">{{$t('message.reset_btn')}}</v-btn>
       <v-btn  type="link"  color="link" to="/billing/" class="mr-4">{{$t('message.back')}}</v-btn>
       </v-card-actions>
       </v-form>
       </v-col>
       <v-col>
     
       </v-col>
       </v-row>
      
      </v-container>
       </v-card>
  </div>`,
  data: () => ({

    login: "",
    status_email: false
  }),

  computed: {
    loginErrors() {
      const errors = []
      if (!this.$v.login.$dirty) return errors
      !this.$v.login.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.login.required && errors.push(this.$i18n.t('message.login_required'))
      !this.$v.login.email && errors.push('должно быть в формате sample@person.com')
      return errors
    },

    status() {
      return this.$store.state.status
    },


  },

  validations: {
    login: {
      required,
      minLength: minLength(4),
      email
    },

  },
  created() {

  },

  methods: {
    async resetPassword() {
      if (!this.$v.$invalid) {
        this.$store.dispatch('sendRestoreEmail', { email: this.login })
        this.status_email = true
      }

    }
  },

})

const UserPasswordChange = Vue.component('UserPasswordChange', {
  //mixins: [validationMixin.validationMixin],
  template: `
  <div>
  
    <v-card class="mb-12">
    <v-container>
 <v-row>
      <v-col cols="8" md="4">
      <v-card-title>{{$t('message.user_password_change')}}</v-card-title>
      <v-form >
      <v-card-text>
      <v-alert v-if="created" type="success">{{$t('message.user_pass_changed')}}</v-alert>
      <v-text-field  prepend-icon="mdi-lock-open" v-model="user.password" type="password"  :error-messages="passwordErrors" @input="$v.user.password.$touch()"@blur="$v.user.password.$touch()"  :label="$t('message.user_password')" required ></v-text-field>
      <v-text-field  prepend-icon="mdi-lock" v-model="user.password_r" type="password" :error-messages="passwordRErrors" @input="$v.user.password_r.$touch()"@blur="$v.user.password_r.$touch()"    :label="$t('message.user_password_repeat')" required ></v-text-field>
      </v-card-text>
      <v-card-actions>
      <v-btn color="primary" @click=" changePassword()" class="mr-4">{{$t('message.change_password')}}</v-btn>
     </v-card-actions>
      </v-form>
      </v-col>
      </v-row>
     </v-container>
      </v-card>
 </div>`,
  data: () => ({
    user: {

      password: "",
      password_repeat: "",

    },
    created: false,
    user_id: null

  }),

  validations: {
    user: {

      password: {
        required,
        minLength: minLength(4)
      },

      password_r: {
        required,
        minLength: minLength(4),
        sameAs: sameAs(function () {
          return this.user.password
        })

      }
    }

  },
  computed: {


    passwordErrors() {
      const errors = []
      if (!this.$v.user.password.$dirty) return errors
      !this.$v.user.password.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.user.password.required && errors.push('Пароль обязательный обязательно.')
      return errors
    },


    passwordRErrors() {
      const errors = []
      if (!this.$v.user.password_r.$dirty) return errors
      !this.$v.user.password_r.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.user.password_r.sameAs && errors.push('Пароль пароли разные.')
      !this.$v.user.password_r.required && errors.push('Повторить пароль.')
      return errors
    }

  },
  mounted() {
    if (this.$route.query) {
      this.user_id = this.$route.params.user_id
       }

  },
  methods: {

    changePassword() {

      this.$v.$touch()
      if (!this.$v.$invalid) {

        this.$store.dispatch("updatePassword", { pass: this.password, id: atob(this.user_id) })
        this.created = true
      }
    },
  }
})

export { Develop, UserRegister, UserLogin, NotFound, RestorePassword, UserPasswordChange, ActivationUser }

