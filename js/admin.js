export default  {

     name : 'Admin',
     template: `
    <div>
    <v-app>
    <v-content>
    
    <v-toolbar>
<v-toolbar-title><v-icon>fas fa-tv</v-icon></v-toolbar-title>
<v-spacer></v-spacer>
<v-toolbar-items>
<v-btn to="/status"><v-icon>fas fa-chart-line</v-icon></v-btn>
  <v-btn to="/users"><v-icon>fas fa-users</v-icon></v-btn>
  <v-btn to="/packages"><v-icon>fas fa-stream</v-icon></v-btn>
  <v-btn to="/categories"><v-icon>fas fa-server</v-icon></v-btn>
  <v-btn to="/setting"><v-icon>fas fa-cogs</v-icon></v-btn>
</v-toolbar-items>
</v-toolbar>

<v-container>

<router-view>
<v-progress-circular indeterminate color='primary'></v-progress-circular>
</router-view>
      </v-container>
    </v-content>
  </v-app>
   </div>`,
   data: () => ({
    sticky: true,
    
  }),
  computed:{

     data: function() {
      let datatime = new Date().setMonth(5)
      return  new Date(datatime).toLocaleDateString()
     }
  },
  
  created(){
   this.check_version()
},
  methods:{
    async  check_version() {
      try {
          const res = await axios.get('/wp-json/iptv/version-plugin')
          console.log(res.data.version)
     }
      catch (err) {
          console.log("error " + err)
      }
  }
}

}
