import { Subaccount} from '../subusers.js'
const Account = Vue.component('Account', {
  components : {Subaccount},
  template: `<div>
    <ip-menu></ip-menu>

   <v-spacer></v-spacer>
   <v-dialog v-model="dialog3" max-width="290" >
   <v-card>
     <v-card-title class="headline">Покупка пакета</v-card-title>
    <v-card-text>
    Нехватает баланса пополните !
     </v-card-text>
    <v-card-actions>
       <v-spacer></v-spacer>

       <v-btn color="green darken-1"  text  @click="dialog3 = false" >
         Закрыть
       </v-btn>

     
     </v-card-actions>
   </v-card>
 </v-dialog>

    <v-container fluid >
    <v-row>
    <v-col>
    <v-card class="mx-auto" max-width="644" transition="fab-transition">
  
   <v-card-title> {{$t('message.user_information')}} </v-card-title>
    <v-card-subtitle class="pb-0">{{$t('message.user_name')}} : {{account.user.user_login}}</v-card-subtitle>
   <v-card-text class="text--primary">
  
   <div v-if="account.user.status != 'false'">
   <div>{{$t('message.user_tariff')}}: {{account.package.title}}</div>
   <div><v-icon>mdi-key-variant</v-icon>{{$t('message.user_your_key')}} : {{account.user.keyhash}}  <v-btn depressed small @click="keyChanges(account.user.user_id , account.user.user_login)">{{$t('message.user_change_key')}}</v-btn></div>
  <div>
  <v-icon>mdi-calendar-range</v-icon>{{$t('message.user_expire')}}: {{ account.user.expire }}   <v-icon small color="green" v-if="account.user.light">fas fa-check</v-icon> <v-icon small color="red" v-else>fas fa-times</v-icon>
  
  </div>
  <div> <v-switch v-model="account.user.adult"  true-value="true"  false-value="false" :label="$t('message.user_adult')"  @change="setAdult(account.user.user_id ,account.user.adult)"   ></v-switch></div>
  <Subaccount :admin="false" :user="account.user" :links="links"> </Subaccount>
  </div>
  <div v-else>{{$t('message.user_buy_before')}} </div>
  </v-card-text>
<v-card-actions>

  

  <v-dialog  v-model="dialog" width="700"  color="white">
  <v-card>
  <v-card-title class="headline  lighten-2" primary-title >
  {{$t('message.user_choice_package_title')}}
 
  </v-card-title>
  <Package  @updateme="loadAccount()"></Package>
  <v-card-actions>
  <v-spacer></v-spacer>
  <v-btn color="primary"  text @click="close()" >
  {{$t('message.close')}}
  </v-btn>
</v-card-actions>
  </v-card>
  
    <template  v-slot:activator="{ on }">
    <v-btn depressed small :disabled="false" color="warning"  v-on="on"> {{$t('message.user_choice_package')}}</v-btn> 
   
     </template> 
      </v-dialog>
</v-card-actions>

</v-card>
  </v-col>

<v-col>
<v-card class="mx-auto" max-width="444" outlined>
  <v-card-title  >
  {{$t('message.user_settings')}}
 </v-card-title>

 <v-card-subtitle class="pb-0">
 <p>{{$t('message.lang')}}</p>
  <v-select :items="languages" @change="chnageLanguage()" v-model="lang" item-text="title"  selected item-value="id"  :label="$t('message.lang')">
 </v-select>
 {{$t('message.user_password_change')}}
 </v-card-subtitle>
  <v-card-text>
  
  <v-form v-model="valid">
  <v-container>
  <v-alert v-if="show" dismissible type="success"> {{$t('message.user_password_changed')}}</v-alert>
    <v-row>
      <v-col cols="12" md="4" >
        <v-text-field prepend-icon="mdi-lock-open" v-model="password" type="password"  :hint="$t('message.password')"  :label="$t('message.password')" required  ></v-text-field>
      </v-col>
       <v-col cols="12" md="4" >
        <v-text-field prepend-icon="mdi-lock" v-model="password_r" type="password"   :hint="$t('message.user_password_repeat')"  :label="$t('message.user_password_repeat')" required ></v-text-field>
        </v-col>
  </v-row>
  </v-container>
  </v-form>
 </v-card-text>
 <v-card-actions>
  <v-btn small color="primary" :disabled="false" @click="changePassword(account.user.user_id)" class="mr-4">{{$t('message.change_password')}}</v-btn>
   </v-card-actions>
</v-card>

</v-col>
</v-row>
</v-container>
  </div>`,
  data: () => ({
    dialog: false,
    message: "Package",
    status: false,
    dialog2 :false,
    dialog3 :false,
    dialog4: false,
    disable : false,
    //
    package : {},
    valid: false,
    firstname: '',
    lastname: '',
    password: '',
    password_r: '',
    show: false,
    show2 : false,
    stalker :{
      password_stalker: '',
      password_stalker_r: '',
      login: ''
    },
    agree : false,
    lang : 'eng'
  }),

  async mounted() {
    //const id = this.$store.state.user.ID
    this.$store.dispatch("Account", { id: this.$store.state.user.ID })
   
    this.$store.commit("load_links")
    this.$store.dispatch("getMPackage")
  },
  computed: {
    account: function () {
      return this.$store.state.account
    },
    links: function () {

      return this.$store.state.linkUrls
    },
    packages : function(){
       return this.$store.state.mpackages
    },
    stalker_account(){
      return this.$store.state.stalker
    },

    balance : function (){
      return  this.$store.state.balance
    },
    languages:function (){
      return [{id:'eng', title:'English'},{id: 'ru' , title: 'Russian'} ]

    },
  },
  methods: {
    keyChanges(user_id,name) {
     // console.log(user_id)
      this.$store.dispatch("updateToken", { user_id: user_id, name:name})
      this.$store.dispatch("Account", { id: user_id })
    },

    chnageLanguage(){
     
      this.$i18n.locale = this.lang
   },

    close(){
      this.dialog = false
     
   },
   loadAccount(val){
    
    this.$store.dispatch("Account", { id: this.$store.state.user.ID })
   },

    setAdult(user, adult){
      this.$store.dispatch("sendAdult",{user_id: user, adult : adult})

    },

    /**
     * change password stalker
     * @param {*} item 
     */
    changeStalkerAccount(item){
    
      this.$store.dispatch("changeStalkerAccount",{ id:item.id, password : this.stalker.password_stalker})
      this.show2 = true
     
     },
    cretaeStalkerAccount(item){
      let random_password = Math.random().toString(36).substring(7);
      this.$store.dispatch("addStalkerAccount",{login:this.account.user.user_login, full_name:item.user_login, password : random_password, account_number:item.id, tariff_plan: item.category_id, status:1})
      this.show2 = true
      this.stalker.password_stalker = random_password
      this.stalker.login  = this.account.user.user_login
     // this.dialog4 =false
      this.disable = true
      this.$store.dispatch("Account", { id: this.$store.state.user.ID })

     // setTimeout(()=>{ this.$router.push('/account') },1000)
     },
    
    changePassword(user_id){
      this.$store.dispatch("updatePassword",{pass:this.password, id:user_id})
      this.show = true

    },
    
  },

})
export { Account }