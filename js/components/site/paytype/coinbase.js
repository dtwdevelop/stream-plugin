const CoinbaseComponent = Vue.component("CoinbaseComponent", {
    props: {
      suma : String,
      account :Object
     },
    template: `
      <div>
    
       <v-card class="mx-auto"max-width="700">
     <v-card-title>
     <v-alert dense type="error" v-if="show2">
      {{$t('message.user_payment_error')}}
    </v-alert>
    <v-alert dense type="error" v-if="alert_t">
     Please login to coinbase account
    </v-alert>
    <v-alert dense type="success" v-if="show3">
    {{$t('message.user_payment_succeful')}}
    </v-alert>
    </v-card-title>
     <v-card-text>
    
    </v-card-text>
    
     <v-card-actions>
     <v-btn small v-if="btn"  @click="AuthCoinbase()" >Auth coinbase</v-btn>

     <div v-if="status">
     
     <v-text-field v-model="account.user.user_login"  :label="$t('message.name')"  ></v-text-field>
     <v-btn small  v-if="btn2"  @click="Pay()" >Pay continue</v-btn>
     </div>
     </v-card-actions>
   </v-card>
   </div>
        
    `,
    data: () => ({
       
        show2: false,
        show3: false,
        error : false,
        message: '',
        name : '',
        component : null,
        client : null,
        dialog  : false,
        url : '',
        src_url: '',
        btn :  true,
        status : true,
        token_s : '',
        btn2 : false,
        data_token : null,
        token_p : null,
        alert_t : false,
        
  
    }),

    created(){

    },
    mounted(){
     
     
    },
   
    computed:{
     
      balance: function () {
        return this.$store.state.balance
      },
      token: function () {
        return this.$store.getters.auth
      },
     
      } ,
     
    methods: {

     async AuthCoinbase(){
         const $this  = this
          this.$store.dispatch('getStateCode',{user:this.account.user.user_id}).then((url)=>{
            this.src_url  = '/wp-json/iptv/coinbase-data?url='+this.url
            // console.log(this.src_url)
             let newWin = window.open(url, "Coinbase", "width=600,height=600");
           
            
            
              this.$store.dispatch("getAuth").then((res)=>{
               $this.data_token  = res
             
               $this.$store.dispatch("PaycoinbaseToken",{ code: this.data_token.token, statecode: this.data_token.statecode  }).then(data=>{
                if(data  == undefined){
                  $this.alert_t  = true
                }
                else{
                 $this.alert_t  = false
                 $this.btn  = false
                 $this.token_p  = data
                 $this.btn2  = true
                }
               
               })
             
              })

          })
         

        },
      
        async Pay(){
         
          if(this.data_token.token != undefined ){
          
            this.$store.dispatch('PaycoinbasePayment',{token:this.token_p ,suma:this.suma})
          
          }
          else{
            this.show2  = true
          }
         }
}
  })
  export {CoinbaseComponent  }