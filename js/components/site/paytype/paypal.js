
const PaypalComponent = Vue.component("PaypalComponent", {
    props: {
      account : Object,
      suma :String,
     },
    template: `
      <div>
<v-card class="mx-auto"max-width="500">

     <v-card-title>
     <v-alert type="error" v-if="show2">
      {{$t('message.user_payment_error')}}
    </v-alert>
    <v-alert type="success" v-if="show3">
    {{$t('message.user_payment_succeful')}}
    </v-alert>
    </v-card-title>

     <v-card-text>
     <v-text-field v-model="account.user.user_login"  :label="$t('message.name')"  ></v-text-field>
     
    

     </v-card-text>
     <v-card-actions>
     <div  id="paypal-button-container"></div>
     </v-card-actions>
   </v-card>
  
      
    </div>
        
    `,
    data: () => ({
       
        show2: false,
        stripe : null,
        card : null,
        show3: false,
        error : false,
        message: ''
  
    }),

    created(){

    },
    mounted(){
     
        const $this  = this
        paypal.Buttons({
        
            createOrder: function (data, actions) {
               // This function sets up the details of the transaction, including the amount and line item details.
               console.log($this.suma)
               return actions.order.create({
               purchase_units: [{
                   amount: {
                     value: $this.suma.toString(),
                    // value: '10'
                   }
                 }]
   
               });
             },
             onApprove: function (data, actions) {
               //This function captures the funds from the transaction.
               return actions.order.capture().then(function (details) {
                console.log('Transaction completed by ' + details.payer.name.given_name);
                 console.log('Transaction completed by ' + data.orderID);
                 const balance = parseFloat($this.balance) + parseFloat($this.suma)
                 $this.$store.commit('change_balance', { price: balance })
                 $this.$store.dispatch("updateBalance",{balance : parseFloat(balance) ,id:$this.account.user.id})
                 $this.$store.dispatch('sendBuyCreditComplete',{suma: parseFloat($this.suma), id:$this.account.user.id, email:$this.account.user.email, name:$this.account.user.user_login,tpay:'paypal' })

                 $this.show3 = true
                 $this.$emit('finished','true')
              
               //  setTimeout(()=>{ $this.e1 = 3 },500)
   
               });
             }
   
           }).render('#paypal-button-container');

    },
   
    computed:{
      balance: function () {
        return this.$store.state.balance
      },
     
      } ,
     
    methods: {
    }
  })
  export {PaypalComponent }