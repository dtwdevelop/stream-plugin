
const AuthRedirect = Vue.component('AuthRedirect', {
 
  template: `
    <div >
   <p>Auth redirect</p>
  </div>
   
  `,
  data: () => ({
   
   }),

  created(){
    
},

 async mounted() {
   if(this.$route.query){
    const  token = this.$route.query.code
    const  state  = this.$route.query.state
    this.changeToken(token,state)
   }
  
},

computed: {
    token: function () {
      return this.$store.state.auth
    },
   
  
  },
  methods: {
    changeToken(token,state){
      this.$store.commit('auth_change',{
        token : token ,
        state : state
      })
     window.close()
    

    }

  }
})
export { AuthRedirect }