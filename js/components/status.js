import { LineChart ,PieChart , PieChartArchive} from './chart-line.js'

export default {
    name : "Details",
    components: { LineChart , PieChart , PieChartArchive },
    template: `
  <v-container>
     
      <v-row>
      <v-col>
     <h4> {{$t('message.monitoring')}}</h4>
       <v-card class="mx-auto" max-width="344">
      <v-card-title> {{$t('message.server_up_time')}}</v-card-title>
      <v-card-text>
      <v-icon>far fa-clock</v-icon>
      {{status.uptime}}  в секундах
      </v-card-text>
      <v-card-actions>
      </v-card-actions>
    </v-card>
   </v-col>
   <v-col>
    <v-card class="mx-auto" max-width="344">
    <v-card-title> {{$t('message.traffic')}}</v-card-title>
    <v-card-text>
   
    <v-icon>fab fa-audible</v-icon>
    <v-divider></v-divider>
    входящего трафика: {{status.input_kbit}} кб
    <v-divider></v-divider>
    исходящего трафика: {{status.output_kbit}} кб
   </v-card-text>
    <v-card-actions>
    </v-card-actions>
  </v-card>
</v-col>

<v-col>
    <v-card class="mx-auto" max-width="344">
    <v-card-title> {{$t('message.total_clients')}}</v-card-title>
    <v-card-text>
    <v-icon>fas fa-users</v-icon>
    {{status.total_clients}}
    </v-card-text>
    <v-card-actions>
    </v-card-actions>
  </v-card>
 </v-col>
 </v-row>
 <v-row>
<v-col>

  <v-card class="mx-auto" max-width="344">
  <v-card-title> {{$t('message.active_thread')}} </v-card-title>
  <v-card-text>
  <v-icon> fas fa-braille</v-icon>
  {{status.online_streams}}
  </v-card-text>
  <v-card-actions>
  </v-card-actions>
</v-card>
</v-col>

<v-col>
 <!-- <v-card class="mx-auto" max-width="344">
  <v-card-title>Количество просмотря канала</v-card-title>
  <v-card-text>
 
  <v-autocomplete  item-value="chanel" @change="getInfo()" v-model="cat_select" :items="items"  item-text="title" label="Каналы" dense :search-input.sync="search"  return-object >
  </v-autocomplete>

  Сейчас смотрят {{  chanel_status}} человек.
  </v-card-text>
  <v-card-actions>
  </v-card-actions>
</v-card> -->
</v-col>
</v-row>

<v-row>
<v-col>
<v-card class="mx-auto" max-width="344">
<v-card-title>{{$t('message.top_chanel')}}</v-card-title>
<v-card-text>

<pie-chart :chart-data="data"></pie-chart>

</v-card-text>
<v-card-actions>
</v-card-actions>
</v-card>

</v-col>
<v-col>
<v-card class="mx-auto" max-width="344">
<v-card-title>{{$t('message.top_achive')}}</v-card-title>
<v-card-text>

<pie-chart-archive :chart-data="data"></pie-chart-archive>

</v-card-text>
<v-card-actions>
</v-card-actions>
</v-card>

</v-col>


<v-col>

</v-col>


</v-row>

  </v-container>
      
    `,
    data: () => ({
      cat_select: [],
      search : '',
      load : false,
      show :false,
      data : [],
        
    }),
    computed: {
        status(){
            return this.$store.state.statserver
        },
        items(){
          return this.$store.state.all_chanels
        },

        chanel_status(){
          return this.$store.state.chanel_detail
        },
        
    },
    
       async mounted() {
         this.$store.dispatch("statusServer",{})
         this.$store.dispatch("getAllChanels",{})
        },

      methods: {
        getInfo(){
        this.$store.dispatch("getChanelsDetail",{chanel:this.cat_select.chanel})
        }
   
     }
}
