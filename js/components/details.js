const Details = Vue.component("Details", {
  template: `
    <div>
    <v-card>
    
    <v-form >
    <v-container>
    <v-dialog
      v-model="dialog"
      width="500"
    >
     

      <v-card>
        <v-card-title class="headline grey lighten-2" primary-title >
       
        </v-card-title>

        <v-card-text>
        <v-form v-model="chanel_f">
        <v-text-field v-model="chanel.title"  :label="$t('message.chanel_name')" single-line   ></v-text-field>
        <v-text-field v-model="chanel.dataid"  :label="$t('message.epg_id')" single-line    ></v-text-field>
        </v-form>
        <v-btn small color="primary" @click="updateChanel(chanel.id)">{{$t('message.save')}}</v-btn>
        </v-card-text>

        <v-divider></v-divider>

        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn
            color="primary"
            text
            @click="dialog = false"
          >
           x
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
    <v-card-title>{{$t('message.change')}}</v-card-title>
    <v-list-item-content>
    <v-row>
   
     <v-col cols="12" sm="10">
     <v-form>
     <v-text-field  :label="$t('message.chanel_name')" single-line v-model="categories.category.title"></v-text-field>
     <v-text-field  :label="$t('message.description')" single-line v-model="categories.category.description"></v-text-field>
     </v-form>
     <v-btn small color="primary" @click="updateCategory(categories.category)">{{$t('message.save')}}</v-btn>

     <v-text-field v-model="search" :label="$t('message.search')" single-line hide-details ></v-text-field>
     <v-data-table :headers="headers"  :search="search"  :items="items"  loading-text="Loading... Please wait">
     <template v-slot:item.logoimg="{ item }">
    
     <v-avatar v-if="item.logo != 'logo'" size="40">
    
          <v-img :src="'https://duga.tv/wp-content/uploads/icons/'+item.chanel+'.png'" alt="logo"></v-img>
     </v-avatar>
     </template>

     <template v-slot:item.icon="{ item }">
     <div>
     
     <v-file-input dense small-chips v-model="logo" accept="image/png, image/jpeg, image/jpeg, image/bmp" label="upload logo" @change="uploadLogo(item.id)"></v-file-input>
      </div>
      </template>

      <template v-slot:item.action="{ item }">
   
       <v-icon @click="setData(item)">mdi-pencil </v-icon>
       <v-icon @click="deleteChanel(item)">mdi-delete </v-icon>
      </template>
      </v-data-table>
     </v-col>
     </v-row>
      </v-list-item-content>
      <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="primary" text  @click="clickClose()" >
           x
          </v-btn>
        
        </v-card-actions>
      </v-container>
      </v-form>
      </v-card>
      </div>
      
  `,
  data: () => ({
    logo: null,
    search: '',
    chanels: [],
    category: [],
    dialog: false,
    chanel: { id: "", title: "t", dataid: "t" },
    dialog: false,
    chanel_f: false,


   

  }),
  computed: {
    categories: function () {
      return this.$store.state.categories
    },
    items: function () {
      return this.$store.state.categories.chanels
    },
    headers: function(){
      return [
      { text:  this.$i18n.t('message.id'), value: 'id' },
      {
        text: this.$i18n.t('message.name'),
        align: 'left',
        sortable: false,
        value: 'title',
      },


      { text:  this.$i18n.t('message.package_id'), value: 'dataid' },
      { text:  this.$i18n.t('message.logo'), value: 'logoimg' },
      { text:  this.$i18n.t('message.logo_upload'), value: 'icon' },
      { text: this.$i18n.t('message.actions'), value: 'action' },

    ]}
  },
  methods: {
    setData(item) {
      this.chanel.title = item.title
      this.chanel.dataid = item.dataid
      this.chanel.id = item.id
      this.dialog = true
    },

    deleteChanel(item) {
      this.$store.dispatch("deleteChanel", { id: item.id })
      this.$store.dispatch("loadEditCategory", { id: this.$store.state.categories.category.id })
    },

    clickClose(event) {
      this.$emit('clicked', false)
    },
    updateCategory(category) {

      this.$store.dispatch("UpdateCategory", { category: { id: category.id, title: category.title, description: category.description } })
    },

    updateChanel(id) {
      this.$store.dispatch("UpdateChanel", { id: id, title: this.chanel.title, dataid: this.chanel.dataid })
      this.$store.dispatch("loadEditCategory", { id: this.$store.state.categories.category.id })
      this.dialog = false

    },
    uploadLogo(ch_id){
      console.log(this.logo);
      this.$store.dispatch("uploadLogo",{logo : this.logo, id:ch_id})
      this.logo  = null
     },

  },

  mounted() {

  },

})
export { Details }