import { Details } from './details.js'

const Category = Vue.component("Category", {
  components: { Details },
  template: `
    <div >
   <v-dialog v-model="dialog3" width="700">
    <Details @clicked="close()" />
    
    </v-dialog>
    <v-card color="primary" dark  v-if="loading" >
        <v-card-text>
          Загрузка ждите
          <v-progress-linear
            indeterminate
            color="white"
            class="mb-0"
          ></v-progress-linear>
        </v-card-text>
      </v-card>

      <v-dialog  v-model="dialog4"  width="500" persistent>
     

      <v-card>
        <v-card-title
          class="headline grey lighten-2"
          primary-title
        >
        {{$t('message.chanels_add')}}
        </v-card-title>

        <v-card-text>
        <v-autocomplete v-model="package.select3" :items="items" :loading="this.$store.state.loaded" :search-input.sync="search2" chips
      clearable
      hide-details
      hide-selected
      item-text="title"
      item-value="chanel"
     :label="$t('message.chanel_name')"
    multiple return-object
    >
    
      <template v-slot:selection="{ attr, on, item, selected }">
        <v-chip v-bind="attr" :input-value="selected" color="blue-grey" class="white--text" v-on="on" >
        <v-icon>fa-file </v-icon>
          {{ item.title }}
        
        </v-chip>
      </template>

      <template v-slot:item="{ item }">

       <v-list-item-content>
          <v-list-item-title v-text="item.title"></v-list-item-title>
          <v-text-field v-model="item.dataid"  :label="$t('message.epg_id')" required ></v-text-field>
        </v-list-item-content>
        <v-list-item-action>
          <v-icon>>fas fa-file</v-icon>
        </v-list-item-action>
      </template>

</v-autocomplete>

        </v-card-text>

        <v-divider></v-divider>

        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="green darken-1" text  @click="AddChanels(add_id)"> {{$t('message.add')}}</v-btn>
          <v-btn color="primary"  text  @click="dialog4 = false"  >
          {{$t('message.close')}}
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
    
    
    

    <v-dialog v-model="dialog" fullscreen  hide-overlay transition="dialog-bottom-transition">
    <template v-slot:activator="{ on }">
      <v-btn color="red lighten-2" dark v-on="on">
        {{$t('message.chanel_add')}}
      </v-btn>
    </template>
    <v-card>
      <v-card-title class="headline grey lighten-2" primary-title >
     {{ $t('message.categories')}}
      </v-card-title>
      <v-card-text>
      <v-form v-model="valid">
      <v-container>
        <v-row> <v-col cols="12"  md="4">
            <v-text-field
              v-model="package.title" :label="$t('message.name')"
              required
            ></v-text-field>
          </v-col>
          </v-row>
          <v-row>
          <v-col
            cols="12"
            md="6"
          > 
       <v-textarea v-model="package.description" :label="$t('message.description')" :value="package.description" ></v-textarea>
    


 <v-autocomplete v-model="package.select" :items="items" :loading="this.$store.state.loaded" :search-input.sync="search2" chips
      clearable
      hide-details
      hide-selected
      item-text="title"
      item-value="chanel"
     label="Название канала"
    multiple return-object
    >
    
      <template v-slot:selection="{ attr, on, item, selected }">
        <v-chip v-bind="attr" :input-value="selected" color="blue-grey" class="white--text" v-on="on" >
        <v-icon>fa-file </v-icon>
          {{ item.title }}
        
        </v-chip>
      </template>

      <template v-slot:item="{ item }">

       <v-list-item-content>
          <v-list-item-title v-text="item.title"></v-list-item-title>
          <v-text-field v-model="item.dataid"  :label="$t('epg_id')" required ></v-text-field>
        </v-list-item-content>
        <v-list-item-action>
          <v-icon>>fas fa-file</v-icon>
        </v-list-item-action>
      </template>

</v-autocomplete>

         </v-col>
        </v-row>
        <v-row>
        <v-col cols="12"  md="4">
        <v-btn color="primary" class="mr-4" @click="addChanel">{{$t('message.add')}}</v-btn>
      </v-col>
      </v-row>
      </v-container>
  
    </v-form>
      </v-card-text>
       <v-divider></v-divider>
       <v-card-actions>
        <v-spacer></v-spacer>
        <v-btn
          color="primary"
          text
          @click="dialog = false"
        >
        {{$t('message.close')}}
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
     <v-card>
    <v-card-title>
    {{$t('message.categories')}}
      <v-spacer></v-spacer>
      <v-text-field v-model="search" :label="$t('message.search')" single-line hide-details ></v-text-field>
    </v-card-title>
    <v-data-table v-if="loader" :headers="headers" :items="packages":search="search"  loading-text="Loading... Please wait">
    <template v-slot:item.chanels="{ item }">
    <v-icon @click="showAddChanel(item.id)">mdi-plus</v-icon>
  </template>
  <template v-slot:item.action="{ item }">
  <v-btn link small   @click="loadPackege(item.id)">
  <v-icon >mdi-pencil </v-icon>
  </v-btn>
  <v-icon color="red" @click="deletePackage(item.id)">mdi-delete </v-icon>
 
  </template>

    </v-data-table>
    <v-sheet v-else color="darken-2" class="px-3 pt-3 pb-3" >
    <v-skeleton-loader class="mx-auto"  max-width="700"  type="card, list-item-two-line" ></v-skeleton-loader>
  </v-sheet>
  </v-card>
  
  <v-dialog v-model="dialog2" persistent max-width="290">
      
      <v-card>
        <v-card-title class="headline">Удаление категорию</v-card-title>
        <v-card-text>Удалить категорию</v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="green darken-1" text @click="dialog2 = false">Disagree</v-btn>
          <v-btn color="green darken-1" text @click="dialog2 = false">Agree</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
     </div>
     `,
  data: () => ({
   
    add_id: null,
    dialog3: false,
    search: '',
    search2: '',
    dialog: false,
    dialog2: false,
    loading: false,
    dialog4: false,
    loader :false ,
    package: {
      title: '',
      price: '',
      description: '',
      select: [],
      urls: [],
      dataid: [],
      select2: [],
      select3: [],
    },
    valid: false,
    but: {
      show: false,
      id: 0,
    },

   

  }
  ),

  computed: {
    packages: function () {
      return this.$store.state.packages
    },
    headers: function(){
      return  [
        {
          text: 'id',
          align: 'left',
          sortable: true,
          value: 'id',
        },
        {
          text: this.$i18n.t('message.name'),
          align: 'left',
          sortable: false,
          value: 'name',
        },
        { text: this.$i18n.t('message.description'), value: 'description' },
        { text:  this.$i18n.t('message.chanels'), value: 'chanels' },
        { text:  this.$i18n.t('message.total_chanels'), value: 'total' },
        { text: this.$i18n.t('message.actions'), value: 'action' },
      ]

    },
    items: function () {
      return this.$store.state.chanels
    },
    status: function () {
      return this.$store.state.status
    },
    linkUrl: function () {
      return this.$store.state.linkUrl
    }

  },

  mounted() {
    const $this = this
    this.$store.dispatch("loadPackages").then(()=>$this.loader=true)
    this.$store.dispatch('ChanelsFromJson')
  },

  methods: {
    addChanel() {
      this.$store.dispatch("addPackage", { package: this.package })
      this.dialog = false
     

    },
    deletePackage(item) {
      // this.dialog2 = true,
      this.$store.dispatch('DeletePackage', { id: item })
    },
    close(val) {
      this.dialog3 = false
    },
    async loadPackege(id) {
      this.loading = true
      const $this = this
      this.$store.dispatch("loadEditCategory", { id: id }).then(() => {
        $this.loading = false
        this.dialog3 = true
      })

    },
    AddChanels(id) {
      this.$store.dispatch("addChanel", { id: id, select: this.package.select3 })
      this.package.select3 = []
     
    },
    showAddChanel(id) {
      this.dialog4 = true
      this.add_id = id

    },
   

  },
})
export { Category }
