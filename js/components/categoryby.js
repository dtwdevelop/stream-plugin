const CategoryBy = Vue.component("CategoryBy", {
  template: `
    <div>
    <v-card>
    <v-container>
     <v-card-title>{{$t('message.categories')}}</v-card-title>
     
     <v-row>
     <v-col cols="12" sm="10">
     <v-text-field v-model="search" :label="$t('message.search')" single-line hide-details ></v-text-field>
     <v-data-table :headers="headers"  :search="search"  :items="categories"  loading-text="Loading... Please wait">
     <template v-slot:item.action="{ item }">
     <v-icon color='red' @click="removeCategory(item.id)">mdi-delete </v-icon>
      </template>
      </v-data-table>
     </v-col>
     </v-row>
     </v-container>
     <v-card-actions>
     <v-spacer></v-spacer>
     <v-btn color="primary" text  @click="clickClose()" >
      x
     </v-btn>
   
   </v-card-actions>
     </v-card>
      </div>
       `,
       props: ['id'],
  data: () => ({
  
    search: '',
   

  }),
  computed: {
    categories: function () {
      return this.$store.state.categoriesby
    },
    pac_id: function () {
      return this.$store.state.categoriesby_id
    },

    headers: function(){ 
      return [
      {
        text: 'ID',
        align: 'left',
        sortable: true,
        value: 'id',
      },
      {
        text:  this.$i18n.t('message.name'),
        align: 'left',
        sortable: false,
        value: 'title',
      },
    
     { 
       text: this.$i18n.t('message.description'),
      value: 'action' 
    },

    ]
  }
    
  },
  mounted() {
  

  },

  methods: {
    removeCategory(cat_id ) {
      
      this.$store.dispatch('removeCategoryBy', { id: cat_id, pac : this.pac_id })
      
      this.$emit('clicked', false)
    },

    clickClose(event) {
      this.$emit('clicked', false)
    },

},

 

})
export {CategoryBy}