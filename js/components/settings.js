var validationMixin = window.vuelidate.validationMixin
const { required, minLength, sameAs, email } = window.validators

export default {
  name: 'Settings',
  mixins: [validationMixin],
  template: `
    <div>
  

    <v-card class="mb-12" v-if="loader">
      <v-container>
   <v-row>
        <v-col cols="2" md="6">
        <v-card-title>{{$t('message.api_settings')}} </v-card-title>
        <v-form >
        <v-card-text>
        <v-expansion-panels v-model="panel" multiple >
         <v-expansion-panel>
         <v-expansion-panel-header>Flussonic and links </v-expansion-panel-header>
         <v-expansion-panel-content>
         <v-text-field v-model="options.login"  :label="$t('message.login_flussonic')" :error-messages="loginErrors" required @input="$v.options.login.$touch()"@blur="$v.options.login.$touch()" ></v-text-field>
         <v-text-field v-model="options.password" type="password" :error-messages="passwordErrors" @input="$v.options.password.$touch()"@blur="$v.options.password.$touch()"   :label="$t('message.password_flussonic')" required ></v-text-field>
         <v-text-field v-model="options.url"   :label="$t('message.link_api')" :error-messages="urlErrors" required @input="$v.options.url.$touch()"@blur="$v.options.url.$touch()" ></v-text-field>
         <v-text-field v-model="options.domain"   :label="$t('message.domain_api')" :error-messages="domainErrors" required @input="$v.options.domain.$touch()"@blur="$v.options.domain.$touch()"  ></v-text-field>
         <v-text-field v-model="options.sub_url"   :label="$t('message.sub_domain')" :error-messages="sub_urlErrors" required @input="$v.options.sub_url.$touch()"@blur="$v.options.sub_url.$touch()"  ></v-text-field>
         <v-text-field v-model="options.epg"   :label="$t('message.epg_link')"  ></v-text-field>
         <v-text-field v-model="options.stat_link"   :label="$t('message.statistic_link')"  ></v-text-field>
         </v-expansion-panel-content>
       </v-expansion-panel>

         <v-expansion-panel>
         <v-expansion-panel-header>Stalker</v-expansion-panel-header>
         <v-expansion-panel-content>
         <v-text-field v-model="options.login_stalker"  :label="$t('message.login_stalker')"  ></v-text-field>
         <v-text-field v-model="options.password_stalker"  :label="$t('message.password_stalker')" type="password"  required ></v-text-field>
         <v-text-field v-model="options.url_stalker"   :label="$t('message.link_api_stalker')" :error-messages="url_stalkerErrors" required @input="$v.options.url_stalker.$touch()"@blur="$v.options.url_stalker.$touch()"  ></v-text-field>
         
         </v-expansion-panel-content>
       </v-expansion-panel>

       <v-expansion-panel>
       <v-expansion-panel-header>Iptv Portal Ru</v-expansion-panel-header>
       <v-expansion-panel-content>
       <v-text-field v-model="options.portal_login"  :label="$t('message.portal_login')"  ></v-text-field>
       <v-text-field v-model="options.portal_password"  :label="$t('message.portal_password')" type="password"   ></v-text-field>
       <v-text-field v-model="options.portal_link_api"   :label="$t('message.portal_link_api')"   ></v-text-field>
       <v-checkbox v-model="options.btn_portal_status" label="Playlist and stalker off "></v-checkbox>
       
       </v-expansion-panel-content>
     </v-expansion-panel>

      <v-expansion-panel>
        <v-expansion-panel-header>Paypal</v-expansion-panel-header>
        <v-expansion-panel-content>
        <v-text-field :type="show1 ? 'text' : 'password'"  @click:append="show1 = !show1"  :append-icon="show1 ? 'mdi-eye' : 'mdi-eye-off'" v-model="options.paypal_token"  :label="$t('message.token_paypal')"  ></v-text-field>
        <v-checkbox v-model="options.paypal_status" label="Paypal on "></v-checkbox>
        </v-expansion-panel-content>
      </v-expansion-panel>

      <v-expansion-panel>
        <v-expansion-panel-header>Stripe</v-expansion-panel-header>
        <v-expansion-panel-content>
        <v-text-field v-model="options.stripe_token"  :label="$t('message.public_key_stripe')"  ></v-text-field>
        <v-text-field type="password" v-model="options.stripe_token2"  :label="$t('message.private_key_stripe')"  ></v-text-field>
        <v-checkbox v-model="options.stripe_status" label="Stripe on "></v-checkbox>
        </v-expansion-panel-content>
      </v-expansion-panel>

      <v-expansion-panel>
        <v-expansion-panel-header>2checkout</v-expansion-panel-header>
        <v-expansion-panel-content>
        <v-text-field  v-model="options.checkout2_key"  label="2checkout  key"  ></v-text-field>
        <v-text-field  v-model="options.checkout2_merchant"  label="merchant code"  ></v-text-field>
        <v-checkbox v-model="options.checkout2_status" label="2checkout on "></v-checkbox>
        </v-expansion-panel-content>
      </v-expansion-panel>

      <v-expansion-panel>
      <v-expansion-panel-header>Coinbase</v-expansion-panel-header>
     <v-expansion-panel-content>
      <v-text-field  v-model="options.coinbase_key"  label="coinbase  key"  ></v-text-field>
      <v-text-field  v-model="options.coinbase_secret"  label="coinbase private key"  ></v-text-field>
      <v-text-field  v-model="options.coinbase_client_k"  label="coinbase  key" hint="coinbase client key"></v-text-field>
      <v-text-field  v-model="options.coinbase_private_k"  label="coinbase private key"  hint="coinbase private key"></v-text-field>
      <v-text-field  v-model="options.coinbase_redirect"  label="redirect url"  hint="redirect url"></v-text-field>

      <v-checkbox v-model="options.coinbase_status" label="coinbase on "></v-checkbox>
      </v-expansion-panel-content>
    </v-expansion-panel>

  <!--  <v-expansion-panel>
    <v-expansion-panel-header>Blockchain</v-expansion-panel-header>
    <v-expansion-panel-content>
  
    </v-expansion-panel-content>
  </v-expansion-panel>
  -->

 

<v-expansion-panel>
<v-expansion-panel-header>PaySera</v-expansion-panel-header>
<v-expansion-panel-content>

</v-expansion-panel-content>
</v-expansion-panel>

    </v-expansion-panels>

  <v-divider></v-divider>
       <p>{{$t('message.lang')}}</p>
         <v-select :items="languages" @change="chnageLanguage()" v-model="options.lang" item-text="title"  selected item-value="id"  :label="$t('message.lang')">
    
    </v-select>
    <v-text-field  v-model="options.sub_limit"  label="Sub Accounts limit"  ></v-text-field>
         </v-card-text>
       <v-card-actions>
       <v-btn color="primary" @click="SaveSettings()" class="mr-4">Сохранить</v-btn>
     </v-card-actions>
        </v-form>
        </v-col>
        <v-col>
      </v-col>
        </v-row>
       </v-container>
        </v-card>
        <v-sheet v-else color="darken-2" class="px-3 pt-3 pb-3" >
        <v-skeleton-loader class="mx-auto"  max-width="700"  type="card, list-item-two-line" ></v-skeleton-loader>
      </v-sheet>
   </div>`,
  data: () => ({
    status : "",
    show1 : true ,
    loader : false,
    panel: [],

  }),

  validations: {
    options: {
      login: {
        required,
        minLength: minLength(4)
      },
      password: {
        required,
        minLength: minLength(4)
      },
      url: {
        required,
        minLength: minLength(10)
      },
      domain: {
        required,
        minLength: minLength(4)
      },
      sub_url: {
        required,
        minLength: minLength(4)
      },
      url_stalker: {
        required,
        minLength: minLength(4)
      },

    },

  },

  computed: {

    options: function () {
      return this.$store.state.options
    },

    languages:function (){
      return [{id:'eng', title:'English'},{id: 'ru' , title: 'Russian'} ]

    },

    loginErrors() {
      const errors = []
      if (!this.$v.options.login.$dirty) return errors
      !this.$v.options.login.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.options.login.required && errors.push('Логин обязательно.')
      return errors
    },
    urlErrors() {
      const errors = []
      if (!this.$v.options.url.$dirty) return errors
      !this.$v.options.url.minLength && errors.push('Длина логина должна быть больше 10 символов')
      !this.$v.options.url.required && errors.push('Линк обязательно.')
      return errors
    },
    passwordErrors() {
      const errors = []
      if (!this.$v.options.password.$dirty) return errors
      !this.$v.options.password.minLength && errors.push('Длина логина должна быть больше 4 символов')
      !this.$v.options.password.required && errors.push('Пароль обязательный обязательно.')
      return errors
    },

    domainErrors() {
      const errors = []
      if (!this.$v.options.domain.$dirty) return errors
      !this.$v.options.domain.minLength && errors.push('Длина домена должна быть больше 4 символов')
      !this.$v.options.domain.required && errors.push('Домен обязательно.')
      return errors
    },
    sub_urlErrors() {
      const errors = []
      if (!this.$v.options.sub_url.$dirty) return errors
      !this.$v.options.sub_url.minLength && errors.push('Длина домена должна быть больше 4 символов')
      !this.$v.options.sub_url.required && errors.push('Домен обязательно.')
      return errors
    },
    url_stalkerErrors() {
      const errors = []
      if (!this.$v.options.url_stalker.$dirty) return errors
      !this.$v.options.url_stalker.minLength && errors.push('Длина домена должна быть больше 4 символов')
      !this.$v.options.url_stalker.required && errors.push('Сталкер обязательно.')
      return errors
    },

   


  },
  mounted() {
    const $this = this
    this.$store.dispatch("GetSetting").then(function(){
      $this.loader = true
    });

  },



  methods: {
    SaveSettings() {
      if (!this.$v.$invalid) {
        this.$store.dispatch("SaveSetting", { data: this.options })
      }

    },
    chnageLanguage(){
     
       this.$i18n.locale = this.options.lang
    }
  },

}


