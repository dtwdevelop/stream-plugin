const Subaccount = Vue.component("Subaccount", {
  props: {
    user : Object,
    links : Array,
    admin : Boolean,
    
  },
  template: `
    <div>
    <v-card>
     <v-container>
 <v-dialog  v-model="dialog" width="300">
      <v-card>
        <v-card-title class="headline grey lighten-2" primary-title >
        {{$t('message.sub_account_add')}}
        </v-card-title>

        <v-card-text>
      <v-form>
      <v-btn v-if="portal" small @click="addSubAccounts(user,'play')" color="red" >{{$t('message.user_token')}}</v-btn>
      <v-btn  v-if="portal" small @click="addSubAccounts(user,'stalker')"  color="green">{{$t('message.sub_account_stalker')}}</v-btn>
      <v-btn small @click="addSubAccounts(user,'portal')">Portal</v-btn>
      </v-form>
        </v-card-text>

        <v-divider></v-divider>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn   color="primary" text  @click="dialog = false" >
           x
          </v-btn>
        </v-card-actions>
      </v-card>
      <template v-slot:activator="{ on, attrs }">
        <v-btn :disabled="total_limit" small color="primary" dark v-bind="attrs" v-on="on" >
        {{$t('message.add')}}
        </v-btn>
      </template>
    </v-dialog>

    <v-dialog v-model="dialog2"  width="500" :hide-overlay="true"  :persistent="true"  >
      
    <v-card>
      <v-card-title
        class="headline grey lighten-2"
        primary-title
      >
      {{$t('message.playlists')}}
      </v-card-title>

      <v-card-text>
    
      <v-form >
      <div v-for="link in links" >
      <v-text-field :value="link.url+'/'+user_sub.hashkey+'/'+link.type +'/playlist'+ link.ext" :label="link.type" ></v-text-field>
      <v-btn link class="ma-2"  :href="link.url+'/'+user_sub.hashkey+'/'+link.type +'/playlist'+ link.ext" download ><v-icon >fas fa-download</v-icon>{{link.type}}</v-btn>

      </div>
    </v-form>
      </v-card-text>
    <v-divider></v-divider>
 <v-card-actions>
        <v-spacer></v-spacer>
        <v-btn color="primary"  text @click="close()" >
        {{$t('message.close')}}
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
    
    <v-card-title>{{$t('message.sub_accounts')}} </v-card-title>
  
    <v-data-table dense :headers="headers"  :hide-default-footer="true"  :items="sub_accounts"  loading-text="Loading... Please wait">
   

    <template v-slot:item.name="{item}">
    <p v-if="item.name != ''">
  {{item.name}}
  </p>

   </template>
   <template v-slot:item.playlist="{ item }">
    <v-btn v-if="item.type == 'play'" small color="green" @click="open(item.token)"><v-icon small>mdi-vector-link</v-icon></v-btn>
   </template>
    <template v-slot:item.action="{ item }">
   <v-btn small color="red" @click.native="deleteSubAccount(item)" > <v-icon color="white" >mdi-delete </v-icon></v-btn>
    
  
  </template>
    </v-data-table>
    <v-row v-if="admin">
    <v-col cols="8" md="4" >
    <v-alert type="success" dense  v-if="show">Balance change to {{balance}}</v-alert>
    Your balance now {{balance}} 
   <v-text-field  v-model="bal" :value="balance" :light="true"   hint="suma"  label="Balance" @change="changeBalance(bal)" ></v-text-field>  
   <v-btn small  @click="addBalance()" >Save</v-btn>
   </v-col>
   </v-row>
   </v-container>
      
    
      </v-card>
    
      </div>
      
  `,
  data: () => ({
   
    
    dialog: false,
    dialog2 : false,
    show : false,
    max : 3,
    bal:  0,

    user_sub:{
      hashkey : ''
    }

  }),
 
  computed:{
    total_limit: function(){
       return this.sub_accounts.length < this.sub_limit ? false : true
    },
    portal : function(){
      return   this.$store.state.options.btn_portal_status
    },

    sub_limit: function () {
      return this.$store.state.options.sub_limit
    },

    sub_accounts : function(){
     return this.$store.state.sub_accounts
    },
    headers: function(){ 
      return [
      { text:  this.$i18n.t('message.id'), value: 'id' },
      { text:  this.$i18n.t('message.user_token'), value: 'token' },
      { text: this.$i18n.t('message.name'), value: 'name' },
      { text: this.$i18n.t('message.user_sub_type'), value: 'type' },
      { text: this.$i18n.t('message.playlist'), value: 'playlist' },
      { text: this.$i18n.t('message.actions'), value: 'action' },

    ]},

    balance : function (){
      return  this.$store.state.balance
    },

    } ,
   
  methods: {
    clickClose(event) {
    //  this.$emit('clicked', false)
    },

    deleteSubAccount(item){
      if(item.type == "play"){
        this.$store.dispatch("deleteSubAccount",{ token : item.token , type : item.type})
      } 
      else{
        this.$store.dispatch("deleteSubAccount",{ token:item.token , type : item.type , id : item.id})
      }
     
      
    },

    close(){
      this.dialog2 = false
  },
  open(hashkey){
   this.dialog2  = true
   this.user_sub.hashkey  = hashkey
  },

   
   async addSubAccounts(user,type){
      if(type == "play"){
        this.$store.dispatch("addSubAccount",{ token : user.keyhash, type:type , name:user.name})
      
      } 
      else{
        //let random_password = Math.random().toString(36).substring(7);
        let random_password = Date.now();
        this.$store.dispatch("addSubAccount",{ token : user.keyhash, type:type ,name:user.name, id:user.id, pass:random_password ,stalkerdata:{tariff_plan : user.packet}})
       
      }
    
      this.dialog = false
    },

    async addBalance(){
      const sum  = parseFloat(this.balance) 
      this.$store.dispatch("updateBalance",{balance : sum , id : this.user.id})
      this.show = true

    },

    async changeBalance(bal){
      console.log(bal)
      this.$store.commit("change_balance",{price : bal})
    },

   

    loadAllSubaccounts(token){
      this.$store.dispatch("LoadSubAccounts",{token:token})
    },

  },

  mounted() {
   
    this.loadAllSubaccounts(this.user.keyhash)
    this.$store.dispatch("getBalance",{id:this.user.id})
    this.$store.dispatch("GetSetting").then(function(){
     
    });
  },

})
export { Subaccount }