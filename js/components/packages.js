import { CategoryBy } from './categoryby.js'

export default {
  name: "Packages",
  components: { CategoryBy },
  template: `
   <div >
   <v-dialog v-model="dialog4" width="700">
   <div v-if="loaded2">
   <CategoryBy id="id" @clicked="closePop()"  />
   </div>
   <v-sheet v-else color="darken-2" class="px-3 pt-3 pb-3" >
   <v-skeleton-loader class="mx-auto"  max-width="700"  type="card, list-item-two-line" ></v-skeleton-loader>
 </v-sheet>
   </v-dialog>

   <v-dialog v-model="dialog" width="700"  hide-overlay transition="dialog-bottom-transition">
   <template v-slot:activator="{ on }">
     <v-btn color="red lighten-2" dark  v-on="on" >
      
      {{ $t('message.add_package')}}
     </v-btn>
   </template>

   <v-card>
     <v-card-title  class="headline grey lighten-2" primary-title>
     {{ $t('message.packet')}}
     </v-card-title>

     <v-card-text>
     <v-form v-model="valid">
     <v-container>
       <v-row>
         <v-col cols="12" md="4">
           <v-text-field v-model="package.title" :label="$t('message.name')" required ></v-text-field>
         </v-col>
         </v-row>
        <v-row>
         <v-col cols="12" md="6" > 
     
       <v-textarea v-model="package.description" :label="$t('message.description_package')" :value="package.description" ></v-textarea>
      <v-combobox  item-value="value" v-model="package.select" :items="items"  item-text="name" :label="$t('message.categories')" multiple chips  readonly>
      <template v-slot:item="{ item}">
   <v-list-item-action>
   <v-icon>fas fa-list</v-icon>
 </v-list-item-action>
   <v-list-item-content>
   <v-list-item-title> {{item.desc}}</v-list-item-title>
   <v-list-item-subtitle> {{item.name}}</v-list-item-subtitle>
 </v-list-item-content>
  
  
 </template>
      </v-combobox>

     
   <v-combobox  item-value="id" v-model="package.tariff" :items="tariff"  item-text="name" label="Tariff"  chips  readonly>
  </v-combobox>

<v-text-field v-model="package.price" :label="$t('message.price')" required  ></v-text-field>
<v-switch v-model="package.archive"  true-value="1"  false-value="0"  :label="$t('message.archive')"  ></v-switch>
      </v-col>
       </v-row>
       <v-row>
     <v-btn color="primary" class="mr-4" @click="addChanel">{{$t('message.add')}}</v-btn>
     </v-col>
     
</v-row>
     </v-container>
 
   </v-form>
     </v-card-text>

     <v-divider></v-divider>

     <v-card-actions>
       <v-spacer></v-spacer>
       <v-btn
         color="primary"
         text
         @click="dialog = false"
       >
       {{$t('message.close')}}
       </v-btn>
     </v-card-actions>
   </v-card>
 </v-dialog>

 <v-dialog  v-model="dialog3"  width="500" persistent>
<v-card>
   <v-card-title class="headline grey lighten-2" primary-title >
   {{$t('message.category_add')}}
   </v-card-title>

   <v-combobox  item-value="value" v-model="cat_select" :items="items"  item-text="name" :label="$t('message.categories')" multiple chips  readonly>
   <template v-slot:item="{ item}">
   <v-list-item-action>
   <v-icon>fas fa-list</v-icon>
 </v-list-item-action>
   <v-list-item-content>
   <v-list-item-title> {{item.desc}}</v-list-item-title>
   <v-list-item-subtitle> {{item.name}}</v-list-item-subtitle>
 </v-list-item-content>
  
  
 </template>
   </v-combobox>
   

   <v-card-text>
</v-card-text>

   <v-divider></v-divider>
<v-card-actions>
     <v-spacer></v-spacer>
     <v-btn color="green darken-1" text  @click="addCategoryById(cat_id)">{{$t('message.add')}}</v-btn>
     <v-btn color="primary"  text  @click="dialog3 = false"  >
      {{$t('message.close')}}
     </v-btn>
   </v-card-actions>
 </v-card>
</v-dialog>
   
   <v-card>
   <v-card-title>
  
   {{$t('message.packages')}}
     <v-spacer></v-spacer>
   <v-text-field v-model="search" :label="$t('message.search')"single-line hide-details ></v-text-field>
   </v-card-title>
   <v-data-table 
     :headers="headers"
     :items="packages"
     :search="search"  loading-text="Loading... Please wait" dense
   >
   <template v-slot:item.chanels="{ item }">
   <v-icon>mdi-plus</v-icon>
 </template>

 <template v-slot:item.title="props">
   <v-edit-dialog :return-value.sync="props.item.title" @save="save"  @cancel="cancel" @close="close"> 
   {{ props.item.title }}
  <template v-slot:input>
  <v-text-field v-model="props.item.title" @change="onChangeTitle(props.item)" label="Edit" single-line counter ></v-text-field>
  </template>
</v-edit-dialog>
</template>

<template v-slot:item.price="props">
   <v-edit-dialog :return-value.sync="props.item.price" @save="save"  @cancel="cancel" @close="close"> 
   £ {{ props.item.price }}
  <template v-slot:input>
  <v-text-field v-model="props.item.price" @change="onChangePrice(props.item)" label="Edit" single-line counter ></v-text-field>
  </template>
</v-edit-dialog>


      </template>
 
 <template v-slot:item.archive="{ item }">
   <v-switch v-model="item.archive"  true-value="1"  false-value="0"  @change="updateArchive(item)" > </v-switch>
 
 </template>

 <template v-slot:item.action="{ item }">
 <v-btn small @click="showAddCategory(item.id)"><v-icon >mdi-plus</v-icon> </v-btn>
 <v-btn small @click="loadCategories(item.id)"><v-icon >mdi-pencil</v-icon> </v-btn>
 <v-icon color="red" @click="deletePackage(item.id)">mdi-delete </v-icon>
 
</template>

   </v-data-table>
 </v-card>
 <v-dialog v-model="dialog2" persistent max-width="290">
     
     <v-card>
       <v-card-title class="headline">Удаление пакета</v-card-title>
       <v-card-text>Удалить пакет</v-card-text>
       <v-card-actions>
         <v-spacer></v-spacer>
         <v-btn color="green darken-1" text @click="dialog2 = false">Disagree</v-btn>
         <v-btn color="green darken-1" text @click="dialog2 = false">Agree</v-btn>
       </v-card-actions>
     </v-card>
   </v-dialog>
   <v-snackbar v-model="snack" :timeout="3000" >
   Название изменино
   <v-btn text @click="snack = false">Close</v-btn>
 </v-snackbar>
    </div>
    `,
  data: () => ({
    search: '',
    dialog: false,
    dialog2: false,
    snack : false,
    dialog3 : false,
    dialog4: false,
    cat_id  : null,
    cat_select :[],
    sel_tariff :[],
    package: {
      title: '',
      price: '',
      description: '',
      select: [],
      urls: [],
      archive : '',
      tariff :{},
      },
    valid: false,
    but: {
      show: false,
      id: 0,
    },
    id_by: null,
    loaded2: false,

   

  }
  ),

  computed: {
    packages: function () {
      return this.$store.state.mpackages
    },
    headers : function(){
      return   [
        { text: 'id', value: 'id' },
        {
          text: this.$i18n.t('message.name'),
          align: 'left',
          sortable: false,
          value: 'title',
        },
        { text: this.$i18n.t('message.description_package'), value: 'description' },
       
        { text: this.$i18n.t('message.price'), value: 'price' },
        { text: this.$i18n.t('message.archive'), value: 'archive' ,  sortable: true},
        { text: this.$i18n.t('message.actions'), value: 'action' },
   ]
    },
    items: function () {
      const data = []
      this.$store.state.packages.forEach((pack) => {

        data.push({ name: pack.name, value: pack.id, dataid: "" , desc:pack.description })
      });
      return data
    },
    linkUrl: function () {
      return this.$store.state.linkUrl
    },

    tariff : function(){
     
       return this.$store.state.tariffs
    }
  },

  mounted() {
    this.$store.dispatch("getMPackage")
    this.$store.dispatch("loadPackages")
    this.$store.dispatch('getTariff')
    //this.$store.dispatch('ChanelsFromJson')
  },

  methods: {
    addChanel() {
      this.$store.dispatch("addMPackage", { package: this.package })
      this.dialog = false


    },
    deletePackage(item) {
      // this.dialog2 = true,
      this.$store.dispatch('DeleteMPackage', { id: item })
      this.$router.go()

    },

    loadCategories(id) {
      const $this = this
      this.$store.dispatch("loadCategoryBy",{ id: id}).then(()=> $this.loaded2=true)
      this.$store.commit("change_category_by",{cat:id})
      this.dialog4 = true
     },

     closePop(){
      this.dialog4 = false
      this.loaded2 = false
      
     },

    LoadPlaylist(id, type) {
      this.but = {
        show: true,
        id: id
      }
      this.$store.dispatch('PlaylistUrl', { id: id, type: type })
    },
    save () {
     this.snack =true
    },
    cancel () {
     this.snack = false
    },
   
    close () {
     this.snack = true
    },
    
    onChangeTitle(item){
        
         this.$store.dispatch("updatePackageName" ,{id:item.id,title:item.title})
    },
    onChangePrice(item){
        
      this.$store.dispatch("updatePackagePrice" ,{id:item.id,price:item.price})
 },
    showAddCategory(id){
        this.cat_id  = id
        this.dialog3  = true
     
    }, 
    addCategoryById(){
    this.$store.dispatch("addCategoryById",{package_id:this.cat_id,select:this.cat_select})
    this.cat_select = []
    this.dialog3 = false
    },

    updateArchive(pack){
     this.$store.dispatch("changeArchive",{id:pack.id,archive:pack.archive})
    },

    setStatus(st) {
      this.but = {
        show: st,
        id: 0
      }

    }
  },
}
