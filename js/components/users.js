
import { Subaccount} from './subusers.js'
export default {
  name: "User",
  components : {Subaccount},
  template: `
    <div >
    {{this.$store.state.message}}
    <v-card>
   <v-card-title>
    <v-btn color="red lighten-2" to="/register" dark  >{{$t("message.add")}} </v-btn>
    <v-spacer></v-spacer>

    <v-dialog
    v-model="dialog_d"
    max-width="290"
  >
    <v-card>
      <v-card-title class="headline">Удалить аккаунт</v-card-title>

      <v-card-text>
       Пользователь будет удален
      </v-card-text>

      <v-card-actions>
        <v-spacer></v-spacer>

        <v-btn color="green darken-1" text  @click="dialog_d = false " >
        Отмена
        </v-btn>

        <v-btn  color="green darken-1" text   @click="this.agree=true; dialog_d = false"  >
          Удалить
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
    
    <v-dialog v-model="dialog"  width="500" :hide-overlay="true"  :persistent="true"  >
      
      <v-card>
        <v-card-title
          class="headline grey lighten-2"
          primary-title
        >
        {{$t("message.playlists")}}
       
        </v-card-title>

        <v-card-text>
        <v-skeleton-loader class="mx-auto" v-if="loadead"  max-width="300" type="card"  ></v-skeleton-loader>
        <v-form >
        <div v-for="link in links" >
        <v-text-field :value="link.url+'/'+item.keyhash+'/'+link.type +'/playlist'+ link.ext" :label="link.type" ></v-text-field>
        <v-btn link class="ma-2"  :href="link.url+'/'+item.keyhash+'/'+link.type +'/playlist'+ link.ext" download ><v-icon >fas fa-download</v-icon>{{link.type}}</v-btn>

        </div>
      </v-form>
        </v-card-text>
      <v-divider></v-divider>

        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="primary"  text @click="close()" >
         {{$t("message.close")}} 
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
    {{$t("message.clients")}}
   
     <v-spacer></v-spacer>
      <v-text-field v-model="search" @change="searchUser()"  :label="$t('message.search')"   single-line  hide-details  ></v-text-field>
    </v-card-title>
   <!-- :options.sync="options"  :server-items-length="pagination.totalItems"   :footer-props="{itemsPerPageOptions: [2,4,8,10,20,50]}"   :page.sync="pagination.page :items-per-page.sync="pagination.itemsPerPage" -->
    <v-data-table v-if="loader"  :search="search" :headers="headers" :items="clients"   dense>
    
    <template v-slot:item.name="{ item }">
   <v-menu >
    <template v-slot:activator="{ on }">
    <div v-on="on">{{item.name}} </div>
    </template>
    <v-list>
    <v-list-item >
    <v-text-field v-model="item.ip" :label="$t('message.yourip')" ></v-text-field>
     </v-list-item>
  </v-list>
    </v-menu>
  </template>

    <template v-slot:item.package="{ item }">
    
    <v-row align="center">
    <v-col >
 
    <v-select :items="packages" @change="setPackage(item.user_id,item.packet,item.id)" v-model="item.packet" item-text="title"  selected item-value="id"  :label="$t('message.user_package')">
    
    </v-select>
    </v-col>
    </v-row>
   
  </template>
  <template v-slot:item.linkurl="{ item }">
  
  <div class="text-center">
  <v-btn small color="red lighten-2" dark v-if="item.packet > 0"  @click="loadLinks(item)" >
  {{$t("message.playlist")}}
  
  </v-btn>
     

  </div>
  </template>

  <template v-slot:item.expire="props">

  <v-icon small color="green" v-if="props.item.status == 'true'">fas fa-check</v-icon> <v-icon small color="red" v-else>fas fa-times</v-icon>
  
  <v-edit-dialog :return-value.sync="props.item.expire"  large  @save="updateDate(props.item)" > 
  {{props.item.expire}}
    <v-icon>mdi-calendar </v-icon>
     <template v-slot:input>
    <v-date-picker v-model="props.item.expire" @input="menu = false" show-current="true"></v-date-picker>
    </template>
 </v-edit-dialog>
  </template>

    <template v-slot:item.status="{ item }">
    <v-switch v-model="item.status"  true-value="true"  false-value="false"  @change="setStatus(item.user_id,item.status,item.keyhash)"   ></v-switch>
    </template>
    
    <template v-slot:item.config="props">
    
   <v-overlay :absolute="absolute" :value="overlay" opacity="0.89">
    <v-card  class="mx-auto"  max-width="900">
   <v-card-title>
      <v-btn icon @click="overlay = false">
      <v-icon>mdi-close</v-icon>
      </v-btn>{{$t("message.settings")}}
      </v-card-title>
     <v-card-text class="text--primary">
    <Subaccount :admin="true" :user="over_data.item" :links="links"> </Subaccount>
    </v-card-text>
    <v-card-actions>
    <v-btn small color="primary" @click="setToken(over_data.item.user_id, over_data.item.name)">Т
    <v-icon  >mdi-circle-edit-outline</v-icon>
    </v-btn>
   <v-btn small color="primary" :disabled="true" @click="cretaeStalkerAccount(over_data.item)">{{$t('message.create')}}
    <v-icon>fas fa-tv</v-icon>
    </v-btn>
    
    <v-switch v-model="item.adult"  true-value="true"  false-value="false" label="Adult"  @change="setAdult(over_data.item.user_id ,item.adult)"   ></v-switch>
   
    </v-card-actions>
  </v-card>

    </v-overlay>
   
    
    <v-btn small  @click="showOver(props)"><v-icon>mdi-cogs</v-icon></v-btn>
    </template>

    
  <template v-slot:item.description="props">
    <v-edit-dialog :return-value.sync="props.item.title"  large  @save="onChangeDesc(props.item)" > 
    <v-icon>mdi-square-edit-outline </v-icon>
     <template v-slot:input>
    <v-textarea outlined  v-model="props.item.description"   :label="$t('message.description')"></v-textarea>
    </template>
 </v-edit-dialog>
 </template>

 <template v-slot:item.user_limit="props">
  <v-edit-dialog :return-value.sync="props.item.title"  large  @save="onChangeLimit(props.item)" > 
  {{props.item.user_limit}} <v-icon color="blue" small> fas fa-tablet-alt</i> </v-icon>
    <template v-slot:input>
   <v-text-field v-model="props.item.user_limit" :label="$t('message.limit')"></v-text-field>
   </template>
</v-edit-dialog>
 </template>

    <template v-slot:item.action="{ item }">
    <v-icon color="red" @click="confirmAgree(item.user_id,item.id)">mdi-delete </v-icon>
  
  </template>
    </v-data-table>
   
    <v-sheet v-else color="darken-2" class="px-3 pt-3 pb-3" >
   
    
    <v-skeleton-loader class="mx-auto"  max-width="800"  type="table-heading, list-item-two-line, image, table-tfoot" ></v-skeleton-loader>
  </v-sheet>

  </v-card>
  <v-snackbar v-model="snack" :timeout="3000" >
  Название изменино
  <v-btn text @click="snack = false">$t("message.close")</v-btn>
</v-snackbar>
     </div>
     `,
  data: () => {
    
    return {
     
      snack : false,
      overlay :false,
      absolute: false,
      over_data : {},
      menu: false,
      pack: "",
      dialog: false,
      item: [],
      loader : false,
      dialog_d : false,
      agree : false,
      options: {},
      search : ''
    }
  },
  mounted() {
    const $this  = this
    this.$store.dispatch("Clients",{pages : this.pagination.page ,limit : this.pagination.itemsPerPage}).then(()=>$this.loader = true)
    this.$store.dispatch("getMPackage")
    this.$store.commit("load_links")
    this.$store.commit("search_set",{s:this.search})
   },

  computed: {
    clients:  {
      get : function(){
        return this.$store.state.clients
      },
    },

    pagination: {
      get: function () {
        return this.$store.getters.pagination
      },
     
    },
   

    headers: function(){
      return [
        { text: 'id', value: 'id' ,sortable: true },

        {
          text: this.$i18n.t('message.name'),
          align: 'left',
          sortable: true,
          value: "name",
        },
      
        { text:  this.$i18n.t('message.email'),  value: 'email' , sortable: false },
        { text:  this.$i18n.t('message.packet'), value: 'package' , sortable: true, },

        { text:  this.$i18n.t('message.valid_to'), value: 'expire', sortable: true, },
        { text:  this.$i18n.t('message.status'), value: 'status' ,  sortable: true},
        { text:  this.$i18n.t('message.settings'), value: 'config' , sortable: false },
        { text:  this.$i18n.t('message.labels'), value: 'description', sortable: false },
        { text: this.$i18n.t('message.limit'), value: 'user_limit',  sortable: false },
        { text: this.$i18n.t('message.actions'), value: 'action',  sortable: false },
       
        { text: this.$i18n.t('message.links'), value: 'linkurl' , sortable: false },
      ]

    },


    packages: function () {
      return this.$store.state.mpackages
    },
    links: function () {

      return this.$store.state.linkUrls
    },
    loadead: function status() {
      return this.$store.state.loaded
    }
  },
  watch: {
    options: {
      handler () {
       
        this.paginate()
      
      },
      deep: true,
    },
    
  },


  methods: {
    deleteUser(id,acc_id) {
    this.$store.dispatch("DeleteAccount", { user: id, id: acc_id })
      // this.$router.go()
    },

    paginate(){
      const $this  = this
     // this.loader = false
     
      this.$store.dispatch("Clients",{pages : this.pagination.page ,limit : this.pagination.itemsPerPage, q:this.search}).then(()=>$this.loader = true)

     },

     searchUser(){
      this.paginate()
     },

    confirmAgree(id,acc_id){
   
     this.deleteUser(id,acc_id)

    },



    setPackage(user_id,pack_id,acc_id) {
      this.$store.dispatch("UpdateUserPackage", { user_id: user_id, package_id:pack_id })
      this.$store.dispatch("UpdateStalkerPackage",{id: acc_id, package_id:pack_id })
     
    },
    setStatus(user, status, token) {
      this.$store.dispatch("UpdateUserStatus", { user_id: user, status: status ,token:token })
    },

    setToken(user_data,name) {
      const $this  = this
      console.log(name)
      this.$store.dispatch("updateToken", { user_id: user_data, name:name})
      this.$store.dispatch("Clients").then(()=>$this.loader = true)
      this. overlay =false
    },

    cretaeStalkerAccount(item){
      let random_password = Math.random().toString(36).substring(7);
     this.$store.dispatch("addStalkerAccount",{login:item.email,full_name:item.name,password:random_password, account_number:item.id, tariff_plan: item.packet,status:1})
     this. overlay =false
    },

    
    showOver(item){
      this.overlay = true
      this.over_data = item
    },

   loadLinks(item) {
      this.item = item
      this.$store.commit("load_links")
      this.dialog = true

    },
    close() {
      this.$store.commit("set_empty")
      this.dialog = false

    },
    onChangeDesc(item){
      this.$store.dispatch("changeDescription",{id:item.id, desc:item.description})
    },
    onChangeLimit(item){
      this.$store.dispatch("changeLimitDivice",{id:item.id, limit:item.user_limit})
    },
    save () {
      this.snack =true
     },

     updateDate(val) {
      this.$store.dispatch("expireUpdate",{id:val.id ,expire:val.expire })
     },

     setAdult(user, adult){
       this.$store.dispatch("sendAdult",{user_id: user, adult : adult})
     }


  }

}