<?php

namespace Iptv;

use Exception;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Exception\TwoFactorRequiredException;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Account;
use Coinbase\Wallet\Value\Money;
use Coinbase\Wallet\Enum\Param;


require ABSPATH . 'wp-content/plugins/iptv/lib/vendor/autoload.php';

class Payment
{

  /**
   * stripe payment
   */
  public static function payByStripe($request)
  {
    $key = get_option("option_iptv");
    \Stripe\Stripe::setApiKey($key['stripe_token2']);
    $client = \Stripe\PaymentIntent::create([
      'amount' => $request['data'] * 100,
      'currency' => 'gbp',
      'payment_method_types' => ['card'],
    ]);
    return $client;
  }

  /**
   * 2checkout payment
   */
  public static function payBy2Checkout($request)
  {

    $token  = $request['token'];
    $suma  = $request['suma'];
    $data  =   array(
      "Country" => "GB",
      "Currency" => "GBP",
      "Language" => "en",
      "BillingDetails" => array(
        "FirstName" => "Customer",
        "LastName" => "2Checkout",
        "Address1" => '123 Test St',
        "City" => 'North',
        "state" => 'Northamptonshire',
        "CountryCode" => 'GB',
        "Zip" => "NN14SQ",
        "Email" => 'testingtester@2co.com',
       // "Phone" => '555-555-5555',

      ),
      "Items" => [
        [
          // "Code" =>"ZNYADDZZ9D", 
          "Name" => "Credit",
          "Description" => "Credit balance",
          "IsDynamic" => true,
          "Tangible" => false,
          "PurchaseType" => "PRODUCT",
          "Quantity" => "1",
          "Price" => [
            "Amount" => $suma,
            "Type" => "CUSTOM"
          ],
        ]
      ],

      "PaymentDetails" => [

        "Currency" => "GBP",

        "PaymentMethod" => [
          "EesToken" => "$token",
          //   "CCID"=> "123",
          //   "CardNumber" => "4111111111111111",
          //   "CardNumberTime"=> "12",
          //   "CardType" => "VISA",
          //   "ExpirationMonth"=> "12",
          //   "ExpirationYear" => "2020",
          //   "HolderName" => "John Doe",
          //   "HolderNameTime" => "12",
          "RecurringEnabled" => false,
          "Vendor3DSReturnURL" => "www.test.com",
          "Vendor3DSCancelURL" => "www.test.com"
        ],
        "Type" => "TEST",
       // "TestOrder": false
      ]
 );
    // print_r(json_encode($data));
    // die;
     $options   =  get_option("option_iptv");
     //trim
    $merchantCode  = trim("{$options['checkout2_merchant']}");
    $key  = trim("{$options['checkout2_key']}");
   
   
    $string = strlen($merchantCode) . $merchantCode . strlen(gmdate('Y-m-d H:i:s')) . gmdate('Y-m-d H:i:s');
    $hash = hash_hmac('md5', $string, $key);
    $datetime = gmdate('Y-m-d H:i:s');
    $datarequest =  [
      'headers' => [
        'X-Avangate-Authentication' => "code=\"$merchantCode\" date=\"$datetime\" hash=\"$hash\" ",
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
      ],
      'body' => json_encode($data),
      ['timeout' => 60,]
    ];
    // print_r($datarequest);
    $url  = 'https://api.2checkout.com/rest/6.0/orders/';
    $req  = wp_remote_post($url, $datarequest);
    $body = wp_remote_retrieve_body($req);
    
    return $body;
  }

  /**
   * blockchain
   */
  public static function payByBlockchain($request)
  {
    try {
      $guid = "ae3b597e-5deb-4fa8-a811-28fb5d6aceb2";
      $main_password = "23bfa5af";
      $second_password = "";
      //126MCgPhV4nFkAQZtGnhLue25bafqBVZU8
      $address  = "1JMQgRzj35TbD3adEaiSA8JobBGka4goTC";
      //$address  = null ;
      $amount  = "0.005";
      $from  = 0;


      $Blockchain = new \Blockchain\Blockchain();
      $Blockchain->setServiceUrl('http://localhost:3000');
      $cred  = $Blockchain->Wallet->credentials($guid, $main_password);
       $response = $Blockchain->Wallet->send($address, $amount, $from);

      return $response;
    } catch (\Blockchain\Exception\ApiError $err) {
      return rest_ensure_response($err->getMessage());
    }
  }
  
/**
 * coinbase
 */
  public static function payByCoinBase($request)
  {
    try {
    
       $key   = get_option("option_iptv");
       $apiKey =  trim($key['coinbase_key']);
       $apiSecret =  trim($key['coinbase_secret']);
      // $configuration = Configuration::apiKey($apiKey, $apiSecret);
       $configuration = Configuration::oauth($request['token']);
      // $configuration->setApiUrl(Configuration::SANDBOX_API_URL);
      //echo $configuration->setLogger($logger);
       $client = Client::create($configuration);
       $account = $client->getPrimaryAccount();
       $suma  = $request['suma'];
     
       $transaction = Transaction::send([
        'toEmail' => 'utpudp@gmail.com',
       // 'bitcoinAmount' => 0.0001,
       // 'bitcoinAmount' => 0.0001133
       'amount' => new Money($suma, CurrencyCode::GBP),
    ]);
    
    
   try {
   
   // $client->createAccountTransaction($account, $transaction);
    
    } catch (TwoFactorRequiredException $e) {
        // show 2FA dialog to user and collect 2FA token
        $client->createAccountTransaction($account, $transaction, [
          Param::TWO_FACTOR_TOKEN => '123456',
      ]);
        //  return rest_ensure_response($e->getMessage());
     
    }
      
      $response = $client->decodeLastResponse();
       
       return $response;

    } catch (Exception $err) {
      return rest_ensure_response($err->getMessage());
    }
  }

  public function payByPaySiera()
  {
    try {
    } catch (Exception $err) {
    }
  }


  public static function transsaction()
  {
    try {
    } catch (Exception $err) {
    }
  }
}
