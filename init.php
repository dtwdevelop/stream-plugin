<?php
/**
 * Plugin Name
 *
 * @package           IptvPlugin
 * @author            dtw
 * @copyright         2019 Vladimir
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Iptv
 * Plugin URI:        https://example.com/plugin-name
 * Description:       tv stream plugin
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            dtw
 * Author URI:        https://example.com
 * Text Domain:       plugin-slug
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

include_once('IptvPublic.php');
include_once('IptvApi.php');
include_once('sql.php');
//$token ="ATihx4MwiwSx-w2VVaMnqXCXYsGIWUAqTkmrYNDjfRvcdMFfa2mAASN9_CGvPBs9CrNIXS4ALKj4uMBX";
$token  = get_option("option_iptv")['paypal_token'];
add_action( 'widgets_init','register_public_widget' );
add_action( 'rest_api_init', 'apitv' );
add_shortcode( 'itvp', 'barcode' );
// add_action('wp_enqueue_scripts', 'register_scripts');
wp_enqueue_script( 'app_vuejs', plugin_dir_url( __FILE__ ).'js/lib/vue.js', [] ,'2.6.11',false);
wp_enqueue_script( 'app_vuex', plugin_dir_url( __FILE__ ).'js/lib/vuex.js', [] ,'3.1.2',false);
wp_enqueue_script( 'wpvue_vuetify', plugin_dir_url( __FILE__ ).'js/lib/vuetify.js',[], '2.0' ,false);
wp_enqueue_script( 'wpvue_translate', plugin_dir_url( __FILE__ ).'js/lib/vue-i18n.js',[], '8.18.2 ' ,false);
wp_enqueue_script( 'wpvue_router', plugin_dir_url( __FILE__ ).'js/lib/vue-router.js',[], '3.1.3' ,false);
wp_enqueue_script( 'wpvue_vuelidate', plugin_dir_url( __FILE__ ).'js/lib/vuelidate.min.js',[], '3.1.3' ,false);
wp_enqueue_script( 'wpvue_validator', plugin_dir_url( __FILE__ ).'js/lib/validators.min.js',[], '3.1.3' ,false);
wp_enqueue_script( 'app_axios', plugin_dir_url( __FILE__ ).'js/lib/axios.min.js', [] ,'1.0',false);
wp_enqueue_script( 'app_mement', plugin_dir_url( __FILE__ ).'js/lib/moment.js', [] ,'1.0',false);
wp_enqueue_script( 'charts', plugin_dir_url( __FILE__ ).'js/lib/chart.min.js', [] ,'1.0',false);
wp_enqueue_script( 'charts_vue', plugin_dir_url( __FILE__ ).'js/lib/vue-chartjs.min.js', [] ,'1.0',true);
wp_enqueue_script('paypal', "https://www.paypal.com/sdk/js?client-id=$token&currency=GBP&disable-funding=credit,card",[],null,true);
wp_enqueue_script('stripe', "https://js.stripe.com/v3/",[],null,true);
wp_enqueue_script('check2pay', "https://2pay-js.2checkout.com/v1/2pay.js",[],null,true);
//wp_localize_script( 'wp-api', 'wpApiSettings', array( 'root' => esc_url_raw( rest_url() ), 'nonce' => wp_create_nonce( 'wp_rest' ) ));
wp_enqueue_script('my_store', plugin_dir_url( __FILE__ ).'js/store.js',[],'1.0',false);
//wp_enqueue_script('my_app', plugin_dir_url( __FILE__ ).'js/app.js',[],'1.0',true);
//wp_enqueue_script('my_admin', plugin_dir_url( __FILE__ ).'js/admin.js',[],'1.0',true);
wp_enqueue_script('app_router',plugin_dir_url( __FILE__ ).'js/app-router.js','', '', false);
wp_enqueue_style('material','https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css');
wp_enqueue_style('vuetify',plugin_dir_url( __FILE__ ).'css/vuetify.min.css');
wp_enqueue_style('icons','https://use.fontawesome.com/releases/v5.0.13/css/all.css');
wp_enqueue_style('font1','https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900');


register_activation_hook( __FILE__,'iptv_install');

add_filter('script_loader_tag', 'add_type_attribute' , 10, 3);
function add_type_attribute($tag, $handle, $src) {
   // print($handle.": ");
    // if not your script, do nothing and return original $tag
    if ( 'app_router' !== $handle ) {
        return $tag;
    }
    // change the script tag by adding type="module" and return it.
    $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
    return $tag;
}

add_action( 'cron_clear_popular', 'bl_clear_popular' );
add_action( 'cron_user_expire', 'check_if_expire' );

add_filter( 'cron_schedules', 'twenty_cron_interval' );
function twenty_cron_interval( $schedules ) { 
    $schedules['twenty_seconds'] = array(
        'interval' => 20,
        'display'  => esc_html__( '20 Seconds' ), );
    return $schedules;
}


if ( ! wp_next_scheduled( 'cron_clear_popular') ) {
    wp_schedule_event( time(), 'daily', 'cron_clear_popular' );
}

if ( ! wp_next_scheduled( 'cron_user_expire') ) {
    //daily
    wp_schedule_event( time(), 'twicedaily', 'cron_user_expire' );
  //  wp_schedule_event( time(), 'twenty_seconds', 'cron_user_expire' );
}



/**
 * clear popular chanels after one day
 */
function bl_clear_popular(){
   try{
     global $wpdb;
   
     $table_statistics  = $wpdb->prefix . 'iptv_statistics';
     $table_archive = $wpdb->prefix . 'iptv_statistics_archive';
     $wpdb->query("TRUNCATE TABLE $table_statistics");
    $wpdb->query("TRUNCATE TABLE $table_archive");
   }
   catch(Exception $err){
  
   }
}

/**
 * check every day if user expire
 */
function check_if_expire(){
    try{
        global $wpdb;
        $iptv_users = $wpdb->prefix . 'iptv_accounts';
        $query = "SELECT id, keyhash, expire as user_expire  FROM  $iptv_users ";
       $accounts = $wpdb->get_results($query);
       if($accounts != null){
           foreach($accounts as $account){
            $now = (new DateTime("now"))->format('Y-m-d');
            $account_expire = (new DateTime($account->user_expire))->format('Y-m-d');
          
           if( $now >  $account_expire ){
              $wpdb->update($iptv_users,['status'=>'false'],['id'=>$account->id]);
              //stalker and portal user disabled
              cron_delete_stalker($account->id ,$account->keyhash ,false);
              }
           }
       }

    }
    catch(Exception $err){
                 
    }
}

//register_deactivation_hook( __FILE__, 'bl_deactivate' ); 
 
function bl_deactivate() {
    $timestamp = wp_next_scheduled( 'cron_clear_popular' );
    wp_unschedule_event( $timestamp, 'cron_clear_popular' );
}

function bl_print_tasks() {
    echo '<pre>'; print_r( _get_cron_array() ); echo '</pre>';
}
//bl_print_tasks();
//check_if_expire();


function rewrite_vue_urls() {
    //http://localhost/billing/auth/?code
  //  add_rewrite_rule('^billing?','index.php?post_type=billing','top');
    add_rewrite_rule('^billing/auth?','index.php?pagename=billing','top');
    add_rewrite_rule('^billing/account?','index.php','top');
    add_rewrite_rule('^billing/develop?','index.php','top');
    add_rewrite_rule('^billing/payment?','index.php','top');
    add_rewrite_rule('^billing/register?','index.php','top');
    add_rewrite_rule('^billing/restore','index.php?pagename=billing','top');
    add_rewrite_rule('^billing/pass-change?','index.php?pagename=billing&res-conf','top');
    add_rewrite_rule('^billing/activation?','index.php?pagename=billing','top');
  }
  add_action('init', 'rewrite_vue_urls');
  add_filter( 'query_vars', 'mytypevars' );

function mytypevars( $vars ) {
    $vars[] = 'res-conf';
  

    return $vars;
}