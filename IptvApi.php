<?php

/**
 * api iptv 
 */

use Iptv\Payment;
use Iptv\Email;

include_once(ABSPATH . 'wp-content/plugins/iptv/utils.php');
include_once(ABSPATH . 'wp-content/plugins/iptv/Payment.php');
include_once(ABSPATH . 'wp-content/plugins/iptv/IptvPortalApi.php');
include_once(ABSPATH . 'wp-content/plugins/iptv/Email.php');
require ABSPATH . 'wp-content/plugins/iptv/lib/vendor/autoload.php';


class IptvApi
{
    public $auth;
    public $url;


    public function __construct()
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix . 'iptv_categories';
        $this->table_name2 = $wpdb->prefix . 'iptv_chanels';
        $this->table_name3 =  $wpdb->prefix . 'iptv_packages';
        $this->users = $wpdb->prefix . 'users';
        $this->balance = $wpdb->prefix . 'iptv_balance';
        $this->iptv_users = $wpdb->prefix . 'iptv_accounts';
        $this->sub_accounts = $wpdb->prefix . 'iptv_tokens';
        $this->table_cross = $wpdb->prefix . 'iptv_packages_categories';
        $this->table_stalker  = $wpdb->prefix . 'iptv_stalker';
        $this->table_statistics  = $wpdb->prefix . 'iptv_statistics';
        $this->table_archive = $wpdb->prefix . 'iptv_statistics_archive';
        $option  =  get_option("option_iptv");
        $this->url  = $option['url'];
        $this->auth = ['login' => $option['login'], 'password' => $option['password']];
        $this->args =  $args = ['headers' => ['Authorization' => 'Basic ' . base64_encode($this->auth['login'] . ':' .  $this->auth['password'])], ['timeout' => 120,]];
    }
    /**
     *  get chanels list from api 
     */
    public function get_chanel_list($request)
    {
        try {
            $url_api  =  get_option("option_iptv")['stat_link'];

            $command = $request['param'];
            $url  = "$url_api$command";
            $data  = wp_remote_get($url, $this->args);
            $body = wp_remote_retrieve_body($data);
            $chanels_names  = [];
            foreach (json_decode($body) as $ch) {
                $chanels_names[] = $ch->value->name;
            };
            $chanels_titles = [];
            foreach ($chanels_names as $chanel_name) {
                $title = $this->get_chanel_urls($chanel_name);

                $chanels_titles[] = [
                    "chanel" => $chanel_name,
                    "title" => $title['title']
                ];
            }
            //json_decode
            return rest_ensure_response($chanels_titles);
        } catch (Exception $err) {
            return rest_ensure_response("Connection error $err");
        }
    }

    /**
     * get chanel url from api
     */
    public function get_chanel_urls($chanel)
    {
        $url_api  =  get_option("option_iptv")['stat_link'];
        $param  = 'media_info/' . $chanel;
        $url  = "$url_api$param";
        try {
            $data  = wp_remote_get($url, $this->args);
            $body = wp_remote_retrieve_body($data);
        } catch (Exception $err) {
            $data = "Connection error";
        }

        return json_decode($body, true);
    }

    /**
     * add package to table
     */
    public function add_main_package($request)
    {
        global $wpdb;
        try {
            $user =  wp_get_current_user();
            $package = $request['payload']['package'];

            $wpdb->insert(
                $this->table_name3,
                [
                    "package_id" => 1,
                    "user_ID" => 1,
                    "tariff_id" =>   $package['tariff']['id'],
                    "title" => $package['title'],
                    "description" => $package['description'],
                    "price" => (float) $package['price'],
                    "archive" => (int) $package['archive'],
                    "expire" => current_time('mysql')
                ]
            );

            $inser_id = $wpdb->insert_id;
            // $this->add_cross_categories($package['select'], $inser_id);
            return rest_ensure_response(["last_id" => $inser_id]);
        } catch (Exception $err) {
            return $wpdb->show_errors();
        }
    }

    /**
     * update title
     */
    public function package_title_update($request)
    {
        try {
            global $wpdb;
            $id = (int) $request['id'];
            $res = $wpdb->update($this->table_name3, ['title' => $request['title']], ['id' => $id]);
            return rest_ensure_response($res);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * update package price
     */
    public function package_price_update($request)
    {
        try {
            global $wpdb;
            $id   = (int) $request['id'];
            $price = number_format($request['price'], 2);

            //    die;

            $wpdb->update($this->table_name3, ["price" => $price], ["id" => $id]);
            return rest_ensure_response($request);
        } catch (Exception $error) {
            return rest_ensure_response($error);
        }
    }

    /**
     * add category to package by category id
     */
    public function add_package_category($request)
    {
        try {
            global $wpdb;
            $package_id = (int) $request['package_id'];
            $this->add_cross_categories($request['select'], $package_id);
            return rest_ensure_response($request['select']);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * cross table link to package and category id
     */
    public function add_cross_categories($data, $package_id)
    {
        try {
            global $wpdb;
            foreach ($data as $data) {
                $wpdb->insert($this->table_cross, [
                    'package_id' => $package_id,
                    'category_id' =>  $data['value']
                ]);
            }
        } catch (Exception $err) {
        }
    }

    /**
     * get all packages from table
     */
    public function get_mpackages($request)
    {
        try {
            global $wpdb;
            $packages = $wpdb->get_results("SELECT *  FROM  $this->table_name3");
            return rest_ensure_response($packages);
        } catch (Exception $err) {
        }
    }

    /**
     * delete mpackege
     */
    public function mpackage_delete($request)
    {
        try {
            $id = (int) $request['id'];
            global $wpdb;
            $wpdb->delete($this->table_cross, array('package_id' => $id));
            $wpdb->delete($this->table_name3, array('id' => $id));
            return rest_ensure_response(['id' => $id]);
        } catch (Exception $error) {
            return rest_ensure_response(['error' => $err]);
        }
    }

    /**
     * add category to table
     */

    public function add_package($request)
    {
        global $wpdb;
        try {
            $user  = wp_get_current_user();
            $wpdb->insert(
                $this->table_name,
                [
                    'title' => $request['payload']['package']['title'],
                    'description' => $request['payload']['package']['description'],
                    'total' => count($request['payload']['package']['select']),
                    'expire' => current_time('mysql'),
                ]
            );
            $inser_id = $wpdb->insert_id;
            $this->add_chanel($request['payload']['package']['select'], $inser_id);
        } catch (Exception $err) {
            return $wpdb->show_errors();
        }

        $err = $wpdb->last_query;
        return rest_ensure_response(["last_id" => $inser_id]);
    }

    /**
     * add chanels to category by id
     */
    public function add_chanels_to_category($request)
    {
        try {

            $id  = (int) $request['id'];

            $this->add_chanel($request['select'], $id);
            return rest_ensure_response($request['select']);
        } catch (Exception $err) {
            return rest_ensure_response($request);
        }
    }

    public function update_category($request)
    {
        global $wpdb;
        try {
            $category = $request['category'];

            $id = (int) $category['id'];
            global $wpdb;
            $wpdb->update($this->table_name, ['title' => $category['title'], 'description' => $category['description']],  ['id' => $id]);

            return rest_ensure_response($category);
        } catch (Exception $err) {
            return rest_ensure_response("invalid id");
        }
    }


    /**
     *  add chanel totable
     */
    private function add_chanel($datas, $id)
    {

        global $wpdb;
        try {
            foreach ($datas as $data) {
                $url = $data['chanel'];
                $wpdb->insert(
                    $this->table_name2,
                    [
                        'package_id' => $id,
                        'title' => $data['title'],
                        'link' =>  $url,
                        'dataid' =>  $data['dataid'],
                        'logo' => 'logo',
                        'chanel' => $data['chanel']
                    ]
                );
            }
        } catch (Exception $err) {
            return $wpdb->show_errors();
        }
    }

    /**
     * update package list
     */
    public function update_package_list($post, $request)
    {
    }

    /**
     * edit category
     */
    public function edit_category($request)
    {
        try {
            $category_id = (int) $request['id'];
            global $wpdb;
            $query = $wpdb->prepare("SELECT * from $this->table_name WHERE id=%d", $category_id);
            $category = $wpdb->get_row($query);
            $query2 = $wpdb->prepare("SELECT *  FROM  $this->table_name2 where package_id=%d", $category->id);
            $chanels = $wpdb->get_results($query2);
            $data = ['category' => $category];
            $data['chanels'] = $chanels;
            return rest_ensure_response($data);
        } catch (Exception $err) {
            return rest_ensure_response("invalid id");
        }
    }




    /**
     * get lists categories
     */
    public function list_packages()
    {
        try {
            global $wpdb;

            $packages = $wpdb->get_results("SELECT *  FROM  $this->table_name");
            return rest_ensure_response($packages);
        } catch (Exception $err) {
            return rest_ensure_response('db connection problem');
        }
    }

    /**
     * get all categories by package id
     */

    public function get_category_by($request)
    {
        try {
            $package_id  = $request['id'];
            global $wpdb;
            $query = $wpdb->prepare("SELECT category_id FROM  $this->table_cross WHERE package_id=%d", $package_id);
            $categories  =  $wpdb->get_results($query);
            $data = [];
            foreach ($categories as $category) {

                $query2 = $wpdb->prepare("SELECT * FROM  $this->table_name WHERE id=%d", $category->category_id);
                $data[]  =  $wpdb->get_row($query2);
            }

            return rest_ensure_response($data);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * remove category from package
     */

    public function remove_category_by($request)
    {
        try {
            $category_id = (int) $request['id'];
            $package_id  = (int) $request['package_id'];
            global $wpdb;
            $responce = $wpdb->delete($this->table_cross, ['category_id' => $category_id, 'package_id' => $package_id]);
            return rest_ensure_response(['id' => $responce]);
        } catch (Exception  $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * return chanels by category
     */
    public function get_chanels($categories)
    {
        try {
            $categories_list  = [];
            global $wpdb;
            foreach ($categories as  $category) {
                $query = $wpdb->prepare("SELECT *  FROM  $this->table_name2 where package_id=%d order by title", $category->category_id);
                $query2 = $wpdb->prepare("SELECT *  FROM $this->table_name where id=%d", $category->category_id);
                $row_category = $wpdb->get_row($query2);
                $chanels = $wpdb->get_results($query);
                $categories_list[] = [$row_category->title => $chanels];
            }
            return $categories_list;
        } catch (Exception $err) {
            return rest_ensure_response('cant get chanels');
        }
    }

    /**
     * get all chanel
     */
    public function getAllChanels()
    {
        try {
            global $wpdb;
            $chanels = $wpdb->get_results("SELECT chanel, title FROM $this->table_name2 group by title ORDER BY title ASC");
            return  rest_ensure_response($chanels);
        } catch (Exception $err) {
            return rest_ensure_response($chanels);
        }
    }

    /**
     * chanels details
     */
    public function chanel_detail($request)
    {


        $param  = 'media?name=' . $request['chanel'];
        $url  = "$this->url$param";
        try {
            $data  = wp_remote_get($url, $this->args);
            $body = wp_remote_retrieve_body($data);
        } catch (Exception $err) {
            $data = "Connection error";
        }
        return json_decode($body, true);
    }
    /**
     * update chanel
     */
    public function update_chanel($request)
    {
        try {
            global $wpdb;

            $chanels = $request['category'];
            $id = (int) $chanels['id'];
            global $wpdb;
            $wpdb->update($this->table_name2, ['title' =>  $chanels['title'], 'dataid' => (int) $chanels['dataid']],  ['id' => $id]);
            return rest_ensure_response($chanels);
            return rest_ensure_response();
        } catch (Exception $err) {
            return rest_ensure_response("invalid id");
        }
    }

    /**
     * upload chanel logo
     */
    public function upload_chanel_logo($request)
    {
        try {
            global $wpdb;
            global $wp_filesystem;
            $id  = (int) $request['id'];
            $params_upload  = $request->get_file_params();
            $logo  = "logo_chnael_" . time() . ".png";
            // print_r($logo);
            // print_r($params_upload['logo']['tmp_name']);
            //  print_r($params_upload['logo']['name']);
            if (empty($wp_filesystem)) {
                require_once(ABSPATH . '/wp-admin/includes/file.php');
                WP_Filesystem();
            }
            // $uploads =  wp_upload_dir();
            // $dir  = WP_CONTENT_DIR . "/uploads/chanellogo";
            // $dir  = $uploads."/uploads/chanellogo";

            $upload = wp_upload_bits($logo, null, file_get_contents($params_upload['logo']['tmp_name']));
            $wpdb->update($this->table_name2, ['logo' => $upload['url']],  ['id' => $id]);
            return rest_ensure_request(["logo" => $upload['url']]);
        } catch (Exception $err) {
            return rest_ensure_request("chanel logo not upload");
        }
    }

    /**
     * delete chanels
     */
    public function chanel_delete($request)
    {
        $chanel_id  = (int) $request['id'];
        global $wpdb;
        try {
            $responce = $wpdb->delete($this->table_name2, array('id' => $chanel_id));
            return rest_ensure_response(['id' => $responce]);
        } catch (Exception $err) {
            return rest_ensure_response("chanel not deleted");
        }
    }

    /**
     * get list chanel by packade id
     * @param request
     */

    public function list_chanels_by_package($request)
    {
        try {
            global $wpdb;
            $package_id = (int) $request['param'];
            if ($package_id == null) throw new Exception("not prama id");
            $query = $wpdb->prepare("SELECT category_id FROM $this->table_cross where package_id=%d", $package_id);
            $categories = $wpdb->get_results($query);
            $chanels  = $this->get_chanels($categories);
            $ext = create_mediu($chanels, $package_id, $request['type']);
            $link  =  playlist($ext);
            $data_responce = ['chanels' => $chanels, "link" => $link];
            return rest_ensure_response($data_responce);
        } catch (Exception $err) {
            return rest_ensure_response('db connection problem ');
        }
    }

    /**
     * create for all link type playlist
     */
    public function package_urls($request)
    {
        try {

            $file_types = ['m3u8', 'm3u', 'ottplayer', 'siptv', 'ssiptv'];
            //  $urls  = [];
            global $wpdb;
            $hash =  $request['hash'];
            $type =  $request['type'];
            $token  = $this->check_sub_token($request['hash']);
            if ($token != null)   $hash  = $token;
            $query1 = $wpdb->prepare("SELECT category_id as id , adult FROM  $this->iptv_users where keyhash=%s", $hash);
            $package = $wpdb->get_row($query1);

            if ($package->id == null) throw new Exception("not prama id");
            $query = $wpdb->prepare("SELECT category_id FROM $this->table_cross where package_id=%d", $package->id);
            $categories = $wpdb->get_results($query);
            $chanels  = $this->get_chanels($categories);
            $adult = "{$package->adult}";


            $ext = create_mediu($chanels, $package->id, $type, $request['hash'], $package->adult);
            $link  = playlist($ext);
            $url = ['url' => $link['url'], 'type' => $type];
            $responce =  new WP_REST_Response();
            $download  = $url['url'];


            header('Content-Type: audio/x-mpegurl');
            // header('Content-Type: text/plain');
            //  header('Content-Disposition: attachment; filename="'.$download.'"');
            header('Expires: -1');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            // header('Content-Length: ' . filesize($download));
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );
            $res  =  file_get_contents($download, false, stream_context_create($arrContextOptions));
            echo $res;
            exit;
        } catch (Exception $err) {
            return rest_ensure_response("failed generate links");
        }
    }

    /**
     * get category by package id
     */
    public function get_package_by_id($id)
    {
        try {
            global $wpdb;
            $user_id  =  $id;
            $package = $wpdb->get_results("SELECT *  FROM  $this->table_name3 where id=$user_id");
            return $package;
        } catch (Exception $err) {
        }
    }

    /**
     * get user_iptv account
     */
    public function user_account($request)
    {
        try {
            global $wpdb;
            $id = (int) $request['id'];

            $user_data = $wpdb->get_results("SELECT user_login ,ip,user_email as email, tv.id, tv.adult , tv.user_id, tv.keyhash, DATE(tv.expire) as expire, tv.session_limit, tv.category_id , tv.status FROM  $this->iptv_users  tv
            left  join $this->users   user
            on  user.ID = tv.user_id
            where  user_id=$id");

            $package  = $this->get_package_by_id($user_data[0]->category_id);
            $balance = $this->account_balance_id(['id' => $user_data[0]->id]);
            $data = ['user' => $user_data, 'package' => $package, 'balance' => $balance];
            // echo($wpdb->last_error);

            return rest_ensure_response($data);
        } catch (Exception $err) {
            return rest_ensure_response('not data' + err);
        }
    }

    /**
     * delete category
     */
    public function delete_package($request)
    {
        try {
            $id = (int) $request['id'];

            global $wpdb;
            $wpdb->delete($this->table_name, array('id' => $id));
            $wpdb->delete($this->table_name2, array('package_id' => $id));
            rest_ensure_response(['id' => $wpdb->insert_id]);
        } catch (Exception $err) {
            return rest_ensure_response('db connection problem');
        }
    }


    /**
     *  create user iptv
     */
    public function add_user($request)
    {

        try {
            $user  =  $request['client'];
            $responce =  wp_create_user($user['login'], $user['password'], $user['email']);
            if ($responce->errors) {
                return rest_ensure_response(["error" => $responce->errors]);
            }
            $ip = get_the_user_ip();
            $this->iptv_user_create($responce, $ip, $user);
            // Email::sendEmail(['type'=>'register', 'user_id'=>$responce , 'email_to'=> $user['email']  ]);
            return rest_ensure_response($responce);
        } catch (Exception $err) {
            return $wpdb->show_errors();
        }
    }


    /**
     * update user password
     */
    public function update_user_password($request)
    {
        try {
            $user_id  = (int) $request['id'];
            $password = $request['pass'];
            wp_set_password($password,  $user_id);
            return rest_ensure_response($request);
        } catch (Exception $err) {
            return $wpdb->show_errors();
        }
    }



    /**
     * create user iptv in table
     */
    public function iptv_user_create($data_id, $ip, $user)
    {
        try {

            global $wpdb;
            $user_iptv = [

                'category_id' => 0,
                'ip' =>  "$ip",
                'description' => ' ',
                'expire' => current_time('mysql'),
                'user_id' => $data_id,
                // 'package_id' => 0,
                'keyhash' => create_token($user['name']),
                'status'  => 'true',
                'adult' => 'true'
            ];

            $wpdb->insert($this->iptv_users, $user_iptv);
            $user_id = $wpdb->insert_id;
            $this->init_balance($user_id);
        } catch (Exception $err) {
            return $wpdb->show_errors();
        }
    }

    public function if_user_active($request)
    {
        $id   = (int) $request['id'];
     
        global $wpdb;
        $result = $wpdb->get_row($wpdb->prepare("SELECT ID , user_status  FROM $this->users WHERE ID=%d", $id));

        if ($result != null) {
            return    rest_ensure_response(['status' =>  $result->user_status]);
        }
        return    rest_ensure_response(['status' => 0]);
    }

    /**
     * activate user
     */
    public function active_user($request)
    {
        try {
            
            $id   = (int) $request['id'];
             global $wpdb;
            $result = $wpdb->get_row($wpdb->prepare("SELECT ID  FROM $this->users WHERE ID=%d", $id));

            if ($result != null) {
                $wpdb->update($this->users, ['user_status' => 1], ['ID' => $id]);
                return    rest_ensure_response(['status' => true]);
            } else {
                return    rest_ensure_response(['status' => false]);
            }
        } catch (Exception $err) {
            return    rest_ensure_response(['status' => false]);
        }
    }


    /**
     * delete user account 
     */

    public function delete_user_account($request)
    {
        try {
            global $wpdb;
            require_once(ABSPATH . 'wp-admin/includes/user.php');
            $user_id = (int) $request['user'];
            $id = (int) $request['id'];
            $wpdb->delete($this->users, ['ID' => $user_id], " ID > 1 ");
            $this->delete_all_sub_accounts($user_id);
            wp_delete_user($user_id);
            $wpdb->delete($this->iptv_users, ['user_id' => $user_id]);
            $wpdb->delete($this->balance, ['user_id' => $id]);
            $wpdb->delete($this->table_stalker, ['user_id' => $id]);
            $this->delete_stalker_account(['id' => $id]);
            return rest_ensure_response("user deleted");
        } catch (Exception $error) {
            return rest_ensure_response($error->getMessage());
        }
    }



    /**
     * delete all sub accounts
     */

    public function delete_all_sub_accounts($user_id)
    {
        try {
            global $wpdb;
            $main_token = $this->get_user_token_by_id($user_id);
            $query  = $wpdb->prepare("SELECT id , token  FROM $this->sub_accounts WHERE token_user=%s", $main_token->keyhash);
            $sub_accounts  = $wpdb->get_results($query);

            if ($sub_accounts != null) {
                foreach ($sub_accounts as $sub_account) {
                    $this->delete_sub_token(['id' => $sub_account->id, 'token' => $sub_account->token]);
                }
            }
        } catch (Exception $err) {
            return rest_ensure_response($error->getMessage());
        }
    }


    /**
     * update user description
     */
    public function user_desc_update($request)
    {
        try {
            global $wpdb;
            $id   = (int) $request['id'];
            $wpdb->update($this->iptv_users, ["description" => $request['desc']], ["id" => $id]);
            return rest_ensure_response($request);
        } catch (Exception $error) {
            return rest_ensure_response($error);
        }
    }

    /**
     * update user session limit tv divice
     */
    public function user_limit_sesstion($request)
    {
        try {
            global $wpdb;
            // print_r($request);
            $id   = (int) $request['id'];
            $limit_session  = (int)   $request['limit'];
            $wpdb->update($this->iptv_users, ["session_limit" => $limit_session], ["id" => $id]);
            return rest_ensure_response($request);
        } catch (Exception $error) {
            return rest_ensure_response($error);
        }
    }

    /**
     * 
     */
    public function update_expire($request)
    {
        try {
            global $wpdb;
            // print_r($request);
            $id   = (int) $request['id'];
            $expire_time  =    $request['expire'];
            $wpdb->update($this->iptv_users, ["expire" =>  $expire_time], ["id" => $id]);
            //25-08-2020
            $date = new DateTime($expire_time);
            $stalker_date = $date->format('Y-m-d H:i:s');
            $token = $this->get_user_token($id);
            if ($token != null) {
                $sub_accounts = $this->get_all_subaccounts(['token' => $token]);

                foreach ($sub_accounts->data as $sub_account) {
                    if ($sub_account->type == 'stalker') {
                        $rez = $this->change_data_expire_stalker(['id' => "{$sub_account->token}", 'expire' => $stalker_date]);
                        sleep(1);
                    }
                }
                return rest_ensure_response($request);
            } else {
                return rest_ensure_response($request);
            }
        } catch (Exception $error) {
            return rest_ensure_response($error);
        }
    }


    /**
     * get all user from table
     */
    public function add_all_users($request)
    {
        try {

            global $wpdb;

            $per_page  = ($request['limit']) ? $request['limit'] : 10;
            $page = ($request['page']) ? abs((int)$request['page']) : 1;
            $total_query = "SELECT COUNT(*) FROM  $this->iptv_users ";
            $total = $wpdb->get_var($total_query);
            // $offset = ( $page *  $per_page ) - $per_page ;
            $offset = ($page * $per_page) - $per_page;


            $packages = $wpdb->get_results(
                "SELECT tv.id, tv.user_id, tv.ip, user.user_login, user.user_email,user.user_status  , DATE(tv.expire) as expire , tv.description, tv.keyhash, tv.status ,tv.category_id  , tv.id , tv.session_limit 
            FROM  $this->iptv_users tv
            left  join $this->users   user
            on  user.ID = tv.user_id
            where user.ID > 1 "
            );
            //LIMIT $offset , $per_page
            return rest_ensure_response(['users' => $packages, 'total' => $total]);
        } catch (Exception $err) {
            return rest_ensure_response('db connection problem');
        }
    }
    /**
     * user package set
     */
    public function user_package_update($request)
    {
        try {
            global $wpdb;
            $user_id = (int) $request['user_id'];
            $package_id  = (int) $request['package_id'];
            if ($rez = $wpdb->update($this->iptv_users, ['category_id' => $package_id, 'status' => 'true'], ['user_id' => $user_id])) {
                $this->update_sub_package_tariff($user_id, $package_id);
                return rest_ensure_response($package_id);
            } else {
                return rest_ensure_response(['error' => 'update failed']);
            }
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * update all sub package id
     */
    public function update_sub_package_tariff($user_id, $package_id)
    {
        try {
            global $wpdb;
            $main_token = $this->get_user_token_by_id($user_id);
            $query  = $wpdb->prepare("SELECT id , token  FROM $this->sub_accounts WHERE token_user=%s", $main_token->keyhash);
            $sub_accounts  = $wpdb->get_results($query);

            if ($sub_accounts != null) {
                foreach ($sub_accounts as $sub_account) {
                    $this->change_stalker_package(['id' => $sub_account->id, 'package_id' => $package_id]);
                    $this->update_portal_package($sub_account->token,$package_id);
                   
                }
            }
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }
    public function update_portal_package($sub_account,$package_id){
        try{
            $package  = $this->get_tariff_by_package($package_id);
            $portal = \Iptv\ServiceRemote::chnage_package($sub_account,$package);
           

        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

    /**
     * update user status
     */
    public function user_status($request)
    {
        try {
            global $wpdb;
           
            $user_id = (int) $request['user_id'];
            $status  =  $request['status'];
            if ($rez = $wpdb->update($this->iptv_users, ['status' => $status], ['user_id' => $user_id])) {
                $this->disable_user_portal(['token'=>$request['token'],'status'=>$status]);
                return rest_ensure_response($rez);
            } else {
                return rest_ensure_response(['error' => 'update failed']);
            }
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    public function get_user_token_by_id($user_id)
    {
        try {
            global $wpdb;
            $query1 = $wpdb->prepare("SELECT keyhash  from  $this->iptv_users where user_id=%d", $user_id);
            return  $wpdb->get_row($query1);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * update user token
     */

    public function update_token($request)
    {
        try {
            global $wpdb;
            $user_id = (int) $request['user_id'];
            $token  =  create_token($request['name']);
            $old_token = $this->get_user_token_by_id($user_id);

            if ($rez = $wpdb->update($this->iptv_users, ['keyhash' => $token], ['user_id' => $user_id])) {

                $this->update_sub_token($old_token->keyhash, $token);
                return rest_ensure_response($rez);
            } else {
                return rest_ensure_response(['error' => 'update failed']);
            }
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * fill balance
     */
    public function fill_balance($request)
    {
        try {
            global  $wpdb;

            $rez  =   $wpdb->get_results("Select id from $this->iptv_users");

            foreach ($rez as $row) {

                $wpdb->insert($this->balance, [

                    'user_id' => $row->id,
                    'balance' => 0.00,
                    'created' => current_time('mysql'),
                    'updated' => current_time('mysql'),

                ]);
            }

            $err  = $wpdb->last_error;
            return rest_ensure_response(['err' => $err]);
        } catch (Exception $rr) {
            return rest_ensure_response($err);
        }
    }

    /**
     * init balance 
     */

    public function init_balance($user_id)
    {
        try {
            global  $wpdb;
            $wpdb->insert($this->balance, [
                'user_id' => $user_id,
                'balance' => 0.00,
                'created' => current_time('mysql'),
                'updated' => current_time('mysql'),

            ]);
            $err  = $wpdb->last_error;
            return rest_ensure_response(['err' => $err]);
        } catch (Exception $rr) {
            return rest_ensure_response($err);
        }
    }

    /**
     * get balance by account id
     */

    public function  account_balance_id($request)
    {
        try {
            $user_id =  (int) $request['id'];

            global $wpdb;
            $query1 = $wpdb->prepare("SELECT *  from  $this->balance where user_id=%d", $user_id);
            $balance =  $wpdb->get_row($query1);
            return rest_ensure_response($balance);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * update balance
     */
    public function update_balance($request)
    {
        try {

            global $wpdb;
            $id = $request['id'];
            $balance = $request['balance'];
            $wpdb->update($this->balance, ['balance' => $balance], ['user_id' => $id]);
            return rest_ensure_response($request);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * update archive status
     */
    public function update_archive($request)
    {
        try {

            global $wpdb;
            $id   = (int) $request['id'];
            $archive = (int) $request['archive'];

            $wpdb->update($this->table_name3, ["archive" => $archive], ["id" => $id]);
            return rest_ensure_response($request);
        } catch (Exception $error) {
            return rest_ensure_response($error);
        }
    }

    /**
     * update adult protection from child
     */

    public function update_adult($request)
    {
        try {

            global $wpdb;
            $id   = (int) $request['user_id'];
            $wpdb->update($this->iptv_users, ["adult" => $request['adult']], ["user_id" => $id]);

            return rest_ensure_response($request);
        } catch (Exception $error) {
            return rest_ensure_response($error);
        }
    }

    /**
     * login in account
     */
    public function loginIn($request)
    {
        try {
            $user  = $request;
            // $wpuser  = wp_authenticate($user['login'],$user['password']);
            $wpuser = wp_signon(["user_login" => $user['login'], "user_password" => $user['password'], 'remember' => true], true);

            return rest_ensure_response($wpuser);
        } catch (Exception $err) {
            return rest_ensure_response('db connection problem');
        }
    }

    /**
     * logout from account
     */
    public function Logout($request)
    {
        try {
            wp_logout();
            return rest_ensure_response(["logout" => true]);
        } catch (Exception $err) {
            return rest_ensure_response('db connection problem');
        }
    }

    /**
     * save setting
     */
    public function save_setting($request)
    {
        try {

            $setting  = $request['setting']['data'];
            //    print_r($setting);
            // die;
            $data = [
                'login' => $setting['login'],
                'password' => $setting['password'],
                'url' => $setting['url'],
                'sub_url' => $setting['sub_url'],
                'domain' => $setting['domain'],
                'epg' => $setting['epg'],
                'login_stalker' => $setting['login_stalker'],
                'password_stalker' => $setting['password_stalker'],
                'url_stalker' => $setting['url_stalker'],
                'paypal_token' => $setting['paypal_token'],
                'paypal_status' => $setting['paypal_status'],
                'stripe_token' => $setting['stripe_token'],
                'stripe_token2' => $setting['stripe_token2'],
                'stripe_token' => $setting['stripe_token'],
                'stripe_status' => $setting['stripe_status'],
                'checkout2_key' => $setting['checkout2_key'],
                'checkout2_merchant' => $setting['checkout2_merchant'],
                'checkout2_status' => $setting['checkout2_status'],
                'stat_link' => $setting['stat_link'],
                'coinbase_key' => $setting['coinbase_key'],
                'coinbase_secret' => $setting['coinbase_secret'],
                'coinbase_client_k' => $setting['coinbase_client_k'],
                'coinbase_private_k' => $setting['coinbase_private_k'],
                'coinbase_status' => $setting['coinbase_status'],
                'coinbase_redirect' => $setting['coinbase_redirect'],
                'sub_limit' => $setting['sub_limit'],
                'portal_login' => $setting['portal_login'],
                'portal_password' => $setting['portal_password'],
                'portal_link_api' => $setting['portal_link_api'],
                'btn_portal_status' => $setting['btn_portal_status'],
            ];
            if (get_option('option_iptv')) update_option('option_iptv', $data);
            add_option('option_iptv', $data);
            $option = get_option("option_iptv");
            return rest_ensure_response($option);
        } catch (Exception $err) {
        }
    }

    /**
     * get setting
     */
    public function get_setting($request)
    {
        if ($option = get_option('option_iptv')) {
            return rest_ensure_response($option);
        } else {
            return rest_ensure_response(null);
        }
    }



    /**
     * log message error
     */
    public function logger_message($params)
    {
        try {

            $log_message  = $params . "\n";


            global $wp_filesystem;
            if (empty($wp_filesystem)) {
                require_once(ABSPATH . '/wp-admin/includes/file.php');
                WP_Filesystem();
            }

            $uploads =  wp_upload_dir();
            $dir  = WP_CONTENT_DIR . "/uploads/";
            $file_name = 'log.log';
            $message  = $log_message;

            $url = "{$dir}{$file_name}";
            if (!$wp_filesystem->put_contents($url, $message, 0777)) {
                echo "file no create";
            }
        } catch (Exception $err) {
            return rest_ensure_response("failed write file");
        }
    }

    /**
     * check chanel in packet
     */
    public function chanel_in_packet($chanel, $packet)
    {
        try {

            global $wpdb;
            $query = $wpdb->prepare("SELECT package_id FROM   $this->table_name2 where  chanel=%s", $chanel);
            $chanels = $wpdb->get_results($query);
            foreach ($chanels as $chanel) {

                $query2 = $wpdb->prepare("SELECT package_id FROM   $this->table_cross where category_id=%d and package_id=%d", $chanel->package_id, $packet);

                $packet_id = $wpdb->get_row($query2);

                if ($packet_id == null) continue;

                return $packet_id->package_id;
            }
        } catch (Exception $err) {
            return true;
        }
    }


    /**
     * get chanel name by name
     */

    public function getChaneByName($chanel)
    {
        try {
            global $wpdb;
            $query = $wpdb->prepare("SELECT title FROM   $this->table_name2 where  chanel=%s", $chanel);
            $chanel = $wpdb->get_row($query);
            return $chanel->title;
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }



    /**
     * get more popular chanel by day
     */
    public function popular_chanels($request)
    {
        try {
            $limit = (int) $request['limit'];
            if ($limit == null) $limit = 5;
            $table  = $this->table_statistics;
            if ($request['archive'])  $table  = $this->table_archive;
            global $wpdb;
            $data_stat = [];
            $query = "SELECT sum(total) as total, stream_clients, created as day ,chanel  FROM  $table  group by chanel  order by total desc limit $limit";
            $popular = $wpdb->get_results($query);
            foreach ($popular as $data) {

                $chanel = $this->getChaneByName($data->chanel);
                $data_stat[] = ['total' => $data->total, 'chanel' => $chanel];
            }
            if ($popular != null) {
                return rest_ensure_response($data_stat);
            }
            return rest_ensure_response($popular);
        } catch (Exception $err) {
            rest_ensure_response($err);
        }
    }


    /**
     * get month day 
     */
    public function get_month_day($token, $chanel, $archive = false)
    {
        try {
            $table  = $this->table_statistics;
            if ($archive)  $table  = $this->table_archive;
            global $wpdb;
            $query = $wpdb->prepare("SELECT dayofmonth(created) as day  FROM  $table where token=%s and chanel=%s", $token, $chanel);
            $dayof = $wpdb->get_results($query);

            $status  = false;
            if ($dayof != null) {
                foreach ($dayof as $day) {
                    if (date('j') == $day->day) {
                        $status = true;
                    }
                }
                return $status;
            } else {
                return $status;
            }
        } catch (Exception $err) {
            rest_ensure_response($err);
        }
    }

    /**
     * insert statistic to table
     */
    public function add_statistic($data, $r, $archive = false)
    {
        try {
            $table  = $this->table_statistics;
            if ($archive)  $table  = $this->table_archive;

            $agent = $r->get_header_as_array('user_agent')[0];
            global $wpdb;
            if ($agent == null) $agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0)';

            $total = (int) $data['stream_clients'];
            $data_in =  [
                'chanel' => $data['name'],
                'agent' =>  $agent,
                'ip' => $data['ip'],
                'token' => $data['token'],
                'stream_clients' => $total,
                'created' =>  current_time('mysql'),
            ];

            $wpdb->insert($table, $data_in);
            //   $this->logger_message($wpdb->last_error);
            // print_r($wpdb->last_error);


        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * check token user
     * add sub domain fix
     */
    public function token_check($request)
    {
        try {

            global $wpdb;

            // $this->logger_message($request->get_params());
            $params_types  =  $request->get_params();

            // hls_dvr
            //  header('HTTP/1.0 302 Found');
            //Location: http://www.iana.org/domains/example/
            $param =  $request->get_params();
            $token  = $param['token'];
            $token_sub  = $this->check_sub_token($param['token']);

            if ($token_sub != null)   $token  = $token_sub;

            if (!isset($param['token'])) {
                header('HTTP/1.0 403 Forbidden');
                exit;
            }
            if ($param['token'] == '') {
                header('HTTP/1.0 403 Forbidden');
                exit;
            }
            //text/plain
            //header("Content-Type: text/plain");
            //  $responce->header("Cache-Control","no-store, no-cache, must-revalidate, max-age=0",true);
            //  $responce->header("Cache-Control","post-check=0, pre-check=0",true);
            //  $responce->header("Pragma","no-cache",true);
            //  $responce->header('Expires','-1',true);
            $query1 = $wpdb->prepare("SELECT category_id, keyhash as token , adult ,  status, session_limit FROM  $this->iptv_users  where keyhash=%s", $token);
            $user_data = $wpdb->get_row($query1);
            $package_id = (int) $user_data->category_id;
            $query2 = $wpdb->prepare("SELECT id, archive FROM  $this->table_name3 where id=%d", $package_id);
            $archive_status = $wpdb->get_row($query2);
            if ($token == "{$user_data->token}") {
                if (!$this->get_month_day($token, $params_types['name'])) {
                    $this->add_statistic($params_types, $request);
                }
                $archive = (int) $archive_status->archive;

                if ($params_types['type'] == 'hls_dvr' && $archive  ==  1) {
                    if (!$this->get_month_day($token, $params_types['name'], true)) {
                        $this->add_statistic($params_types, $request, true);
                    }
                }

                //mpegts_dvr
                if ($params_types['type'] == 'mpegts_dvr' && $archive  ==  1) {
                    if (!$this->get_month_day($token, $params_types['name'], true)) {
                        $this->add_statistic($params_types, $request, true);
                    }
                }

                $wpdb->update($this->iptv_users, ['ip' => $param['ip']], ['keyhash' => $token]);
                if ("false" == "{$user_data->status}") {

                    header('HTTP/1.0 403 Forbidden');
                    exit;
                }

                if ($params_types['type'] == 'hls_dvr' && $archive  !=  1) {

                    header('HTTP/1.0 403 Forbidden');
                    exit;
                }

                $test = "" . $params_types['referer'] . " : " . $params_types['name'];
                $this->logger_message($test);
                $packet_in  = $this->chanel_in_packet($params_types['name'], $package_id);



                //package id
                if ($package_id != $packet_in) {
                    //   print_r("No in package");
                    header('HTTP/1.0 403 Forbidden');
                    exit;
                }

                $limit = (int) $user_data->session_limit;

                $token_set  = $param['token'];

                //    if ($token_sub != null) {
                //        // print_r($token_sub);
                header("HTTP/1.0 200 OK");
                header("X-UserId: " . $token_set . "\r\n");

                if ($token_sub != null) {
                    header("X-Max-Sessions: 1 \r\n");
                } else {
                    header("X-Max-Sessions: $limit \r\n");
                }

                exit;
                // } 
                // elseif($packet_in != null) {
                //   //  print_r("user id 2");
                //     header("HTTP/1.0 200 OK");
                //     header("X-UserId: " . $token_set . "\r\n");

                //     header("X-Max-Sessions: 1 \r\n");
                //     exit;
                // }
                // else{
                //     header('HTTP/1.0 403 Forbidden');
                //      exit;

                // }

            } else {

                header('HTTP/1.0 403 Forbidden');
                exit;
            }
        } catch (Exception $err) {
            // print_r($err->getMessage());
            header('HTTP/1.0 403 Forbidden');
            exit;
        }
    }


    /**
     * add sub account
     */
    public function  add_subaccount($request)
    {
        try {
            $params = $request->get_params();
          //  print_r($params['type']);

            $main_token  = $params['token'];

            global $wpdb;
            if ($params['type'] == 'play') {
                $sub_user_token = create_token($params['name']);
                $sub_token = [
                    'token_user' => $main_token,
                    'token' =>   $sub_user_token,
                    'name' =>  '',
                    'type'  => $params['type'],
                    'expire' => current_time('mysql'),
                ];

                $wpdb->insert($this->sub_accounts, $sub_token);
                $last_id   = $wpdb->insert_id;
                return rest_ensure_response(["sub_acc" => $sub_token, "id" => $last_id]);
            }
           
            
            else {
                $pass_stalk   =  $params['pass'];
               
                $sub_stalk = [
                    'token_user' => $main_token,
                    'token' =>   $pass_stalk,
                    'name' =>  str_replace(" ", "_", $params['name']),
                    'type'  => $params['type'],
                    'expire' => current_time('mysql'),
                ];
                $wpdb->insert($this->sub_accounts, $sub_stalk);
                $last_id   = $wpdb->insert_id;
                //  $stalker_name  = $params['name'] . "_" . $last_id;
                $stalker_name  = $params['name'];
                $tariff  = $this->get_tariff_by_package($params['stalkerdata']['tariff_plan']);
                $now   = new DateTime;
                $time_off = $now->modify("+1 month")->format("d-m-Y H:i:s");
                $stalker_data = [
                    'login'  => $pass_stalk,
                    'full_name' =>  str_replace(" ", "_", $stalker_name),
                    'password' => $pass_stalk,
                    'account_number' => $params['id'],
                    'tariff_plan' => $tariff,
                    'status'  => 1,
                    'end_date' => $time_off,
                ];
                if($params['type'] == 'stalker'){
                   
                  
                    $this->add_stalker_account_without_request($stalker_data);

                }
                elseif($params['type'] == 'portal'){
                   $user = \Iptv\ServiceRemote::add_user($stalker_data);
                }
               
                return rest_ensure_response(["sub_acc" => $sub_stalk, "id" => $last_id]);
            }


            return rest_ensure_response($request);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * get all sub account by token
     */
    public function get_all_subaccounts($request)
    {
        try {
            $params = $request['token'];
            global $wpdb;
            $query =  $wpdb->prepare("SELECT * FROM $this->sub_accounts WHERE token_user=%s", $params);
            $token_result = $wpdb->get_results($query);
            if ($token_result == null)   return rest_ensure_response([]);
            return rest_ensure_response($token_result);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * update main token in sub token
     */
    public function update_sub_token($old_token, $new_token)
    {
        try {
            global $wpdb;
            $updated = $wpdb->update($this->sub_accounts, ["token_user" => $new_token], ["token_user" => $old_token]);
            return  rest_ensure_response($wpdb->last_error);
            die;
            if (false  == $updated) throw new Exception("Failed update");
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * delete token
     */
    public function delete_sub_token($request)
    {
        try {
            global $wpdb;
            $token = $request['token'];
            $rez  = $wpdb->delete($this->sub_accounts, ['token' => $token]);
            if ($rez){
                $this->delete_stalker_account(['id' => $token]);
                $user_delete  = \Iptv\ServiceRemote::remove_user($token);
            } 
            return rest_ensure_response($rez);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     *  get parent token
     */

    public function check_sub_token($token)
    {
        global $wpdb;
        $query =  $wpdb->prepare("SELECT token_user as u_token FROM $this->sub_accounts WHERE token=%s", $token);
        $token_result = $wpdb->get_row($query);
        if ($token_result != null) {

            return $token_result->u_token;
        } else {
            return null;
        }
    }

    /**
     * get user by id token
     */
    public function get_user_token($id)
    {
        try {
            global $wpdb;
            $q = $wpdb->prepare("SELECT keyhash FROM $this->iptv_users WHERE id=%d", $id);
            $token  = $wpdb->get_var($q);
            return $token;
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * add stalker
     */

    public function add_stalker_account($request)
    {
        try {

            $option  =  get_option("option_iptv");
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => $request->get_params(), ['timeout' => 120,]];
            $url = $option['url_stalker'] . 'accounts/';
            // $this->add_stalker_db(['id' => $request['account_number'], 'password' => $request['password']], true);
            $data  = wp_remote_post($url, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     *  [login] => demos2@demos.com
     *[full_name] => demos
     * [password] => 4eseqb
     * [account_number] => 24
     * [tariff_plan] => 15
     *[status] => 1
     *)
     */
    public function add_stalker_account_without_request($datas)
    {
        try {
            //  print_r($datas); die;
            $option  =  get_option("option_iptv");
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => $datas, ['timeout' => 120,]];
            $url = $option['url_stalker'] . 'accounts/';
            $data  = wp_remote_post($url, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * chnange stalker password
     */
    public function change_stalker_account($request)
    {
        try {
            $password = $request['password'];
            $id = $request['id'];
            $this->add_stalker_db(['id' => $id, 'password' => $password], false);
            $option  =  get_option("option_iptv");
            $url = $option['url_stalker'] . 'accounts/' . $id;

            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => ['password' => $password], 'method'    => 'PUT', ['timeout' => 120,]];
            $data  = wp_remote_request($url, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * get package tariff_id
     */
    public function get_tariff_by_package($id)
    {
        try {

            global $wpdb;
            $query = $wpdb->prepare("SELECT tariff_id  FROM   $this->table_name3 WHERE id=%d", $id);
            $tariff = $wpdb->get_var($query);
            return $tariff;
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

   

    /**
     * change stalker tariff
     */
    public function change_stalker_package($request)
    {
        try {
            $pac_req = $request['package_id'];
            $package  = $this->get_tariff_by_package($pac_req);
          
            $id = $request['id'];
           
            $option  =  get_option("option_iptv");
            $url = $option['url_stalker'] . 'accounts/' . $id;
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => ['tariff_plan' => $package], 'method'    => 'PUT', ['timeout' => 120,]];
            $data  = wp_remote_request($url, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    public function change_status_off($request)
    {
        try {

            $id = $request['id'];
            $option  =  get_option("option_iptv");
            $url = $option['url_stalker'] . 'accounts/' . $id;
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => ['status' => 0], 'method'    => 'PUT', ['timeout' => 120,]];
            $data  = wp_remote_request($url, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * change expire stalker date
     */

    public function change_data_expire_stalker($req)
    {
        try {

            $id = $req['id'];
            //25-08-2020 13:00:00
            $date   = $req['expire'];
            ///2020-08-30 00:00:00
            $option  =  get_option("option_iptv");
            $url = $option['url_stalker'] . 'users/' . $id;
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => ['tariff_expired_date' => $date, 'status' => 1], 'method' => 'PUT', ['timeout' => 120,]];
            $data  = wp_remote_request($url, $auth);
            $body = wp_remote_retrieve_body($data);

            return $body;
        } catch (Exception $err) {
            return $err;
        }
    }

    /**
     * delete stalker account
     */
    public function delete_stalker_account($request)
    {
        try {
            $id = $request['id'];
            $option  =  get_option("option_iptv");
            $url = $option['url_stalker'] . 'users/' . $id;
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])],  'method'    => 'DELETE', ['timeout' => 60,]];
            $data  = wp_remote_request($url, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * add account to db
     */
    public function add_stalker_db($data, $type = true)
    {
        try {
            global  $wpdb;
            $params  = [
                'user_id' => $data['id'],
                'stalker_id' => $data['id'],
                'stalker_p' =>  $data['password'],
                'creted' => current_time('mysql')
            ];
            if ($type) {
                $wpdb->insert($this->table_stalker, $params);
            } else {
                $params  = [
                    'stalker_p' =>  $data['password'],
                ];
                $wpdb->update($this->table_stalker, $params, ['stalker_id' => $data['id']]);
            }
        } catch (Exception $err) {
        }
    }

    /**
     * get stalker account
     */
    /**
     * add stalker
     */

    public function get_stalker_account($request)
    {
        try {
            $login = $request['param'];
            $option  =  get_option("option_iptv");
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => $request->get_params(), ['timeout' => 120,]];
            $url = $option['url_stalker'] . 'users/';
            $full  = $url . $login;

            $data  = wp_remote_get($full, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * get all stalker tariff
     */
    public function get_all_tariff($request)
    {
        try {
            $login = $request['param'];
            $option  =  get_option("option_iptv");
            $auth =   ['headers' => ['Authorization' => 'Basic ' . base64_encode($option['login_stalker'] . ':' .  $option['password_stalker'])], 'body' => $request->get_params(), ['timeout' => 120,]];
            $url = $option['url_stalker'] . 'tariffs';
            $full  = $url;
            $data  = wp_remote_get($full, $auth);
            $body = wp_remote_retrieve_body($data);
            return rest_ensure_response(json_decode($body));
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * check email
     */
    public function check_email($request)
    {
        try {
            global $wpdb;
            $email  = $request['email'];
            $query  =  $wpdb->prepare("SELECT user_email as email FROM  $this->users WHERE user_email=%s limit 1", $email);
            $email_db  = $wpdb->get_row($query);

            if ($email == "{$email_db->email}") {
                return rest_ensure_response(['email' => true]);
            } else {
                return rest_ensure_response(['email' => false]);
            }
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * get chanel url from api
     */
    public function server_status($request)
    {
        $param  = 'server';
        // $url  = "$this->url$param";
        $http   =  get_option('option_iptv');
        $url  = $http['stat_link'] . $param;

        try {
            $data  = wp_remote_get($url, $this->args);
            $body = wp_remote_retrieve_body($data);
        } catch (Exception $err) {
            $data = "Connection error";
        }


        return json_decode($body, true);
    }

    /**
     * payment by stripe
     */
    public function apiStripe($request)
    {
        try {
            $client  = \Iptv\Payment::payByStripe($request);
            return rest_ensure_response(["key" => $client->client_secret]);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * checkout payment
     */
    public function apiCheckout2($request)
    {
        try {
            $body = \Iptv\Payment::payBy2Checkout($request);
            return rest_ensure_response(["result" => json_decode($body)]);
        } catch (Exception $err) {
            return rest_ensure_response(['error' => $err->getMessage()]);
        }
    }

    /**
     * coinbase payment
     */
    public function apiCoinBase($request)
    {
        try {
            $responce  = Payment::payByCoinBase($request);
            return rest_ensure_response($responce);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    public function authCoinbase($request)
    {
        try {
            $options   = get_option("option_iptv");
            $provider = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                => trim($options['coinbase_client_k']),    // The client ID assigned to you by the provider
                'clientSecret'            => trim($options['coinbase_private_k']),   // The client password assigned to you by the provider
                'redirectUri'             => $options['coinbase_redirect'].'/billing/auth',
              //  'redirectUri'             => 'http://duga.tv/billing/auth',
                'urlAuthorize'            => 'https://www.coinbase.com/oauth/authorize',
                'urlAccessToken'          => 'https://api.coinbase.com/oauth/token',
                'urlResourceOwnerDetails' => 'https://api.coinbase.com/v2',
            ]);
           

            if (!session_id()) {
                session_start();
            }

            if (!$request['code']) {
              
                $user_id = $request['id'];
                $authorizationUrl = $provider->getAuthorizationUrl();
                $_SESSION['oauth2state'] = $provider->getState();
                $permUrl = $authorizationUrl . "&scope=wallet:accounts:read,wallet:transactions:send&meta[send_limit_amount]=1";
              
              
                return  rest_ensure_response(['url' => $permUrl]);
            } elseif (empty($request['state']) || ( isset($_SESSION['oauth2state']) && $request['state'] !== $_SESSION['oauth2state']) ) {
               
                if (isset($_SESSION['oauth2state'])) {
                    unset($_SESSION['oauth2state']);
                }
              
                return rest_ensure_response(["error" => 'state invalid']);
            } else {

                $accessToken = $provider->getAccessToken('authorization_code', ['code' => $request['code']]);
                $token  =  $accessToken->getToken();
                return rest_ensure_response(["token" => $token]);
            }
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $err) {
            return rest_ensure_response($err->getMessage());
        }
    }

    /**
     * 
     */
    public function coinbaseData($request)
    {
        try {
            $data  = wp_remote_get($request['url']);
            $data =  wp_remote_retrieve_body($data);
            // return rest_ensure_response(['response'=>$data] );
            header('Content-Type: text/html');
            return $data;
        } catch (Exception $err) {
            return rest_ensure_response($err->getMessage());
        }
    }


    /**
     * blockchain payment
     */
    public function apiBlockchain($request)
    {
        try {
            $response  =  Payment::payByBlockchain($request);
            return rest_ensure_response($response);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * payssiera payment
     */
    public function apiPaySsiera($request)
    {
        try {
        } catch (Exception $err) {
        }
    }

    /**
     * restore password
     */
    public function restorePassword($request)
    {
        try {
            global $wpdb;
            $query = $wpdb->prepare("SELECT ID as id, user_email as email FROM $this->users WHERE user_email=%s", $request['email']);
            $data = $wpdb->get_row($query);

            if ($data != null) {

                Email::sendEmail(['type' => 'reset', 'user_id' => $data->id, 'email_to' => $data->email]);
                return rest_ensure_response(['status' => true, 'email' => $data]);
            } else {

                return rest_ensure_response(['status' => false, 'email' => $data]);
            }
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

    /**
     * auth portal session_id
     */
    public function auth_portal($request){
        try{
           $auth_portal  = \Iptv\ServiceRemote::auth_portal($request);
         
           
           return rest_ensure_response($auth_portal);
        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

    
    /**
     * add user portaliptv
     */
    public function add_user_portal($request){
        try{
            $user_portal = \Iptv\ServiceRemote::add_user("tester");
         
            return rest_ensure_response($user_portal);

        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

    /**
     * disable user
     */
    public function disable_user_portal($request){
        try{
            $all_sub_accounts  = $this->get_all_subaccounts($request);
           foreach($all_sub_accounts->data as $data){
               if($data->type !== 'portal') continue ;
               $user = \Iptv\ServiceRemote::disable_user($data->token ,$request['status']);
           }
           
          return rest_ensure_response($user);

        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

    /**
     * change portal package
     */
    public function change_package_portal($request){
        try{
            $username  = $request['username'];
            $package = $request['package'];
            $updated_package = \Iptv\ServiceRemote::chnage_package($username , $package);
           return rest_ensure_response($updated_package);

        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }

     /**
     * get all package 
     */
    public function packages_portal($request){
        try{
            $user = \Iptv\ServiceRemote::get_packages();
            
            return rest_ensure_response($user);

        }
        catch(Exception $err){
            return rest_ensure_response($err);
        }
    }


    /**
     * send email  by type action
     */
    public function emailSend($request)
    {
        try {
            // var_dump($request); die;
            if ($request['type'] == 'package-confirm') {
                $now   = new DateTime;
                $time_off = $now->modify("+1 month")->format("d-m-Y");
                $data = Email::sendEmail([
                    'type' => 'package_confirm',
                    'user_id' => intval($request['id']),
                    'email_to' => $request['email'],
                    'suma' => $request['suma'],
                    'expire_date' => $time_off,
                    'name'  =>  $request['name'],
                    'package_name' => $request['pack_name'],

                ]);
                return $data;

            } elseif ($request['type']  == 'payment') {
               
                $data = Email::sendEmail([
                  
                    'type' => 'payment',
                    'user_id' => intval($request['id']),
                    'email_to' => $request['email'],
                    'suma' => $request['suma'],
                   'name'  =>  $request['name'],
                   ]);
                   sleep(1);
                 $data =  Email::sendEmail([
                    'type' => 'payment_admin',
                    'user_id' => intval($request['id']),
                    'email_to' => $request['email'],
                    'suma' => $request['suma'],
                    'pay_type' => $request['tpay'],
                   ]);
                   return $data ;

                } else {
                return rest_ensure_response(['error' => 'type action error']);
            }
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }


    /**
     * check  version of plugin
     */
    public function check_table_version()
    {
        try {
            $version   =  get_option('iptv_db_version');
            (isset($version)) ? 1 : 0;
            return rest_ensure_response(["version" => $version]);
        } catch (Exception $err) {
            return rest_ensure_response($err);
        }
    }

   


    /**
     * create routes
     */
    public function register_routes()
    {
        //stream api
        include_once(ABSPATH . 'wp-content/plugins/iptv/routes.php');

        foreach ($routes as $route) {
            if($route['url'] == '/auth' || $route['file'] == 'file' ){
              
                register_rest_route('iptv', $route['url'], [
                    'methods'   => $route['methods'],
                    'callback' => [$this, $route['callback']],
                  
                ]);

            }
            register_rest_route('iptv', $route['url'], [
                'methods'   => $route['methods'],
                'callback' => [$this, $route['callback']],
                'permission_callback' => 'auth_action'
            ]);
        }
    }
}

/** 
 * action auth 
 */
 function auth_action($request){
    $data = $request->get_header('Iptvkey');
   
    $key  = "aXB0djQ1Njc=" ;
   if($data == $key){
       return true;
   }
  return false;

}

function apitv()
{
    $controller = new IptvApi();
    $controller->register_routes();
}
